Development in PDXC ServiceNow needs to follow the process and best practice guidelines here.

# Documentation

## Documentation for PDXC ServiceNow Developers

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practice and guidelines for Service Portal development                                      |                                                                                                               |
------------------

## Documentation for PDXC ServiceNow Catalog Developers

[See the Catalog Developer Home page...](./Catalog%20Developers/README.md)

----------------------------

|[Top](#documentation)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
