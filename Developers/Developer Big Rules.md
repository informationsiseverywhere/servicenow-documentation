# Development 'Big Rules'

It is critical that every developer and Tech Lead understand the ‘Big Rules’ that govern PDXC ServiceNow development, to protect the scalability, integrity and architecture of the PDXC SN solution. Development that does not respect the ‘Big Rules’ is likely to be rejected, and not migrated to production.

These rules are to be read and understood in conjunction with the overall
[developer
framework](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md).

The Big Rules are categorised as **Avoid** rules, **Never** rules, or **Always**
rules:

| **Rule**                | **Definition**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Avoid](#avoid-rules)   | Items which we should try to resist. However, there are still reasons we might need to do them.  The decision to move forward with an “avoid” item should specifically include Product Ownership, Architecture, and Engineering.  Justification (business, architectural and design rationale) must be noted in the Story. Such items may still become blockers during the Release Review.  If the engineering team has concerns here, the technical lead should bring this to the Global Technical Lead team through one of the channels open to him or her.  The Technical Lead should be prepared to discuss what alternatives were discussed, dismissed, and why. This should happen _before_ development starts to avoid abortive work.                |
| [Never](#never-rules)   | Items that are simply not acceptable, and for which all alternatives should be explored before being considered.  Because there are a variety of factors which lead to “never” not actually meaning “never”, any such item should be discussed with Global Technical Leads *before* beginning development.  “Never” item implementations are generally the result of non-existent solutions being sold, and then PDXC SN architects and product owners being overridden by higher level management.  As a rule, these items are more costly to develop, maintain, and support, and there is a very good chance they impact the reliability of the solution as a whole. |
| [Always](#always-rules) | Key items that should always be considered during the development process. A lack of consideration of these rules can lead to fatally flawed developments that have to be re-worked.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

AVOID RULES
===========

Avoid variation (customization) from Out of the Box (OOTB)
-----------------------------------------------------------

-   PDXC SN is a “standard” solution, supporting hundreds of customers and
    millions of end-users.

-   PDXC SN absorbs ServiceNow major platform updates at least twice per year.

-   ServiceNow cannot guarantee compatibility of DXC customizations with future
    releases.

-   Every customization increases the cost of development, support, and
    maintenance, and can impact the aggressive timelines under which DXC absorbs
    new upgrades.

-   If the OOTB solution can meet the majority of the business requirements for
    a given story,  or if the business requirements can be satisfied by a
    component of the broader Platform DXC solution (e.g. reporting), even at just
    an “80%” level, every effort should be made to take the non-development
    path.

-   ServiceNow support of customer (DXC) customizations is limited to items
    they can duplicate in an OOTB instance.

Avoid customer-specific development
-----------------------------------

-   The same drivers for avoiding variation from OOTB apply here.  Every
    customer-specific variation incurs cost of perpetual development and
    support, also known as technical debt, which does not scale.

-   Integrations and Customer catalogs are the areas of the platform where
    customer-specific work is planned for and expected, although it should
    always be considered preferable to use a global solution.

-   “This one customer wants it,” is not sufficient justification, unless PDXC
    SN architects and product owners, with full awareness of PDXC SN
    architectural direction, have specifically agreed to the approach.

Avoid schema changes (adding/removing table columns)
----------------------------------------------------

-   Changes to custom tables (u_table) are more readily accepted and preferred
    over changes to Out of the Box (OOTB) and core data tables.

-   Schema changes can bring performance impact at release time.

-   OOTB and core data tables which do not have columns for seemingly obvious
    business purpose may be by design.  If there is a discrepancy between OOTB
    business processes and your story, consider checking with ServiceNow to
    understand why they don’t have that particular feature.

Avoid releasing stories which deliberately share update sets
------------------------------------------------------------

-   A story should solve a well-defined business problem, and be independently
    testable.

-   Update sets should deliver one story.

-   If it makes sense to put a group of stories all in one update set, those
    stories are most likely poorly written.

-   A release review includes looking at the contents of the update set and
    verifying that they deliver (only) what is described in the story.  When an
    update set contains multiple stories, it is more difficult to associate
    individual elements with the story driving the change.

-   Technical entanglements happen.  An entanglement is where multiple stories
    (update sets) touch the same object, such as a UI page, and become
    sequentially dependent on each other.  While it is preferable to avoid
    these, they happen.  This should be managed by indicating story
    interdependencies in migration information.  It is not acceptable to just
    lump them all together.

-   Update sets shared between stories immediately slows down the release review
    process, which could cause a story to miss a target release, and it blocks
    all the stories sharing the same update set until every issue has been
    resolved

ALWAYS RULES
============

Always ensure new tables have full product architecture approval
----------------------------------------------------------------

-   All new tables have direct cost recovery implications for DXC.  Solutions
    which introduce new tables without evidence that product architecture has
    explicitly approved each new table will not be released.    


Always consider the PDXC SN Security Model
------------------------------------------

-   Any Permission changes (groups, roles, group-role associations)  should
    always be made with consideration of the PDXC SN Security model.  Updates
    made to the security model documentation must be updated as part of story
    release process.

Always consider the impact to support
-------------------------------------

-   What engineering team is accountable for supporting your change, not just
    today, but a year from now?

-   Has documentation for your changes been integrated appropriately with the
    Account Transition Guidelines?  Should they be?

-   Do the story work notes and/or code comments adequately reflect the approach
    taken for this implementation?

    -   In six months, will someone else, or even you, be able understand why
        you did what you did?

    -   When it comes time to do the skip list review, comparing your work to
        OOTB, will it be obvious why your changes are there and are still
        needed, or can be reverted to OOTB?

Always make sure your story accurately reflects what has been developed
-----------------------------------------------------------------------

-   A robust and well-formed story is the primary factor in ensuring a smooth 
    and efficient technical review and release management process.
    1.  Your story must contain a well-defined Description that reflects the scope and purpose of the accompanying technical materials.
    1.  Your story must reflect well-defined Acceptance Criteria that communicates clearly the expected purpose and behavior of the solution.
    1.  Your story must provide evidence (an attached email, meeting minutes, grooming session reports) that the proposed solution design has been vetted and approved by the appropriate Product Owner and Domain Architect  of the impacted [Service Now App/Area](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md). Obtain this approval **prior** to coding in the shared development environment. 
            
-   As technical requirements or other factors drive changes to the story, make
    sure the story is updated.

-   It is helpful when the developer attempts to provide a solution overview in
    the work notes.  The solution overview documents the technical approach
    taken to accommodate the business requirements captured in the story.  When
    there are multiple possible implementation options, or if a known “avoid”
    option was taken, it is smart to capture the thinking which led to the final
    solution.

NEVER RULES
===========

Never include Customer-specific development in global update stories (and vice versa)
--------------------------------------------------------------------------------------

-   A story may include customer-specific development, targeting a specific
    stack, OR ...

-   it targets the global solution, going to all stacks.


Never independently modify capabilities owned and supported by other teams
--------------------------------------------------------------------------

-  Changes to all ServiceNow objects and documentation have ongoing cost and support
   implications.  Specific development teams have direct support accountability for each
   capability.  Changes to ServiceNow capabilities must be performed or
   explicitly authorized by the accountable team before promotion to devtest.
   
-  Stories will not be considered for release until content has been technically
   reviewed and accepted by the accountable development team.
   
-  It is not feasible to maintain a reliable list identifying ownership of each ServiceNow
   object, documentation, and capability in DXC ServiceNow BP.  Technical leads authorized
   to promote content to devtest have access to resources and knowledge to help determine
   object ownership.
   

------------------------------

```
The Global Tech Leads have a responsibility, as gatekeepers, to protect the scalability,
integrity and architecture of the PDXC SN solution.

As such, they may reject development that does not respect the ‘Big Rules’.
```

------------------------------

|[Top](#development-big-rules)|
|--------|


