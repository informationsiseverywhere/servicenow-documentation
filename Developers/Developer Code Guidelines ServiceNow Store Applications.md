# Platform X Guidelines for ServiceNow Store Applications

*[Back to Platform X ServiceNow Developer Guidelines](Developer%20Code%20Guidelines%20DXC.md)*

ServiceNow Store Applications carry unique challenges and must be carefully managed
within Platform X ServiceNow.  
Platform X ServiceNow (PX SN) development teams
should start here when considering how to release a Store Application.

## Contents

* [Introduction](#introduction)
* [Store App Lifecycle](#store-app-lifecycle)
* [Installing a Store App](#installing-a-store-app)
  * [Before DEV](#before-dev)
  * [Before SANDBOX](#before-sandbox)
* [Migration Information](#migration-information)
* [Dependency Analysis](#dependency-analysis)

## Store App Lifecycle

The lifecycle of a Store application is not the same as the NOW platform:

Store applications are **not** plugins.

* Store Applications can have dependencies (entanglement) with other plugins and applications.
  * Dependency plugins and applications may be the responsibility of other Platform X
    ServiceNow teams - one team cannot independently upgrade another team's application.
  * Dependency versions can be higher or lower than versions currently in Platform X ServiceNow.
* Application versions are released on a cycle independent of the platform's version (family).
  * They are independently managed by the third party vendor or ServiceNow product team
    which publishes the application.
  * Versions can disappear during a release cycle, putting Platform X ServiceNow releases at risk.
* **Some** applications will have **some** elements automatically upgraded or otherwise changed
  by the platform during a family or patch upgrade.
* Store Applications must be entitled and licensed in each ServiceNow account
  where they will be used.
* Store Applications must be installed to all Platform X instances.
* On-premises application installation requires output of the completed .0 (Family upgrade) release
  before the application can be downloaded by Ops teams.
* **Some** store applications have an impact on Platform X's subscription model.
* **Some** store applications require individual fees or subscription which must be managed with the Application's vendor.

What do these mean?

* Application teams must manage "Skip List" and other upgrade activities, both at the time of
  Platform upgrade, and periodically (as versions or dependencies are released).
* Every application version and plugin which must change must be explicitly managed.
* Where dependencies exist on other application teams, commitments must have been made
  by those teams.
* No application can be installed in a ".0" (Family upgrade) or patch upgrade release.
* Every application install should have its own development story.
  * Customizations must be in a separate story.
  * Multiple applications within the same Application team's responsibility may
    be in one story.  For example, a ServiceNow store product such as Security
    Incident Response includes many applications.  It may be appropriate to
    upgrade all of those, as part of the whole, in a single story.  However,
    each Product (with its associated applications) should be in its own story.
  * A separate story is required for applications or plugins that are a cross-domain responsibility.
* Store Applications must be entitled for all Platform X Servicenow Accounts (currently 9).
* Application teams must work with vendor to guarantee availability of
  application through to last possible production date.
* Store Applications cannot have a development/release cycle spanning a Family upgrade.
  * Applications installed in DEV must be installed to all Platform X
    ServiceNow instances in the same Family/Patch version.
  * **An application installed in DEV which has not been released will
    unconditionally block a Platform X ServiceNow family upgrade**.
  * Applications targeting the last two releases before a family or patch
    upgrade create significant risk for the family or patch upgrade, and will
    only be considered on an exceptional basis, where risk has been managed
    down to acceptable levels.

## Installing a Store App

### Before DEV

**An application may be installed to Dev only after Global Technical Lead
team has documented the story to indicate that pre-DEV work has been
completed.**

1. Application team completes application evaluation and analysis in
   [SANDBOX](#before-sandbox).
2. Application team performs [Dependency Analysis](#dependency-analysis) on
    application while in sandbox.  All new/updated dependencies must be planned
    for.
    1. For cross-domain entanglements, same effort must be performed by
      team responsible for the dependency application.
    2. Output of dependency analysis must be attached to story.
3. Application team reviews all cost implications with Product Architecture
   team.  Evidence of Product Architecture agreement should be in story.
4. Application is being worked through a story being targeted for a
   specific release.
5. Application team secures guarantee from Application vendor that the specific
    application version being worked in story will be available to install
    through date of last production instance of likely release.
6. Application team works with Application vendor to understand unique
    procurement or purchase requirements for **each Platform X ServiceNow
    account**.
7. Application team reviews outputs of the above with Platform X ServiceNow
    Global Technical Lead (GTL) team.
8. Application team has accurate Product Owner and Domain Architect information
    recorded in [Platform X Teams - Who's Who (TeamsFull.md)](../Onboarding/TeamsFull.md)
9. **Where fees or application purchase is required** Global Technical Lead
    team works with Application team and vendor (if required) to go through
    "purchase" of application for each Platform X ServiceNow account.  Product
    Owner and Domain Architect will be designated contacts where needed.
10. Global Technical Lead team performs entitlement of application for all
    Platform X ServiceNow instances in all Platform X ServiceNow accounts.
    Product Owner and Domain Architect will be configured to receive
    notifications sent by vendor for each application in each Platform X account.
11. Application team updates story [Migration Information](#migration-information).
12. Global Technical Lead team documents story to indicate that pre-DEV
    requirements have been completed.

### Before SANDBOX

Applications should be evaluated in the cscsandbox2 instance.

1. Product Architecture team must be aware of the trial.
2. Product Engineering team must be aware of the trial.
3. Any significant architectural or operational risks have been identified.
4. Application team understands the commitment it is making to support
    the application, through platform and application upgrades.

### Migration Information

In addition to the below details, if there are any stories which must be
in the same release, those dependencies should also be included
[this is standard practice (migration information template).](Developer%20Template.md#migration-information-template)

For release of an application, four key elements of data are used.
All should be provided in the Migration Information.  
**scope**: *example: sn_intune_integrat*  
**name**: *example: Service Graph Connector for Microsoft Intune*  
**application id**: *example: a1ce9646683e0050f8774bfad236c981*  
**version**: *example: 2.0.0*  

Refer to [Dependency Analysis](#dependency-analysis) for information on how to
find this data.

Where more than one application is included in a story, the sequence must
be provided in the exact order that will be used to install to dev.

Migration Information example:

```Migration Information
Must be in same release as STRY123
Depends on STRY125 - can be in same or later release.
Enables STRY126 - can be in same or earlier release.

plugin foo.foundation
Store Application A com.snc.foo ver 1.2.3 a1ce9646683e0050f8774bfad236c981
Store Application B com.snc.foo2 ver 8.2.3 f1ce9646683e0050f8774bfad236c981

```

### Dependency Analysis

1. Output of completed dependency script in cscdev should be documented in the story
2. The completed dependency script should be attached to the story
3. Output of completed dependency script in sandbox should be documented in the story

For release of an application, four key elements of data are used.
All should be provided in the Migration Information.  
**scope**: *example: sn_intune_integrat*  
**name**: *example: Service Graph Connector for Microsoft Intune*  
**application id**: *example: a1ce9646683e0050f8774bfad236c981*  
**version**: *example: 2.0.0*  

Most of this information can be gotten from the ServiceNow Store page for the application:
<https://store.servicenow.com/sn_appstore_store.do#!/store/application/a1ce9646683e0050f8774bfad236c981/2.0.0>

The above url gives the application id (a1ce9646683e0050f8774bfad236c981) and the version (2.0.0).
The Name can found at the top of the page itself (Service Graph Connector for Microsoft Intune).

Scope is most quickly found from the SANDBOX where the application has been installed.
**System Applications** -> **All Available Applications** -> **All** -> *Search for name of application*
If found, the scope will be shown as the "id" (e.g. **Id: sn_intune_integrat**)

Use the below script in order to perform Dependency analysis.

Script will be executed in SANDBOX instance in Scripts - Background window.

the targetApps array should be updated to contain all of the applications that
the application team intends to include in a single story.

```javascript
var targetApps = [
    /*
        //template:
        {
            scope: "",
            name: "",
            appId: "",
            version: ""
        },
    */
        {
            scope: "sn_intune_integrat",
            name: "Service Graph Connector for Microsoft Intune",
            appId: "a1ce9646683e0050f8774bfad236c981",
            version: "2.0.0"
        }
    ];

if (!VerifyUserHasRequisites()) {
  gs.error('Current user session has not been properly configured.  Please correct issues and restart.');
} else {

    var results = {};
    results.successes = [];
    results.failures = [];
    for (var pos = 0; pos < targetApps.length; pos++) {
        var appDefinition = targetApps[pos];

        targetApps[pos].sys_store_app = GetLocalAppData(appDefinition, true);

        if (JSUtil.nil(targetApps[pos].sys_store_app) && appDefinition.version) {
            var tempAppDefinition = {
                scope: appDefinition.scope
            }
            targetApps[pos].sys_store_app = GetLocalAppData(tempAppDefinition, true);
            if (JSUtil.notNil(targetApps[pos].sys_store_app)) {
                for (appVersion = 0; appVersion < targetApps[pos].sys_store_app.length; appVersion++) {
                    gs.print("WARNING: An imprecise version (" + targetApps[pos].sys_store_app[appVersion].version + ") match was found for " + appDefinition.name + ";" + appDefinition.version + "; " + appDefinition.scope);
                }
            }
        }


        var dependencies;
        if (JSUtil.notNil(targetApps[pos].sys_store_app)) {
            for (var appRecord = 0; appRecord < targetApps[pos].sys_store_app.length; appRecord++ ) {
                
                if (targetApps[pos].sys_store_app[appRecord].dependencies) {
                    dependencies = targetApps[pos].sys_store_app[appRecord].dependencies.toString();
                }
                var version = targetApps[pos].sys_store_app[appRecord].version;
                var scope = targetApps[pos].sys_store_app[appRecord].scope;
                var name = targetApps[pos].sys_store_app[appRecord].name;
                gs.print ("Found local app data for name(" + name + ")/scope("+ scope +")/version("+ version +"):");
                if (JSUtil.notNil(dependencies) && JSUtil.notNil(version)) {
                    gs.print("    dependencies " + dependencies);
                    var gotDepData = GetDependencyAppData(dependencies);
                    
                    JSON.stringify(gotDepData)
                    if (JSUtil.nil(targetApps[pos].sys_store_app.dependencies)) {
                        targetApps[pos].sys_store_app.dependencies = {};
                    }
                    targetApps[pos].sys_store_app.dependencies[version] = gotDepData;
                } else {
                    gs.print("    no dependencies indicated");
                }
                
            }
        } else if (JSUtil.notNil(appDefinition.scope)) {
            var foundPluginVersion = GetPluginData(appDefinition.scope);
            if (foundPluginVersion) {
                gs.print("Found installed plugin version " +foundPluginVersion + " for " + appDefinition.name + ";" + appDefinition.version + "; " + appDefinition.scope);
            } else {
                gs.print("WARNING: No plugin or app data found for " + appDefinition.name + ";" + appDefinition.version + "; " + appDefinition.scope);
            }
            
        } else {
            gs.print("No local app store data found for " + appDefinition.name + ";" + appDefinition.version + "; " + appDefinition.scope);
        }
    }

}

function GetDependencyAppData (dependenciesString) {
    //dependency string example:
    //sn_sec_cmn_orch:11.4.0,sn_ti_seismic:1.0.0

    var appData = [];
    if (JSUtil.nil(dependenciesString)) {
        return appData;
    }
    var deps = dependenciesString.split(',');
    for (var depPos = 0; depPos < deps.length; depPos++)
    {
        var depDefinition = {};
        var fields = deps[depPos].split(':');
        depDefinition.scope = fields[0];
        depDefinition.version = fields[1];
        var storeAppData = GetLocalAppData(depDefinition, true);
        appData.append(appData, storeAppData);
    }
    return appData;
}

function GetPluginData (scope) {
    var result;
    if (scope) {
        var grPluginData = new GlideRecord('sys_plugins');
        grPluginData.addQuery('source', scope);
        grPluginData.setLimit(1);
        grPluginData.query();
        if (grPluginData.next()) {
            result = grPluginData.getValue('version');
            if (!result) {
                result = "empty (but present)";
            }
        }
    }
    return result;

}

function GetLocalAppData (appDefinition, wantSysStoreApp) {
    var localAppData = [];
    var grAppData;
    if (wantSysStoreApp) {
        grAppData = new GlideRecord('sys_store_app');
    } else {
        grAppData = new GlideRecord('sys_remote_app');
    }
    var minMet = false;
    if (JSUtil.notNil(appDefinition.scope)) {
        minMet = true;
        grAppData.addQuery('scope', appDefinition.scope);
    }
    if (JSUtil.notNil(appDefinition.appId)) {
        minMet = true;
        grAppData.addQuery('sys_id', appDefinition.appId);
    }
    if (JSUtil.notNil(appDefinition.name)) {
        minMet = true;
        grAppData.addQuery('name', appDefinition.name);
    }
    if (JSUtil.notNil(appDefinition.version)) {
        grAppData.addQuery('version', appDefinition.version);
    }
    if (!minMet) {
        gs.print("insufficient data provided for app");
        return localAppData;
    }
    grAppData.query();
    
    while (grAppData.next()) {
        var row = {};
        var keys = Object.keys(grAppData);
        for (var pos = 0; pos < keys.length; pos++) {
            row[keys[pos]] = grAppData.getValue(keys[pos]);
        };
        localAppData.push(row);
    }
    return localAppData;
}


function VerifyUserHasRequisites() {
  var currentDomainId = GlideDomainSupport.getCurrentDomainValueOrGlobal();
  var currentAppId = gs.getCurrentApplicationId();
  var wantAppId = 'global';
  var wantAppName = 'global'; 
  var response = false; 
  if (currentDomainId == 'global' && currentAppId === wantAppId) {
    response = true;
  } else {
    if (currentDomainId != 'global') {
    gs.print ("Your current domain is NOT global.  Please change your domain to global.  If your domain picker already indicates global, please change to TOP and then back to global.")
    }
    if (currentAppId !== wantAppId) {
      gs.print ("You are not in the required application scope.  Please change application scope to " + wantAppName + " and try again.");
    }
  }
  return response;
}

```

In your first execution, targetApps will contain just your subject application.

```javascript
var targetApps = [
        {
            scope: "sn_intune_integrat",
            name: "Service Graph Connector for Microsoft Intune",
            appId: "a1ce9646683e0050f8774bfad236c981",
            version: "2.0.0"
        }
    ];
```

```example first iteration in sandbox:
*** Script: Found local app data for name(Service Graph Connector for Microsoft Intune)/scope(sn_intune_integrat)/version(2.0.0):
*** Script:     dependencies com.snc.itom.license:sys,com.snc.itom.discovery.license:sys,com.glide.hub.action_type.datastream:sys,sn_cmdb_int_util:2.5.0,sn_cmdb_ci_class:1.16.0
```

Update targetApps for every every identified dependency.

**dependency walking is still manual** (you must update and rerun script through multiple iterations):

* App store data and functionality (and DXC knowledge of it) changes rapidly.
* This process is intended to give an accurate assessment of dependencies.
* Automatic parsing of dependencies will result in lost awareness as things change.
  It is better for dev teams to discuss found issues with GTL team.

Based on dependencies found for sn_intune_integrat

```text
com.snc.itom.license:sys,com.snc.itom.discovery.license:sys,com.glide.hub.action_type.datastream:sys,sn_cmdb_int_util:2.5.0,sn_cmdb_ci_class:1.16.0
```

We find 3 plugins and 2 applications.
*plugins have a version of "sys"*

com.snc.itom.license:sys

com.snc.itom.discovery.license:sys

com.glide.hub.action_type.datastream:sys

sn_cmdb_int_util:2.5.0

sn_cmdb_ci_class:1.16.0

Update targetApps with all found dependencies and rerun script.
Every found dependency, including dependencies of dependencies, must be
represented in targetApps.  targetApps can grow quite long.

```javascript
var targetApps = [
        {
            //dependency of sn_intune_integrat
            scope: "com.snc.itom.license",
            name: "",
            appId: "",
            version: "sys"
        },
        {
            //dependency of sn_intune_integrat
            scope: "com.snc.itom.discovery.license",
            name: "",
            appId: "",
            version: "sys"
        },
        {
            //dependency of sn_intune_integrat
            scope: "com.glide.hub.action_type.datastream",
            name: "",
            appId: "",
            version: "sys"
        },
        {
            //dependency of sn_intune_integrat
            scope: "sn_cmdb_int_util",
            name: "",
            appId: "",
            version: "2.5.0"
        },
        {
            //dependency of sn_intune_integrat
            scope: "sn_cmdb_ci_class",
            name: "",
            appId: "",
            version: "1.16.0"
        },
        {
            scope: "sn_intune_integrat",
            name: "Service Graph Connector for Microsoft Intune",
            appId: "a1ce9646683e0050f8774bfad236c981",
            version: "2.0.0"
        }
    ];
```

Below is the output of a script where targetApps has been updated to include all identified dependencies.

example output of completed script in sandbox
- **The completed dependency script should be attached to the story**
- **output of completed dependency script in sandbox should be documented in the story**

```text
*** Script: Found installed plugin version 1.0.0 for ;sys; com.snc.itom.license
*** Script: Found installed plugin version 1.0.0 for ;sys; com.snc.itom.discovery.license
*** Script: Found installed plugin version 1.0.0 for ;sys; com.glide.hub.action_type.datastream
*** Script: WARNING: An imprecise version (2.6.3) match was found for ;2.5.0; sn_cmdb_int_util
*** Script: Found local app data for name(Integration Commons for CMDB)/scope(sn_cmdb_int_util)/version(2.6.3):
*** Script:     no dependencies indicated
*** Script: WARNING: An imprecise version (1.31.0) match was found for ;1.16.0; sn_cmdb_ci_class
*** Script: Found local app data for name(CMDB CI Class Models)/scope(sn_cmdb_ci_class)/version(1.31.0):
*** Script:     no dependencies indicated
*** Script: Found local app data for name(Service Graph Connector for Microsoft Intune)/scope(sn_intune_integrat)/version(2.0.0):
*** Script:     dependencies com.snc.itom.license:sys,com.snc.itom.discovery.license:sys,com.glide.hub.action_type.datastream:sys,sn_cmdb_int_util:2.5.0,sn_cmdb_ci_class:1.16.0
```

The identical script should then be run in DEV.  Take note of the differences.  Those are your dependencies.

example output of completed script in cscdev:
- **output of completed dependency script in cscdev should be documented in the story**

```text
*** Script: Found installed plugin version 1.0.0 for ;sys; com.snc.itom.license
*** Script: WARNING: No plugin or app data found for ;sys; com.snc.itom.discovery.license
*** Script: Found installed plugin version 1.0.0 for ;sys; com.glide.hub.action_type.datastream
*** Script: WARNING: An imprecise version (2.4.1) match was found for ;2.5.0; sn_cmdb_int_util
*** Script: Found local app data for name(Integration Commons for CMDB)/scope(sn_cmdb_int_util)/version(2.4.1):
*** Script:     no dependencies indicated
*** Script: WARNING: An imprecise version (1.32.0) match was found for ;1.16.0; sn_cmdb_ci_class
*** Script: Found local app data for name(CMDB CI Class Models)/scope(sn_cmdb_ci_class)/version(1.32.0):
*** Script:     no dependencies indicated
*** Script: WARNING: No plugin or app data found for Service Graph Connector for Microsoft Intune;2.0.0; sn_intune_integrat
```

From the above, we see:

1. missing plugin com.snc.itom.discovery.license.
2. A different version of sn_cmdb_int_util.  The version in dev (2.4.1) is lower
  than the dependency version (2.5.0). This means this app will be upgraded by
  installing your app.
3. A different version of sn_cmdb_ci_class in dev (1.32.0) compared to sandbox
  (1.31.0), but still higher than the dependency version (1.16.0).

Dev team must understand the dependencies, including working with responsible
Platform X ServiceNow teams, before an application can be installed.
