# PDXC Code Guidelines for ServiceNow Service Portal

*[Back to PDXC ServiceNow Developer Guidelines](Developer%20Code%20Guidelines%20DXC.md)*

These guidelines should be followed by all development teams working with Service Portal to maintain consistency in the PDXC ServiceNow code base. These guidelines take into account a global development structure with developers in regions as diverse as Australia, India, Europe and North America, working in the same development environment. The teams are responsible for specific application areas within the platform.  The Daleks team is responsible for maintenance and enhancements for Service Portal.

## Contents

* [Service Portal Introduction](#service-portal-introduction)
* [Service Portal Configuration](#service-portal-configuration)
* [Service Portal Development](#service-portal-developement)
* [Naming Standards](#naming-standards)
* [Service Portal Pages](#service-portal-pages)
* [Widget Cloning](#widget-cloning)
* [Catalog Item Remediation](#catalog-item-remediation)
* [Translation Support](#translation-support)

---

## Service Portal Introduction

Service Portal is the self-service portal for end users of ServiceNow, developed and deployed in 2018-2019.  Service Portal replaces CMS Portal with new technologies and methodologies.  The Service Portal is powered by three primary technologies: AngularJS, Bootstrap and Sass.

An overview of Service Portal is here:
**External Reference:** [Service Portal Overview](https://dxcportal.sharepoint.com/sites/SMDevStudio/Shared%20Documents/Forms/AllItems.aspx?viewpath=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FService%20Portal%20%28SP%29%20MVP%201%2E0%2FNew%20PDXC%20SP-Overview%2Epdf&parent=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FService%20Portal%20%28SP%29%20MVP%201%2E0)*.*

---

## Service Portal Configuration

Instructions for configuring Service Portal for a client are in the self-service section of the Account Transition Guidelines here:
**External Reference:** [Service Portal Configuration](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/blob/master/SelfService.md)*.*

---

## Service Portal Developement

This Service Portal document is intended to build off the existing PDXC Developer documentation found here. 
https://github.dxc.com/ServiceNowDXC/Documentation

Service Portal code development relies heavily on widgets, and a widget's basic components are HTML, CSS and JavaScript code that can run on both the client and server side.  The following JavaScript training path is recommended before starting widget development.

1. Courses on DXC University
    * JavaScript Essentials: Getting Started
  (ID: 00023836)
    * JavaScript Essentials: JavaScript In Depth
  (ID: 00027426)
  Skip over the Xpath and XSLT sections
2. Successful completion of Code Academy’s [JavaScript](https://www.codecademy.com/learn/introduction-to-javascript) course is recommended
3. Review the ServiceNow [JavaScript Primer](https://www.servicenow.com/content/dam/servicenow/other-documents/training/ServiceNow-JavaScript-Primer.pdf)
    * There are additional Resource links at the end of this document, that have additional sources of good Java Scripting tutorials
    * W3Schools JavaScript Tutorials: https://www.w3schools.com/js/default.asp is a good resource to review and to use as an ongoing reference.
4. For ServiceNow specific scripting, take the “Scripting in ServiceNow” free developer training: https://developer.servicenow.com/app.do#!/trainlist/app_store_learnv2_scripting_london_scripting_in_servicenow?v=london
    * Once this course completed, optionally you can enroll in the ServiceNow Advanced Scripting class, which is a formal 3 day ServiceNow course.

---

## Naming Standards

**Widgets**
The primary building blocks of Service Portal are widgets.  Widgets contain a majority of the code modifications made to the DXC Service Portal.   There are many Out of the Box widgets, and to assist in upgrade processes and name collisions, we have a naming standard applied to all Widgets.  The widget "Name" will start with [Custom] followed by the name of the widget.  The widget ID will be prefixed with 'custom-' + \<widget id\>.

For example, a Widget cloned from the "Search Page" widget, would have the following name:

| Name | Customized Name | Customized ID |
| --- | --- | --- |
|Search Page | [Custom] Search Page | custom-search-page |

**Angular ng-templates**
If a widget needs to be cloned, the Angular Templates will need to be cloned and renamed as part of that process.  The same Angular Template cannot have a reference to two different widgets, since the ID is a unique key.  The Angular Templates will be prefixed with the ID of "custom-" followed by the rest of the ID.  These records are stored in the `sp_ng_template` table.

**Widget Instances**
It is a best practice to provide every widget instance with a proper Title.  This field is not a required field, but it is expected that each instance will have a title.  That title should reflect the widget it represents, so in most cases we will want to set the widget instance Title to the name of the related Widget.

---

## Service Portal Pages

When **cloning** an Out of the Box Service Portal page, there are some special consideration.  We do not update OOTB pages, but we will clone a page and modify that clone.  As the page "id" field is unique, you must rename the page to be cloned first.  That name should be <page_id>_original, with the _original added as a postfix.  Once that is saved, you can perform the "Clone Page" UI action on the page and give this new clone the name of the original page id.  By default ServiceNow will add the "Copy Of" to the Title and ID, so that will have to be updated.  We need to keep that original page id, since that page id is embedded in many widgets and navigations within Service Portal

**Migration Note:** This clone must be captured in two different update sets, such that we capture the rename first.  This way we will avoid commit errors for unique key violations on the page id field.

### Common Service Portal Pages

The following are some common Service Portal pages that are currently being used.

| SP Page &quot;id&quot; | Comments |
| --- | --- |
| index | Regarding the [Custom] Homepage Search, this widget has an embedded widget &quot;[Custom] Typeahead search&quot; that is responsible for performing the typeahead search.  Generally, this widget is known not to cause any problems; however many search issues can be tested by using this search. The widget [Custom] Knowledge Featured Articles builds a list of featured articles (to set a KB article as **featured** , a &quot; **homepage**&quot; **keyword** needs to be associated to such KB article; usually in the **Knowledge Database Related lists** ).  The list is displayed in a 3 x 3 layout, and they are sorted by &quot;sys\_updated\_on&quot;.  More details on the server script of that widget The widget [Custom] SP Quick Picks get&#39;s the user&#39;s company quick picks from the SP Quick Picks table (u\_sp\_quick\_picks) by using a function _getServicePortalQuickPicks_ found in the _CSCPortalHelper_ Script include.   |
| log\_ticket | This widget embeds a widget named **[Custom] Report SC Catalog Item** (with widget id = **custom-report-sc-catalog-item-v2** ). This widget is known to cause problems, and this is mostly due to ServiceNow adding features to the Service Catalog and updating existing Service Catalog modules.  **Whenever we do system upgrades (i.e. Kingston to London), look out for this widget --- it is prone to hang when submitting the form.  To solve problems with this widget a**  **merge between our custom code and ServiceNow&#39;s**  **usually fixes the problem.** |
| kb\_home | This knowledge homepage (and Knowledge Management in SP in general) does not receive a lot of rework but mainly new features. The main element here that can receive attention is the [Custom] Knowledge Homepage Search.  The widget itself (the embedded [Custom] Typeahead search in it) should not cause huge problems but Knowledge searches might.   **If something breaks within Knowledge search** (which will also be reflected in Global Search) a good starting point for troubleshooting would be the **Knowledge Results search source**. |
| sc\_landing | This widget was adapted from the Service Catalog CMS Homepage (CMS Page **service\_catalog\_v2** ).  The code that get&#39;s the user&#39;s most popular items, which is mainly based on company&#39;s most popular, was adapted for Service Portal. However, because this code needs to do a certain computation (our algorithm to compute Most Popular), the widget was made so that it loads asynchronously with the page.  In other words, when a user navigates to this page, the Most Popular items will not be present, but within a few seconds the widget will have them displayed. Use GlideRecord::canRead() to determine if a user is entitled to see the catalog item.  If the user can read the catalog item, count it, and move to the next one. |
| check\_status | This widget has an option **Tables** (with **default value** ) in the Option&#39;s Schema (covered in the SP Basic or Advanced training) that receives a JSON String containing all the labels (translated messages, from **sys\_ui\_message** table).  This JSON string also contains the queries that we can see in the **myTicketsAjax** script include that was discussed earlier in the CMS Portal section. We then created an instance of the widget,  The widget then loads and parses the default value of the JSON string.  The good thing about this is that all the queries, messages, etc. are contained now in the widget; no need to make a GlideAjax call to the server.  **If something breaks with the queries** check the _Tables_ option in the option schema of the widget; look for the queries.  Copy/paste the JSON into a JSON &quot;visualizer&quot; that can format the JSON in a readable manner for you.  **If we need to add an additional tab to any of the table views** , take a good look at how we are currently doing it for one of the tabs and replicate it.  You should copy/paste a substring of the entire JSON String (containing the label, query, table name, etc) and replace the redundant data with the new data (i.e. label, query, table name).  The reverse operation would be done if we would like to **remove a tab** ; a careful deletion of a substring of the JSON string should work well enough. |
| cloud | In this page we are using the exact same widget that we use for the SP Page id= **check\_status** (read previous row, above in this table).  However, we use the widget in a different manner. For this page, we added an option **Exec** to the Option&#39;s Schema with **no** default value, with the intention that when we create an instance of this widget, we fill in this Option value --- **if we don&#39;t want to get the default data table that we would otherwise get** (read SP Page check status above). |
| ad\_pwd\_reset | This is pretty much a static page displaying an iframe with _src_ targeting the ServiceNow OOTB page for password reset.  It is very similar to how we do Password Reset in CMS.  **There&#39;s a**  **redirection exception**  **in this page, read more details in the Redirection section.**  The redirection exception is there to allow the user to use the page without getting booted to the SP Homepage. |
| my\_surveys | This page has been **slightly** modified so that we could add our customizations (like CSS Styling, Breadcrumb updates, etc.). This page usually does not get too much attention or rework.   **Watch out for** system upgrades.  This page could break if ServiceNow upgrades the Service Portal Surveys system.  In that case, a **revert-merge** to OOTB with **our** customizations adapted to the widget should fix the problem. |
| isec\_ops | This page is a recent addition to our portal, it&#39;s based in the same construction as in the **Password Reset page (ad\_pwd\_reset)** in that it has a **left nav** menu widget, and the **iframe** widget. The redirection exception is there to allow the user to use the page **and drill down** without getting booted to the SP Homepage. |
| search | This is the **global search** page.  Whenever the user searches for something in the Homepage Search widget **OR** in any of the **Typeahead Search widgets in the breadcrumbs**, that user will be taken to this page to see the search results. Other than the CSS Styling and Template customizations that we have added, this widget is configured to go through all our search sources for SP.  **If anything regarding SP Searches breaks** it&#39;s very likely that we can see the issue in this page.  In that case, to solve the search problem, **a good place to start** looking is the **search sources** |
| sc\_cat\_item | This page does not usually receive major customizations, **but it is known to break** whenever we do a system upgrade.  **The fix for this** usually is to do a **revert-merge** OOTB with **our** customizations.  ServiceNow updates and adds features to this area very often and any update to this area can break it.  |
| cloud\_details | This page is very similar to other pages, but the important element in this page is the Cloud Actions widget, specifically the visibility to the UI Actions. |

---

## Widget Cloning

Do not update Out of the Box widgets.  Any new functionality required that cannot be accomplished via configuration of a widget instance option will require a Clone of the widget.  The widget should be cloned using the "Clone Widget" form action.  Make sure to provide the proper name and id, as described in the widget naming standards.  If required, the widget dependencies, Angular providers and Angular ng-templates will need to be manually related back to the cloned widget.
**Note:** Consult with your technical lead before cloning a widget.

---

## Catalog Item Remediation

During the move from CMS to Service Portal, some functionality available with Catalog Items has had to change or be lost.  The Service Catalog will work for a majority of items, but since Service Portal was designed to be responsive and mobile-compliant, some features will not work.  These issues are discussed in more detail [here...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Catalog%20Developers/Service%20Portal%20Compatibility%20Issues.md)

---

## Translation Support

All client-visible data is required to be translated into the preferred language of the logged-on user.  PDXC ServiceNow supports over 20 languages, so translations are required for each supported language.  

For Service Portal specifically, the instructions are as follows:

### SP Header Menu Translations

Menu Item labels are stored in a field with a translated_text field type. This means in order for it to be translated, a translation record must exist on the Translated Text `sys_translated_text` table.

* Select System Localization > Translated Text on the Application Navigator
* Table Name : Menu Item [sp_rectangle_menu_item]
* Language : (Language Value to Configure)
* Document:  Select menu item to translate
* Field Name : Label
* Value : (translation)

### SP Widget Instance Translations

Some widget instances require their Title field to be translated.  This will occur when the supporting widget references that instance option to render content within the HTML.  For example, this server script code will force a translation to occur on the widget instance option title field.

```javascript
data.placeholder = gs.getMessage(options.title);
```

Widget Instance translations for title are stored in `sys_translated_text` table.

* Select System Localization > Translated Text on the Application Navigator
* Table Name : Menu Item [sp_instance]
* Language : (Language Value to Configure)
* Document:  Select Widget Instance to translate
* Field Name : Title
* Value : (translation)

### SP Widget Translations

The most common place to provide translation support will be in the Widget.  Content can be translated in the HTML, Client or Server side scripts.  Any HTML content that is contained within ${} will be translated.  For example

```html
    <h4 class="panel-title pull-left">
      ${Watch list}
    </h4>
```

The value of "Watch list" will be looked up by "key" in the `sys_ui_message` table.

Translating strings in the server script:  This is the safest way to fetch a translated message on the server-side.  Use `gs.getMessage()` syntax to tag strings for translations.  

**Note:**  Translations for strings go into `sys_ui_message`, which is where the majority of translations occur.  If the translation occurs against a field type of `translated_text`, the translations will be looked up in the `sys_translated_text` table, based on the Document Key and field name, as is the case for the widget instance title field.

**Warning:** Only translate strings in one location.  Do not apply translation syntax in both the HTML and Server side, as you will get a double translation effect, and nothing will translate.

---

## PDXC ServiceNow Code Development Framework

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice                                                                                                               |
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |
