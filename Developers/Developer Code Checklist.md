DEVELOPER’S AND REVIEWER'S CHECKLISTS
======================

## DEVELOPER'S CHECKLIST

### BEFORE DEVELOPMENT
  
| #  | Check     |
|---|--------------------------------------------------------------------|
| 1  | Has an update set been created for the Story?                                                                                                                                                                 |
|  2 | Does the update set follow DXC’s naming convention?                                                                                                                                                           |
| 3  | Is the developer’s profile language set to “English (Webster’s)”?                                                                                                                                             |
|  4 | Does the developer have his/her own profile for development with admin access on Sandbox & Development server?                                                                                                |
| 5  | Has the TOP domain been selected before starting the development? (There are exceptions to working in the TOP domain. Business Rules on non-domain separated table usually have to live at the global level). |
| 6  | Have the Story point estimates been entered in the Story?                                                                                                                                                     |
| 7  | Is the grooming task in the Story marked as Complete?                                                                                                                                                         |
### DURING DEVELOPMENT

| #  | Check     |
|---|--------------------------------------------------------------------|
| 8  | Do the code changes follow DXC’s code development guidelines?                                                                     |
| 9  | Do the code changes follow ServiceNow code development guidelines? |

### AFTER DEVELOPMENT

| # | Check                                                     |
|---|--------------------------------------------------------------------------------------------------------------------------------------------|
| 10  | Have all the unnecessary objects, created during the Story development, been deleted?                                                      |
| 11  | Have all the unnecessary objects, which were modified during development but are no longer required, been rolled back?                     |
| 12  | Have all the log statements (such as gs.log) been removed from the code?                                                                   |
| 13  | Has the Story been updated with sufficient details of the technical solution?                                                              |
| 14  | Was the unit testing sufficient to cover all scenarios?                                                                                    |
| 15  | Did the functionality pass and meet the objectives in the unit testing?                                                                    |
| 16  | Are there any related functionalities which should be highlighted to the testing team, so that the additional functionality can be tested? |
| 17  | Has the development task in the Story been marked as ‘Complete’?                                                                           |
| 18  | Is the Story State marked as 'Development Complete' by the Scrum Master or Application Engineer once the Product Owner has approved the Demo? |

## REVIEWER'S CHECKLIST

This checklist should be followed by reviewers while reviewing code developed by a peer:

### GENERAL

| #  | Check                                                     |
|---|----------------------------------------------------------------------------------------------------------------|
| 1  | Does the update set follow the standard naming convention?                                                     |
| 2  | Are the objects modified in the correct domain (TOP, with a few exceptions)?                                   |
| 3  | Do all attachments (xml, csv, etc.) follow correct naming convention and have they been attached in the Story? |

### DESIGN

| #  | Check   |
|---|-------------------------------------------------------------------------------------------------|
| 4  | Has the best approach, amongst various similar approaches, been used for the solution design?   |
| 5  | Does the code follow principles of extensibility, maintainability, scalability and readability? |
| 6  | Could there be any security related issues because of the design?                               |

### CODE

| #  | Check                    |
|---|---------------------------------------------------------------------------------|
| 7  | Does the code achieve the intended objectives of the Story?                     |
| 8  | Does the code follow basic guidelines on white spacing, comments and indenting? |
| 9  | Does the code follow [DXC’s code development guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md) ?                         |
| 10  | Does the code follow [ServiceNow’s code development guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md)?                  |
| 11  | Is the code (or functionality) considerably hardcoded?                          |
| 12  | Could there be any performance related issues because of the code?              |

### UNIT TESTING

| #  | Check       |
|---|-------------------------------------------------------------------------|
| 13  | Was the unit testing sufficient to cover all relevant scenarios?        |
| 14  | Did the functionality pass and meet the objectives in the unit testing? |
|   | Has the unit test document been updated and attached with the Story?    |

### CODE MIGRATION

| #  | Check                                                                    |
|---|-----------------------------------------------------------------------------------------------------------------------------|
| 15  | Has the migration information been updated in the Story?                                                                    |
| 16  | Does the update set only contains objects which are part of the Story and intended for migration?                           |
| 17  | If the Story has a dependency on another Story, object, data or catalog item, was the dependency resolved before migration? |
| 18  | Was any error encountered during migration of the update set?                                                               |

### TECHNICAL STORY (Optional)

| #  | Check |
|---|----------------------------------------------------------------------------|
| 19  | Is a new technical Story required to improve/fix any part of the solution? |

------------------------------------------


PDXC ServiceNow Code Development Framework
==========================================

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice                                                                                                               |
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |


