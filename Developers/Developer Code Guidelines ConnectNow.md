# PDXC Code Guidelines for ConnectNow

*[Back to PDXC ServiceNow Developer Guidelines](Developer%20Code%20Guidelines%20DXC.md)*

## Contents

* [Introduction](#introduction)
* [ConnectNow Developer Guidelines](#connectnow-developer-guidelines)
* [ConnectNow Documentation](#connectnow-documentation)
* [How to Contribute](#how-to-contribute)

## Introduction

The ConnectNow subsystem of PDXC ServiceNow is the primary mechanism for
integrating PDXC SN instances with external systems.  ConnectNow is proprietary
to DXC, and is a fundamental component of PDXC ServiceNow.

This is a placeholder document which will be fleshed out in the fullness of time.
Please check back any time you need to make changes in this space, as the then-current
guidelines will be the measure by which changes will be reviewed.

## ConnectNow Developer Guidelines

### All ConnectNow development must be reviewed with, and accepted by, the OEE Core Integrations team before promotion to devtest.  

  Refer to [How to Contribute](#how-to-contribute) section.  This includes:
  * All changes to ConnectNow framework
  * All changes to existing or new partner definitions, capabilities, transform maps, etc...

### All integrations must have a clearly documented support model, for both development stack and production.  
  No CI/CD pipelines without understanding all engagement points.

### All integrations must include documentation
  Generally, these instructions are called Installation and Configuration Guides (ICG),
  and address onboarding and run-and-maintain activities required by operational teams.
  For any doubts regarding form, location, and suitability, please ensure the PDXC OOSS
  function or appropriate SRE teams are engaged.

## ConnectNow Documentation

Coming soon

## How to Contribute

### Generally making changes

ConnectNow changes are made in PDXC ServiceNow.  All content is supported by
PDXC ServiceNow's development and release processes.

### When your team has a ConnectNow dependency

Engineering technical leads with components that have a ConnectNow dependency
should engage with the OEE Core Integrations team via the
[IntegratingWithConnectNow](https://teams.microsoft.com/l/channel/19%3a35a54f0d734f417db5e3838c9ab43ff6%40thread.skype/IntegratingWithConnectNow?groupId=f9550240-335a-47d5-88fc-8003e5e4a471&tenantId=93f33571-550f-43cf-b09f-cd331338d086)
channel in the
[PDXC SN Technical Leads](https://teams.microsoft.com/l/team/19%3ac1a29c138d8943b789617b098a0aa9ba%40thread.skype/conversations?groupId=f9550240-335a-47d5-88fc-8003e5e4a471&tenantId=93f33571-550f-43cf-b09f-cd331338d086)
team.
