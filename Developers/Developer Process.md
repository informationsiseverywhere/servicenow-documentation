[Introduction](#introduction)

[Methodology](#methodology)

[Development Process](#development-process)

[Code Development and Review Cycle](#code-development-and-review-cycle)


# INTRODUCTION

The purpose of the PDXC ServiceNow Developement Framework is to describe agreed standards that should be followed while designing and developing PDXC ServiceNow solutions.

The framework provides a set of guiding principles, with the goal of developing applications with a common set of core methodologies and techniques that will aid in the development of robust and maintainable applications. The framework includes ServiceNow's own best practice guidance.

Audience
--------

-   OE&E PDXC SN developers and Leads.

-   Any other developers working within Platform DXC (PDXC) ServiceNow, including external collaborators.

CODE DEVELOPMENT FRAMEWORK
==========================

Methodology
-----------

The code development framework comprises:

- general best practices defined by ServiceNow
- general best practices defined by PDXC, and the PDXC SN code and platform owners
- recommended programming styles, practices, and methods for each aspect of code development to support PDXC ServiceNow offerings

Developers must follow these guidelines to build high quality code, help improve the readability of code, and make code maintenance easier.

![Framework](../images/DevFramework.png)

Development Process
-------------------

DXC has multiple PDXC SN engineering development teams, located at various
locations globally, who are responsible for designing, delivering and
maintaining high quality code, using Agile methodology.

### Agile Methodology

The iterative, incremental approach of Agile, with a very short feedback loop
and adaption cycle means that PDXC SN production releases are scheduled every 2 weeks.
The different development teams (scrum teams) use Agile methodologies such as
Scrum and Kanban.

### Development Environments

All development teams perform development tasks on a shared infrastructure. From
the development perspective, only three environments are involved:

-   **SANDBOX INSTANCE**: Used by developers to check feasibility, or perform a
    demonstration in principle with the aim of verifying some concept.

-   **DEVELOPMENT INSTANCE**: Used by developers for developing Stories,
    building enhancements and fixing defects. Code is NOT moved from the Sandbox
    to the Development instance.

-   **TEST INSTANCE**: Referred to as “DevTest”, this instance is used by
    testers to test Stories developed by developers. Code is moved from Dev to Test.

### Code Development And Review Cycle

DXC follows established code development processes, including clear guidelines
for consistency amongst teams and multiple levels of reviews and testing for
ensuring deliverables of the highest quality.

This is the high-level summary of the development cycle followed:

![Framework](../images/DevelopmentFlow.png)

\* Note: DevQA - used by GDN testers and capability teams to test code before release to PreProd and Prod servers

This cycle is facilitated by the use of the *Story*, *Defect,* and *Release* modules in the customised *Agile Development* application in ServiceNow.

-   **CODE DEVELOPMENT (BY DEVELOPER ON SANDBOX/DEV SERVER)**:

    -   Development of a Story consists of two phases - grooming of requirements
        followed by code development.

    -   In case grooming requires checking of feasibility and evaluating impact
        of code change, it is done in the Sandbox environment. Else, the
        development is directly done on Development instance. *[Note, when new
        developers are still training, their activity should always be carried
        out in the Sandbox environment first].*

    -   Any complex development should always be discussed with the development
        team Tech Lead in terms of approach before the actual development on the
        dev server

-   **CODE REVIEW BY REVIEWER 1 (PEER REVIEW ON DEV SERVER - OPTIONAL):**

    -   Once the solution has been developed, it may be reviewed by a peer in
        the same development team.

    -   Any suggestions or feedback is sent to the developer and is incorporated
        in the next review cycle.

-   **CODE REVIEW BY REVIEWER 2 (TECH LEAD REVIEW ON DEV SERVER)**:

    -   Code should be reviewed by the Tech Leads designated for the development
        team.

    -   Any suggestions or feedback are sent to the developer and incorporated
        in the next development cycle.

    -   If the Story passes review, it is migrated to the DevTest environment
        (typically by the Tech Lead or developer), and the testers are informed
        so they can begin testing.

-   **TESTING BY DEVELOPMENT TEAM TESTERS (ON DEVTEST)**:

    -   First round of testing by the development team tester(s).

    -   Any observations or defects are sent to the developers for correction.
        The development cycle repeats until the development team are satisfied
        with the Story..

-   **CODE REVIEW BY REVIEWER 3 (GLOBAL TECH LEAD REVIEW ON DEVTEST)**:

    -   Once the solution passes testing on DevTest, the Story is reviewed by the
    Global Tech Lead as part of a general release building process

    -   Any suggestions or feedback are sent to the developer. Depending on the
    nature of the feedback, re-work may be required and the development cycle
    repeated.

-   **TESTING OF THE PDXC SN RELEASE (ON DEVQA)**:

    -   Testing is carried out in DevQA.

    -   Any defects are raised by the tester within the Defect module in ServiceNow, and initially triaged by the Application Engineer.

    -   If validated as a true defect, a new Story is created by the Application Engineer and is assigned to the developers for correction; the new Story is incorporated in another development cycle. Liaison with the Product Owner will determine priority.

-   **RELEASED TO PRODUCTION**:

    - Story once tested and verified is released to production and made available to the user community.
    
------------------------

    
PDXC ServiceNow Code Development Framework
==========================================

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |
------------------
