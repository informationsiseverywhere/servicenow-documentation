TEMPLATES
=========
The suggested templates below assist developers and reviewers in communicating in a consistent and standardised way.
They are not mandatory, but their use can help to improve efficiency within a team, and avoid repeated requests for more information.

[Unit Test Template](#unit-test-template)

[Code Review Email Template](#code-review-email-template)

[Migration Information Template](#migration-information-template)

[Grooming Analysis Template](#grooming-analysis-template)

## Unit Test Template

A similar template as below (created in Excel) can be used by developers to
document the unit test results on the Dev server:

| **No.** | **Unit Test Scripts** | **Configuration/Test Data** | **Expected Result** | **Actual Result** | **Pass/ Fail** | **Comments** |
|---------|-----------------------|-----------------------------|---------------------|-------------------|----------------|--------------|
| 1       |                       |                             |                     |                   |                |              |
| 2       |                       |                             |                     |                   |                |              |
| 3       |                       |                             |                     |                   |                |              |
| 4       |                       |                             |                     |                   |                |              |

The results are helpful to the reviewer(s) to ascertain whether necessary
testing has been performed covering all scenarios. The sheet should be uploaded
to the Story so that its test scripts, test data and test results can be
referred by other developers, reviewers and testers.

## Code Review Email Template

This template can be used for requesting a code review. Simply copy and paste
into an email:

*Email Subject:*

\<Story Number\> is ready for your review on Dev

*Email Body:*

Update set \<update set name with link\> for Story \<Story number with link\> is ready for your review on Dev.

**Update Set Details**:

-   Update Set Name:

-   Count of objects modified:

**Story Details**:

-   Summary of the objectives:

-   Existing functionality:

-   New functionality:

**Code Change Details**:

-   \<Object Name 1\>: Enter summary of the code change

-   \<Object Name 2\>: Enter summary of the code change

**Unit Test Details:**

-   Unit Test Performed on Dev? \<Enter **Yes** or **No**\>

-   Unit Test Result Sheet attached in the Story? \<Enter **Yes** or **No**\>

## Migration Information Template

**No template** - only guidance.  Refer to example below*

1. Ensure all objects follow [Migration Information Naming Conventions](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md#migration-information-naming-conventions).
2. List each item to be migrated, one per line, in the order that it must be committed. **Do not use bullets, numbers, or other clarifying text, with the below exceptions:**
    1. **If** there is a known reason to accept or skip remote updates during commit, these should be noted, including the reason, on the same line as the affected update set.  Skip/Accept handling is exceptional, and is normally not needed.
    2. Update sets are packaged for release based on the order they were committed to devtest.  If you know sequence must be explicitly managed in a different order, this should be documented.  Releasing objects in a different sequence than they were applied to devtest is exceptional, and is normally not needed.
2. Document known dependencies or required relationships to stories **which have not already been released**.
3. If there is additional information required to understand non-migration-specific aspects of items included in the release, that should be documented elsewhere in the story, and not in migration information.

**example**
```migration information example
plugin foo.foundation
Store Application com.snc.foo ver 1.2.3
DoTheBeforeThings_STRY124_before.txt
update_set_1
update_set_2 (accept remote update - object xyz is commonly changed in local instance)
u_table1_data1_stry124.xml
u_table2_data2_stry124.xml
DoTheAfterThings_STRY124_after.txt

Must be in same release as STRY123
Depends on STRY125 - can be in same or later release.
Enables STRY126 - can be in same or earlier release.
```


## Grooming Analysis Template

This template can be used for updating the grooming task in the Story so that it can be referred to by other developers in the future:

**GROOMING ANALYSIS:**

**Existing functionality**

-   Enter details of functionality which already exists in the system

**New functionality requested in the Story**

-   Enter details of the new functionality which has been requested in the
    stories objectives

**Root cause (if applicable)**

-   Enter root cause identified during the grooming analysis (if applicable).

-   For OOTB issues, developers are advised to open an incident with ServiceNow
    support.

**Proposed Development approach (if applicable)**

-   Enter details of high level solution for the Story

-   The development approach should be reviewed with the Tech Lead before
    starting the development (where appropriate)

**Queries /Open questions**

-   Enter all assumptions, queries in this section

-   These will be validated by Tech Leads/Application Architect/Service
    Architects based on the nature of the issue

**Initial Effort Estimation**

-   Enter details of high level effort estimate required for developing the
    Story.

-   The effort estimates, however, can change during the course of the
    development of the Story by developer.

-----------------------------

PDXC ServiceNow Code Development Framework
==========================================

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice                                                                                                               |
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |

