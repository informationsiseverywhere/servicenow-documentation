CREATION OF LOCALE
==================

**IMPORTANT:** If you are adding an entirely new language to the definitive list of Platform X ServiceNow supported languages, go to section 6. [New Language](#6-new-language).

ServiceNow provides five tables where the language records are created depending on the requirement.  An example load spreadsheet is provided and should be used as the starting point for all multilingual (ML) translations.

-   Download the [Sample Excel Sheet for ML](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Sample%20Excel%20Sheet%20for%20ML.xlsx?raw=true) file.

[Message](#1-message)|[Choice](#2-choice)|[Label](#3-label)|[Translated Name/Field](#4-translated-name-field)|[Translated Text](#5-translated-text)|[New Language](#6-new-language)
-----|-------|------|------|------|-

## 1. MESSAGE

### PREPARE EXCEL SHEET

![](../images/ExcelTrans01.png)

-   Provide ‘u_table’ as sys_ui_message

-   u_uid field is used to give key message for translation.  Provide the exact English ‘en’ value including any comma/full stop.

-   Include all supported languages in the Excel sheet, including ‘en’.

### SEND EXCEL TO TRANSLATION TEAM 

-   Mail to Claudia Arruda (claudia.arruda\@dxc.com) and your manager.

### LOAD DATA

-   Once received from the Translation Team, copy the Excel sheet data into
    Notepad and make sure there are not any HTML tags.  If present, contact the
    Translation Team.

-   In ServiceNow, click on *Load Data* and select existing table option as
    ‘u_consolidated_translations_import’.

-   Select the Excel sheet with locale value and select the *Submit* button.

![](../images/LocaleLoadData.png)

-   Click on the link "Run Transform".

-   Click on the *Transform* button.

### REVIEW RECORD

-   Verify the records in ‘sys_ui_message’ table and make sure all languages are
    loaded successfully.

### EXPORT XML

-   Go to the message table ‘sys_ui_message’ and search for the message in the
    key field.

-   Export as XML.

-   Add XML to story for import to the Test environment and information for import into the Migration Notes.

### IMPORT XML

-   In the Test environment, navigate to any table list, right-click on table heading and Import XML.

-   ‘Choose file to upload’: The XML file downloaded from the related story.

-   Select the *Upload* button.

### UPDATE THE MASTER LIST

- Add all of the new Service Portal related translations you loaded to the [Portal Translations Master List](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Portal_Translations_Master_List.xlsx) file.

## 2. CHOICE

### GET THE ENGLISH LOCALE

-   Verify the choice value for ‘en’ locale is present in the Dev environment.

### PREPARE EXCEL SHEET

-   **u_table:** Name of the table as ‘sys_choice’.

-   **u_uid:** Start with ‘Table\|Element\|Domain Sys ID\|Value\|Dependent value\|Label’

    -   **Example:** incident\|category\|774190f01f1310005a3637b8ec8b70ef\|Hardware\|6\|label

    -   The ‘Domain Sys ID’ is normally the ‘TOP’ domain, which has a sys_id of ‘774190f01f1310005a3637b8ec8b70ef’.  The other likely domain sys id to use, is the global domain value of ‘195d29bb6feb15001a72ee4dbb3ee48f’.

    -   The last field on this uid string is ‘label’, which is used most of the time and this value should be left in place.  The other possible value is ‘hint’, which is not normal to translate, since hints are not typically on choice values.

-   **Note:** The "Sequence" and "Inactive" flag on the choice list are not provided in the load sheet.  These values are queried from the existing choice list entry and set based on those values.

-   Include all supported languages in the Excel sheet, including ‘en’.

### SEND EXCEL TO TRANSLATION TEAM 

-   Mail to Claudia Arruda (claudia.arruda\@dxc.com) and your manager.

### LOAD DATA

-   Once received from the Translation Team, copy the Excel sheet data into
    Notepad and make sure there are not any HTML tags.  If present, contact the
    Translation Team.

-   In ServiceNow, click on *Load Data* and select existing table option as
    ‘u_consolidated_translations_import’.

-   Select the Excel sheet with locale value and select the *Submit* button.

![](../images/LocaleLoadData.png)

-   Click on the link "Run Transform".

-   Click on the *Transform* button.

### REVIEW RECORD

-   Verify the records in the ‘sys_choice’ table and make sure all languages are
    loaded successfully.

### MOVE TO UPDATE SET

-   In Servicenow, make sure your current update set is selected for the current story under development
-   Manually move the related choice list updates into this update set. 
    - Using the "Add to Update Set" UI Action   
    - This can be done either on the record form or the list view (if more than 1 record)
-   Only a single choice list will need to be added for each element, as the entire choice set for that element is copied into update set.

### REVIEW UPDATE SET

-   Go to your current update set 

-   Validate the choice list was captured correctly.

-   Add update set information into the Migration Notes.

### UPDATE THE MASTER LIST

- Add all of the new Service Portal related translations you loaded (or added manually) to the [Portal Translations Master List](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Portal_Translations_Master_List.xlsx) file.

## 3. LABEL

### GET THE ENGLISH LOCALE

-   Verify the field label for ‘en’ locale is present in ‘sys_documentation’.

### PREPARE EXCEL SHEET

-   **u_table:** Name of the table as ‘sys_documentation’.

-   **u_uid:** Start with Table\|FieldName\|Label or plural

    - **Example:** incident\|u_vendor\|label and incident\|u_vendor\|plural

    - **Note:**  The ‘label’ will translate the singular version of the field label, while the ‘plural’ value at the end of the ‘uid’ field, will translate the plural version of the field label.

### SEND EXCEL TO TRANSLATION TEAM 

-   Mail to Claudia Arruda (claudia.arruda\@dxc.com) and your manager.

### LOAD DATA

-   Once received from the Translation Team, copy the Excel sheet data into
    Notepad and make sure there are not any HTML tags.  If present, contact the
    Translation Team.

-   In ServiceNow, click on *Load Data* and select existing table option as
    ‘u_consolidated_translations_import’.

-   Select the Excel sheet with locale value and select the *Submit* button.

![](../images/LocaleLoadData.png)

-   Click on the link "Run Transform".

-   Click on the *Transform* button.

### REVIEW RECORD

-   Verify the records in the ‘sys_documentation’ table and make sure all languages
    are loaded successfully.

### EXPORT XML

-   Go to the message table ‘sys_documentation’ and search for the label.

-   Export as XML.

-   Add XML to the story for import to the Test environment and information for import into the Migration Notes.

### IMPORT XML

-   In the Test environment, navigate to any table list, right-click on table heading and Import XML.

-   ‘Choose file to upload’: The XML file downloaded from the related story.

-   Select the *Upload* button.

### UPDATE THE MASTER LIST

- Add all of the new Service Portal related translations you loaded to the [Portal Translations Master List](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Portal_Translations_Master_List.xlsx) file.

## 4. TRANSLATED NAME FIELD

### PREPARE EXCEL SHEET

-   **u_table:** sys_translated

-   **u_uid:** Table\|FieldName\|Value

    -   **Example:** question\|question_text\|User Firstname:
    
    -   **Note:** question_text is the ‘element’ field on the sys_translated table.

### SEND EXCEL TO TRANSLATION TEAM 

-   Mail to Claudia Arruda (claudia.arruda\@dxc.com) and your manager.

### LOAD DATA

-   Once received from the Translation Team, copy the Excel sheet data into
    Notepad and make sure there are not any HTML tags.  If present, contact the
    Translation Team.

-   In ServiceNow, click on *Load Data* and select existing table option as
    ‘u_consolidated_translations_import’.

-   Select the Excel sheet with locale value and select the *Submit* button.

![](../images/LocaleLoadData.png)

-   Click on the link "Run Transform".

-   Click on the *Transform* button.

### REVIEW RECORD

-   Verify the records in the ‘sys_translated’ table and make sure all languages are
    loaded successfully.

### EXPORT XML

-   Go to the message table ‘sys_translated’ and search for the label.

-   Export as XML.

-   Add XML to the story for import to the Test environment and information for import into the Migration Notes.

### IMPORT XML

-   In the Test environment, navigate to any table list, right-click on table heading and Import XML.

-   ‘Choose file to upload’: The XML file downloaded from the related story.

-   Select the *Upload* button.

### UPDATE THE MASTER LIST

- Add all of the new Service Portal related translations you loaded to the [Portal Translations Master List](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Portal_Translations_Master_List.xlsx) file.

## 5. TRANSLATED TEXT

### PREPARE EXCEL SHEET

-   **u_table:** sys_translated_text

-   **u_uid:** Table\|FieldName\|Sys_Id

    -   **Example:** question\|help_text\|6deb8e90dbbe5e006cd53c8caf9619c5
    
    -   **Note:**  The sys_id value is taken from the related “Document” field on the sys_translated_text’ field, related to the ‘table’ specified in the ‘u_uid’ field.

### SEND EXCEL TO TRANSLATION TEAM 

-   Mail to Claudia Arruda (claudia.arruda\@dxc.com) and your manager.

### LOAD DATA

-   Once received from the Translation Team, copy the Excel sheet data into
    Notepad and make sure there are not any HTML tags.  If present, contact the
    Translation Team.

-   In ServiceNow, click on *Load Data* and select existing table option as
    ‘u_consolidated_translations_import’.

-   Select the Excel sheet with locale value and select the *Submit* button.

![](../images/LocaleLoadData.png)

-   Click on the link "Run Transform".

-   Click on the *Transform* button.

### REVIEW RECORD

-   Verify the records in the ‘sys_translated_text table and make sure all languages are
    loaded successfully.

### EXPORT XML

-   Go to the message table ‘sys_translated_text’ and search for the label.

-   Export as XML.

-   Add XML to the story for import to the Test environment and information for import into the Migration Notes.

### IMPORT XML

-   In the Test environment, navigate to any table list, right-click on table heading and Import XML.

-   ‘Choose file to upload’: The XML file downloaded from the related story.

-   Select the *Upload* button.

### UPDATE THE MASTER LIST

- Add all of the new Service Portal related translations you loaded to the [Portal Translations Master List](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Portal_Translations_Master_List.xlsx) file.

## 6. NEW LANGUAGE

**NOTE:** The following Portal areas are English-only since used only by DXC, and should NOT be translated:

-  Cloud Admin Portal, Cloud User Portal, Cloud Shared Services

-  Ideas

-  Security Incidents, Security Operations

-  Standard Change Templates

### UPDATE TABLES AND FILES (Promoted to all instances)

-  Add the new language to the 'sys_language' table.

-  Add the new language to the 'CSCLanguageHelper' script.

-  Add the language to the choice list of the 'language' field on the 'sys_user' table.

-  Add the language to the list on the "Languages by Company" section of the 'core_company' record for DXC.

-  Have the scrum team's Application Engineer (AE) add the new language:

   o  As a column in the [Sample Excel Sheet for ML](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Sample%20Excel%20Sheet%20for%20ML.xlsx?raw=true) file.
 
   o  To the Account Transition Guidelines (ATG) > Company.md > Further Information on Specific Fields > [Multiple Languages Required](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/blob/master/Company.md#multiple-languages-required) section.

### UPDATE TABLES AND FILES (Dev instance only)

-  Add the new language to the 'translations_assist_library' script.

-  Add a column for the new language to the table 'u_consolidated_translations' and set the field length to 65,000.

-  Add a column for the new language to the import set table 'u_consolidated_translations_import' and set the field length to 65,000.

### REQUEST AND LOAD DATA

-  Open the [Portal Translations Master List](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Portal_Translations_Master_List.xlsx) file.

-  Replace the language in the column to the right of u_en with the target language, e.g. u_ro.

-  Send the updated Master List to the Translations team for translation, mailing to Claudia Arruda (claudia.arruda\@dxc.com) and your manager.
 
-  After receiving the file back from the Translations team, break out the 'sys_choice' records into a separate file.

-  Load all of the records for the tables other than sys_choice (sys_ui_message, sys_documentation, etc.) while in the Default update set. Use the standard process in sections 1, 3, 4 and 5 above to load these, but before transforming them, make sure to update the transform map to map the new language field from the import set table to the destination table.

-  Switch to your update set and load the records for 'sys_choice' using the standard process in section 2. There is no need to modify the transform map as that was done on the last load.

### REVIEW RECORDS

-  Verify the records for the new language in the section 1-5 tables.

-  Work with the scrum team testers to verify translations are displaying properly.

-  The Daleks scrum team must notify the APAC Victoria scrum team so that they can provide new language support for notifications.

----------------------------

|Note, there is additional language configuration information in [SharePoint](https://dxcportal.sharepoint.com/sites/SMDevStudio/Shared%20Documents/Forms/AllItems.aspx?viewid=a0a2db3c%2Db384%2D462a%2Dbbad%2D5ece67b4dea5&id=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FPublished%20Documents%2FSystem%20Configuration%20Documents)|
|--------------|

-----------------------------

PDXC ServiceNow Code Development Framework
==========================================

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice                                                                                                               |
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |


