# PDXC Code Guidelines for <Template: Replace with subject area>

Template: Minimize use of standardized content/tables/links, etc...  Include this link
back to the DXC developer guidelines, and make "standardized content" changes there.
Remember to add a link from there to this page, if the subject area is of wide-relevance.

*[Back to PDXC ServiceNow Developer Guidelines](Developer%20Code%20Guidelines%20DXC.md)*

Template: Replace with brief introductory synopsis, if desired.  1-3 sentences meant to help
reader understand the overall target of this page.

This is a template one can use to create new subject areas for DXC ServiceNow development
guideline

## Contents

* [Introduction](#introduction)
* [Section 1](#section-1)
* [Section 2](#section-2)
  * [Subsection 2.1](#subsection-21)
  * [Subsection 2.2](#subsection-22)

Template: links to subsections (### and smaller) are optional. Use them if they're useful.
They may be useful when a section is particularly long, or when the item is
particularly important.  Recommendation: avoid using valuable page space here
in Contents unless you're trying to solve a readability problem.

## Introduction

Template: All sections are optional -- use as appropriate.  Introduction section is where
you might put what you couldn't fit in the brief introductory synopsis.

## Section 1

Section 1 text

---
Template: horizontal rules are optional.  If using them, be consistent in length and usage.

## Section 2

Section 2 text

### Subsection 2.1

Subsection 2.1 text

example javascript:

```javascript
var foo = 'fi';
```

example html:

```html
<html>
<head>
</head>
</html>
```

### Subsection 2.2

subsection 2.2 text

---
Template: horizontal rules are optional.  If using them, be consistent in length and usage.
