PDXC SN Development Admin Rules
===============================

The *Development Framework* contains much in the way of guidelines and best practice,
some industry-driven by ServiceNow, some driven by DXC to ensure a maintainable and consistent way of developing.

However, there are some key DXC rules that all developers must be particularly aware of, and that are enforceable by
regional and Global Tech Leads.

[Development Stack](#development-stack)

[Access Management](#access-management)

[Development/Change Management](#developmentchange-management)

[The BIG Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Big%20Rules.md)

-------------------------------

## Development Stack

The PDXC ServiceNow development stack:

-   cscdev.service-now.com
-   csctest.service-now.com
-   cscdevqa.service-now.com

These instances are, for all intents and purposes, *production* environments.

These environments are NOT periodically refreshed/cloned. There are discrepancies between environments. Don't create more.

Individuals who are granted admin rights must adhere to these guidelines. Access may be revoked at any time for those that flout the guidelines and rules.

## Access Management

1.  Do not create accounts, unless the account creation has been formally approved via one of our business processes.
2.  Do not grant admin rights to others. Only [Tech Leads](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TechLead.md) have the authority to grant admin access, after formal management approval
via a [RITM request.](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Access-SN%20Dev.md)
3.  Granting permissions to users:

    a.  Never add roles directly to users.
    
    b.  There may be occasions where it is appropriate for you to manage
        (non-admin) group memberships.
        
    c.  Please ensure you have clear authority (email or other).
    
    d.  Please ensure your changes adhere to the [PDXC SN permissions model](https://dxcportal.sharepoint.com/:x:/r/sites/SMDevStudio/_layouts/15/Doc.aspx?sourcedoc=%7Beb151efd-34c1-424e-89d4-8790f7cc0c0e%7D&action=default&uid=%7BEB151EFD-34C1-424E-89D4-8790F7CC0C0E%7D&ListItemId=97356&ListId=%7B12C13203-AD65-4818-A62C-9336E7306C18%7D&odsp=1&env=prod).

## Development/Change Management

1.  Do not make changes in csctest, cscdevqa, or other downstream environments
    (pre-prod, prod, etc…)
    
2.  Do not install plugins / make changes which will affect the global environment.

3.  All PDXC SN development is done in cscdev

    a.  Using update sets which follow the [DXC naming convention](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md#update-sets)
    
    b.  Requires association with a Story tracked in the ServiceNow Agile module.
    
    c.  Following [Developer guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md)
    
4.  Changes are promoted to csctest as part of a Tech Review (TR) process.

    a.  Never by the original developer
    
    b.  Update sets are marked "Complete" only at the end of the TR process, and ready for
        promotion.
        
    c.  Changes consist of update sets and XML files containing data/records
        which are captured in the Migration Information field of the SN Agile
        Story.
        
    d.  XML files should not contain objects which can be captured in an update
        set.
        
5.  PDXC SN uses a "Fix-Forward" approach to development:

    a.  Changes made in update sets are not rolled back (in any environment).
    
    b.  A new update set is created (in cscdev) and standard promotion process
        is used.
        
6.  No update sets are applied to cscdev from a sandbox environment.

7.  Exploratory/iterative development (including troubleshooting) should be done
    in a sandbox as much as possible.
 

------------------------


 PDXC ServiceNow Code Development Framework
==========================================

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice                                                                                                               |
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |

-----------------------------------------------

|[Top](#pdxc-sn-development-admin-rules)|
|--------|
