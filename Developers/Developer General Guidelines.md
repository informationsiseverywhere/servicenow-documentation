GENERAL GUIDELINES
==================

These guidelines should be followed by all development teams to main consistency in the PDXC ServiceNow code base. These guidelines take into account a global development structure with developers in regions as diverse as Australia, India, Europe and North America, working in the same development environment. The teams are responsible for specific application areas within the platform. (e.g. Incident, Change, Problem, Cloud, Asset Management, SRM, etc.) See who is responsible for what [here...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md)

[Domain Separation](#domain-separation)

[Developer Profile](#developer-user-profile)

[Admin Account Restrictions](#admin-account-restrictions)

[Moving Code](#moving-code-between-environments)

[General Do's and Dont's](#general-dos-and-donts)

[ACL Practices](#acl-practices)

[Unintentional Changes](#unintentional-changes)

[Background Scripts](#background-scripts)

[Table Creation](#table-creation)

[Integration](#integration)

[Catalog](#catalog)

[Language](#language)

## Migration Information Naming Conventions
[XML File Naming Convention](#xml-file-naming-convention)

[Update Set Naming Convention](#update-set-naming-convention)

[Background Script File Naming Convention](#background-script-file-naming-convention)


## Domain Separation

Domain Separation is a way to separate data and business process into logically-defined domains within a single ServiceNow instance. Domain Separation enables DXC to:

-   Strictly enforce data segregation between business entities

-   Customize business process definitions and user interfaces for each business entity

-   Leverage global processes and global data without copying data or configs

We have enabled the domain separation plugin to serve hundreds of customers and to maintain a single PDXC SN solution. We have several secure instances that also leverage the same PDXC SN solution even though the instance is configured for a single customer.

**Developers must develop solutions (ServiceNow object) in the TOP domain
only**; there are some objects that are not domain separated, for example
‘Script Include’, but they should always be in TOP domain for any development.
See Appendix for list of exceptions

## Developer User Profile

The developer must ensure their profile has the required information to develop
any functionality. The User Profile Domain field should always be set as TOP.
You still have the option to move your current session domain from the domain
picker menu (Right hand system Gear button \>\> Developer) to Global or a lower
domain when required for investigation but make sure you change back to TOP for
development.

**The developer’s user profile language must be set as English (Webster’s).**
Ensure all development is performed while you are in “English (Webster’s)”, not
“English (Oxford)”.

As an example, if the developer does not set his/her profile to ‘English (Webster’s)’, the creation of a field auto generates a record in the ‘sys_documentation’ table in the language of the developer’s profile rather than creating it in English (Webster’s) (en).

## All Platform Development Uses English Language

**All development on the platfrom must be done in English language.**  This includes, and is not limited to:
- ServiceNow object names
   - Variables
   - Variable Sets
   - Script Names
   - Field names
- Variables, comments, message strings within javascript

Any abbreviations used in any of the above must represent an English word.
There are no exceptions for words or languages "similar" to english.

All translations are managed using translations in five tables containing label, message, sys_translated, sys_translated_text, and choice lists.
Refer to [Language](#language) section for more detail about translations.

## Admin Account Restrictions

**Creation of Admin accounts is strictly restricted**; developers should not create/provide any other user with admin access, including test users. **There is no exception; if someone asks, redirect the request to your Manager or Regional Lead.**

An audit is performed regularly on each environment, so any activities performed that are prohibited will be identified.

-   **DEVELOPMENT**

Developers have full access (Admin Role) to the Dev environment. However, they should not perform any action that will negatively impact the system or other developers.

-   **DEVTEST**

The DevTest environment also comes with full access (Admin Role); the main
purpose is to migrate update sets, import/export XML and as a reference point.
This environment has full restrictions for developers; this means they should
not do any activity that only an Admin can do. Example: creation and
modification of an object. Do not attempt to fix any issue directly in the
DevTest environment.

| It is STRICTLY forbidden to modify any object directly in DevTest as this will cause synch issues between Dev and DevTest, or worse, with upper environments. Don’t even think about doing it! |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|


-   **SANDBOX**

The Sandbox environment is used for trial and error purposes. Developers should
try first in the Sandbox environment if unsure of specific functionality, but
should try to avoid impacting the system by running free form scripts or
deletion of any existing objects.

The Studio Sandbox is ‘cscsandbox2’, unless mentioned otherwise. For example,
during an upgrade the ‘cscsandbox2’ environment is used as the Regression
Testing environment of the new upgrade. In that case, a notification mentioning
the change freeze and the temporary sandbox address will be sent.

The developer can create an Update Set in Sandbox2 to track their change, but in
every case, this Update Set cannot be migrated to Dev nor to DevTest. Any
development done in Sandbox must be manually replicated in Dev and captured in a
new Update Set.

--------------------------------

## Moving Code Between Environments

Changes will include update sets, XML files, plug-in activation and data
manipulation scripts

### Update Sets

The Developer will create an update set (using the naming convention noted below) before starting the development of a Story in the Dev environment. Upon completion, the developer must send the update set to their Technical Lead for review. There may be back and forth communication to accommodate any required changes as a result of the review, but once the developer has received the confirmation of the solution from the Tech Lead, then no further modification should be performed to that version of the update set.  When an update set is ready to be migrated to DevTest, the update set will be marked as "Complete" and migrated to DevTest.  It should not be reopened for editing in the "In progress" status after this migration.

**Once an update set is moved to DevTest environment and committed that update set should never be ‘backed out’.** For any issue reported by the tester, the developer should create an incremental version of the same update set fixing the issue, requesting another review and then migrating it to the DevTest environment.

For any exceptions, contact a Technical Lead.

#### Update Set Naming Convention

*\<Application\>_\<F\>/\<D\>_\<Story Number\>_\<Version\>_\<Your Initials or
short ID \>*

Where:

**\<Application\>:** e.g. Incident, Change, SRM, Portal, Chat, ESD, etc.

**\<F\>/\<D\>:** ‘**F**’ is a Feature and ‘**D**’ is a defect (as noted on the
Story, Classification field)

**\<Story Number\>:** The ‘Number’ field on the Story form.

**\<Version\>:** The incremented number of an update set. Always start with zero
(‘0’).

**\<Your Initial\>:** Developer Initials or Short Id.

Examples:

Incident_D_STRY0045395_0_gsingh95

Change_F_STRY0045475_0_gsingh95

Change_F_STRY0045475_1_gsingh95

**External Reference:** [ServiceNow Documentation](https://docs.servicenow.com/bundle/istanbul-application-development/page/build/system-update-sets/concept/c_UpdateSets.html)

### XML Files

The developer exports records into XML files where required as part of their
solution. The system will generate the XML file name containing the Table and
the Filter. The XML file should be attached to the Story with the naming
convention below, and should also be noted in the ‘Migration Information’ field
of the Story.

#### XML File Naming Convention

**\<TableName\> (\<Filter\>)_\<StoryNumber\>.xml**

**[(Filter)]:** Optional, and the system will only add if a condition is present
while exporting the XML.

###

--------------------------------

## General Do’s And Don’ts

| What                   | Do                                                                                                                                                                                                                                                            | Don’t                                                                                                                                                                       |
|------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Update Set Management  | An Update set should be created for each Story using the naming convention noted in this document                                                                                                                                                             | Don’t create too many update sets in “In progress” state as other developers may also require access to the objects modified in the update set.                             |
| Update Set Management  | All code modifications should be part of an update set linked to a Story.                                                                                                                                                                                     | Don’t mark update set as completed until you are absolutely certain.                                                                                                        |
| Update Set Management  | In exceptional scenarios, where there are no stories (usually during analysis), create a generic update set which can be used by you/your team during such scenarios (e.g. *UKQuantum_Discarded_Updates*)                                                     | Don’t re-open completed update sets in the dev environment                                                                                                                  |
| Update Set Management  | Revert any unintentional changes                                                                                                                                                                                                                              | Don’t rename completed update sets                                                                                                                                          |
| Update Set Management  |                                                                                                                                                                                                                                                               | Don’t remove any updates from a completed update set                                                                                                                        |
| Update Set Migration   | Code should be migrated from Dev to upper environments only through an update set.                                                                                                                                                                            | Don’t migrate code between Sandbox and Dev using update sets.                                                                                                               |
| Update Set Migration   | Correct order of migration: Dev DevTest DevQA. DevTest Pre-Prod Prod                                                                                                                                                                                          | Code should always be re-created manually on Dev.                                                                                                                           |
| Update Set Migration   |                                                                                                                                                                                                                                                               | Don’t make any code changes manually on DevTest or on any other upper environments.                                                                                         |
| Trouble- shooting      | Developers should use various debugging methods including gs.print(), gs.log() statements which are visible in the System Diagnostics \> Debug Log.                                                                                                           | Don’t use gs.addInfoMessage(), gs.addErrorMessage(), alert(), prompt() etc for troubleshooting as it is visible to all users and may have an impact on the user experience. |
| Trouble- shooting      | Also jslog() statement can be used for client side troubleshooting (e.g. in client scripts and UI policy).                                                                                                                                                    | Don’t leave prints and jslogs in code before sending for review                                                                                                             |
| Background Scripts     | Developers should get the background scripts reviewed (either by peers or by designated Tech Leads) before execution.                                                                                                                                         | Don’t execute background scripts without getting them reviewed.                                                                                                             |
| Background Scripts     | All background scripts should be first executed on Sandbox to minimise any unintentional impact, degradation or loss of data.                                                                                                                                 | Don’t execute background script directly on Dev or any other upper environments without discussing with designated Tech Leads.                                              |
| Background Scripts     | In case any abnormality is observed or identified post executing the query, it should be immediately raised with designated Tech Lead/ Global Tech Lead so that immediate corrective action can be taken.                                                     |                                                                                                                                                                             |
| Providing admin access | Admin role will be granted to the developer by designated Tech Lead/ Global Tech Lead on all the approved environments.                                                                                                                                       | Don’t create admin users or grant admin role to any existing user on any environment unless approved by the Tech Leads.                                                     |
| OOTB Defects           | After analysis, if the defect is considered to be an OOTB defect, the developer should raise an issue to ServiceNow via the HI Portal ([hi.service-now.com](https://hi.service-now.com/)).Note, each developer should have a personal login to the HI Portal. | For an OOTB defect, do not create a DXC custom fix unless authorised by the Tech Lead and Domain Architect                                                                  |
| OOTB Defects           | The developer must regularly chase ServiceNow support (daily if a DXC Hot Fix defect) and ask for a ServiceNow Hot Fix.                                                                                                                                       |                                                                                                                                                                             |
| OOTB Defects           | For any confirmed OOTB issue, ServiceNow support should provide a problem number associated with the incident. Before closing the incident, the developer should ensure they get the problem ticket from ServiceNow and document it in the Story.             |                                                                                                                                                                             |
| OOTB Defects           | As soon as ServiceNow delivers the Hot Fix/workaround, the developer may need to analyse the package before making changes on the development environment.The potential impacts on DXC customised code must be understood.                                    |                                                                                                                                                                             |
--------------------------------

## ACL Practices

OOTB ACLs must not be modified but a new ACL should be created for any custom
functionality. In some circumstances, when an OOTB ACL may impact a custom ACL
(If more than one ACL exists of the same type on same object, the system will
grant access if any ACL is passed), then the OOTB ACL should be deactivated and
the custom ACL should be amended to accommodate the OOTB condition. In this
case, we need to promote both the OOTB ACL (marked as active false as that’s the
only change allowed in an OOTB ACL) and the custom ACL.

Descriptions of new ACLs should be as the OOTB standard. Please check OOTB
read/write/create/delete/etc. ACLs.

Make sure you set the ‘Admin Overrides’ field to true unless the requirement
intentionally specified to restrict for Admin as well.

An ACL has Requires Role, Condition and Script sections. **In our PDXC ServiceNow environment, it is advisable to use the Script section in place of the Requires Role section.** This is for maintainability reasons: The system will create another record with the Requires Role, and one ACL may have multiple roles. In a complex Story, there may be a number of ACLs and then multiple roles associated
with each ACL, resulting in a huge number of objects to maintain, leading to possible human errors

**External Reference:** [ServiceNow Documentation](https://docs.servicenow.com/bundle/istanbul-servicenow-platform/page/administer/contextual-security/concept/access-control-rules.html)*.*

---------------------

## Unintentional Changes

While working on the development of any functionality, the developer may
intentionally or unintentionally update many objects that are not part of the
current functionality. It is the sole responsibility of the developer to provide
a clean update set only containing the modifications related to the relevant
Story. During ServiceNow upgrades, there are often objects that are skipped that
shouldn’t be, because they have been inadvertently included in an update set.

As part of the Story development it is the developer’s responsibility to
minimize these risks by following these guidelines:

-   You should be aware of your modification and whether it is required or not
    as part of your requirement. If not, revert your change immediately.

-   At the end of the development, review your update set. Any objects you
    identified that are not part of your development should be reverted and
    moved to the Default update set.

-   Do not click on Save/Update/Insert when you open any object and have not
    made any changes. The System will flag it as updated by you and this object
    will be skipped during any future upgrade.

-   To Revert the object properly, do NOT reset the change made. By doing so, it
    will indeed reset the changes but the object will still be skipped. To
    revert the object properly, the Developer should revert the version, doing
    the following:  
      
    **Open object \> Version related list \> Open previous version record \>
    Revert to this version**

**External Reference:** [ServiceNow Documentation](https://docs.servicenow.com/bundle/istanbul-application-development/page/build/team-development/task/t_RevertAChange.html).

------------------

## Background Scripts

Running free-form JavaScript can cause system disruption or data loss. 

Developers must use this module wisely. Write a script using the text editor and send for initial review to your Regional Technical Lead. At this stage the script should not be run in any system. When a developer receives feedback to run the script then it should be run in the Sandbox environment first. Send it back for review with any changes made to meet the requirement. If no changes, then the same script will be run in the Dev environment and will be used for release.

When writing scripts, ensure the following:

-   Should not include any infinite loop.

-   ‘setLimit’ function should be used to limit the records to perform unit
    testing before updating all required records.

-   Use of ‘break’ statement wherever required.

-   Comment all ‘delete’ statements the first time the script is run and use
    logs to verify the output even in sandbox.

-   Never use ‘updateMultiple’ in a background script or anywhere in ServiceNow.

-   Use logs to display the output as a result of the script.

-   Write a proper information/purpose about the script at the beginning as a
    comment.

-   The script should have a “txt” extension, following the naming convention
    below, be mentioned in the ‘Migration Information’ field and be attached to
    the Story.

#### Background Script File Naming Convention

>   **\<DescriptiveName\>_\<StoryNumber\>.txt**

**External Reference:** [ServiceNow Documentation](https://docs.servicenow.com/bundle/istanbul-servicenow-platform/page/script/server-scripting/concept/c_ScriptsBackground.html)

--------------

## Table Creation

If approved by a Domain Architect or Technical Lead, the developer can create a custom table but the module and the access control should be created separately.

The developer should:

-   Be in the TOP domain to avoid creation of global domain objects.

-   Uncheck 'Create module' field.

-   Uncheck 'Create access controls' field.

-   If you creating a ‘Domain’ field, the database name (sys_domain) needs to be
    set manually before the creation of the domain field otherwise the system
    will add ‘u_’ prefix, and this field will not provide the desired result of
    data segregation.

-   If Access Control is then required, see section 5.1. If the table created is
    used for configuration purposes, the developer will discuss the table and
    module access with the Application Engineer or the Service Architect. The
    same access is applied in the access control and the module.

-   If the created table needs to extend the Task or Extended Task table, the
    approach requires a validation from the Application Engineer, Domain
    Architect and the Regional Technical Lead. An alternative approach may be
    suggested.

-   For any table extension, ensure you will be using the **majority** of the
    fields of the original table

-----------

## Integration

There are numerous system integrations that developers should be aware of. See
separate integration documents.

**Use the function below in your code to bypass integrations in server side
code:**

*gs.getSession().isInteractive() == true*

*Example:* You may have written server side code to display an error message and
abort the transaction when a particular field is null; but that may not be true
for an external system, where the process may require a record to be submitted
irrespective of the value in that field. Therefore, unless specifically
specified as part of the integration requirement, server side code should
include the above function to ensure it does not run outside of the context of a
standard user interacting with the system.

-------

## Catalog


**Catalog development is restricted to the following objects:**

-   Catalogs

-   Catalog Items

-   Catalog Workflows (including Dynamic Workflow)

-   Catalog Client Scripts

-   Catalog Variables and Variable Sets

-   Other objects related to Catalog Management

Catalogs are categorised as Global, Customer or Offering.

If a developer is required to build or modify any catalog back-end
functionality, follow these guidelines for testing:

-   Global catalog items can be used to test your developed functionality
    from the Portal Only and should not be used from the ITIL View. This will
    prevent any accidental modification.

-   If you find the Global Catalog Item does not satisfy the test case and requires a modification to the Catalog Item or any related       object (e.g.to modify Workflow field, Workflow, Variable Set, Company association, Omit
    Price, No Quantity, Delivery time, related list etc.), you should use or
    create a Catalog Item developed in Dev or DevTest prefixed with your region
    or team name (e.g. APAC, NA, Daleks). APAC Test Catalog Items use workflow “CSC Framework Main Workflow” in the Workflow Field. Any
    modification to workflow is still restricted.

-   Advise the Catalog Development team immediately if you accidently modify any catalog object. Don’t attempt to fix it - it will be       fixed by the Catalog Development team.

--------

## Language

We have enabled the Language plugin in our environment so that our PDXC ServiceNow solution supports Multilanguage. Currently, we have over 20 supported languages.

To execute the translation, the system requires a combination of code and an actual locale value.

Developers are requested to use the **getMessage()** function in their code
development for displaying the message - it has many benefits over freeform hard
text. Below are some OOTB examples:

-   gs.addInfoMessage gs.getMessage('Auto-dispatch failed. Change the
    information and save the form to run auto-dispatch again'));

-   gs.addErrorMessage(gs.getMessage('The Work Order Task does not have any
    Agent to give the removed asset to.'));

-   g_form.showFieldMsg("assignment_group", getMessage("No groups have been
    defined to cover the selected dispatch group"), "error");

-   alert (getMessage('Invalid Sequence, valid numbers are greater than 0'));

| **References: Languages**     |
|-------------------------------|
| [Creation of Locale](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md) |

-----------------------------------------

PDXC ServiceNow Code Development Framework
==========================================

| \# | Page                                 | Description                                                                                                                                         |
|----|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | [Developer Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Process.md)                    | The general development methodology, including the code dev and review cycle                                                                        |
| 2  | [Developer General Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20General%20Guidelines.md)         | Important developer set-up information, what you should and shouldn’t do with your profile, ACL practices, background scripts, table creation, etc. |
| 3  | [Developer Code Guidelines ServiceNow](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) | Best practice guidance from ServiceNow, including client scripts and business rules                                                                 |
| 4  | [Developer Code Guidelines DXC](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20DXC.md)        | Additional DXC-specific best practice                                                                                                               |
| 5  | [Developer Code Checklist](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Checklist.md)             | Quick checklists for developers and code reviewers when working a story, before, during, and after development                                      |
| 6  | [Developer Language Locales](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Language%20Locales.md)           | Steps to take when translation is required for customer facing elements                                                                             |
| 7  | [Developer Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md)                   | Email templates to assist developers and reviewers in communicating in a consistent and standardised way.                                           |
| 8  | [Developer Admin Rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)|Quick overview of access management and change management rules for developers|
| 9  | [Developer Code Guidelines Service Portal](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20Service%20Portal.md)             | DXC-specific best practices and guidelines for Service Portal development                                      |                                                                                                               |



