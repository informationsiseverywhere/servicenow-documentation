# PDXC Code Guidelines for ServiceNow Workflow

*[Back to PDXC ServiceNow Developer Guidelines](Developer%20Code%20Guidelines%20DXC.md)*

## Contents

* [ServiceNow's Workflow Best Practices](#servicenows-workflow-best-practices)
* [PDXC ServiceNow's Workflow Supplemental Guidelines](#pdxc-servicenows-workflow-supplemental-guidelines)

## ServiceNow's Workflow Best Practices

### Source: KB0680098 - Workflow Best Practices

This content has been copied from
[KB0680098 - Workflow Best Practices | Tips and Gotchas](https://hi.service-now.com/kb_view.do?sysparm_article=KB0680098)
as it was in February 2020.
Concepts documented here have been the subject of PDXC customer escalations, requiring hotfixes.  For that reason, we
choose to copy and present the data here, and not simply provide a link.  Absolutely, please refer to the KB itself.

#### Things to Avoid

Here's list of things to avoid when building a workflow:

* [NEVER delete a workflow version.](https://hi.service-now.com/kb_view.do?sysparm_article=KB0635645) Set a workflow version to inactive instead.
* When using the Create Task or Run Script activities, avoid creating a record on the same table as \[Table\] used in workflow version.  Doing so will cause an infinite loop.
* **Avoid using gs.sleep on any of the workflow activities.**  The gs.sleep does not release session; **the workflow will hold on to the thread; the instance will run out of worker threads for other jobs.** Use a Workflow Timer activity instead of gs.sleep.
* REST v2 internally has a timer that will wait for a response from the target server, this will end up hogging the thread.
* Avoid using "current.update()" within the workflow activities.  Especially avoid calling update() on the sysapproval_group table from the Approval Group activity and on the sysapproval_approver table from the Approval User activity. Workflow engine runs between before business rule < 1000 and before business rule >= 1000; there is no need to run update() to save the changes.
* Don't use stage field to drive the workflow.  Workflow sets the stage field.
* Avoid using too many Branch activities. Publishing the workflow may take a long time.  The workflow may break during execution as well. If waiting for multiple parallel approvers, use the Approval Coordinator activity instead. If a group of activities can be moved to a subflow, break the activities into subflow instead.
* There can only be one Manual Approval activity within an Approval Coordinator activity.
* Avoid adding customization to the Workflow Activity Definitions, workflow related Business Rules and "Workflow*" Script Includes. These files drive the workflow and approval functionalities. Continuous updates are done to these files. If customization is absolutely necessary, revisit the customization every time new patches are applied to the instance.
* Rollback To activity should only transition to an activity that already executed. The Rollback To activity only updates the state of Approval activities to "Not Requested" and Task activities to "Open" or "Pending". Activities that perform external system operations, such as deleting a file or sending an email; or changes by a Set Values and Run Script activities are not rolled back. Keep the Rollback To transitions simple, avoid rolling back to If activities.
* When using Parallel Flow Launcher activity (PFL) for Orchestration purpose, consider using Create Task and attaching a separate workflow for this task. E.g. In implementing onboarding activities, instead of using PFL to run various scripts to fulfill onboarding activities which may hog the worker thread, consider using multiple Create Task activities with a workflow specific to each task.
* Avoid redeclaring the catalog item variables on the Workflow SC variables. This will cause duplicate variable names to show up on the RITM and SCTASK record. Workflow SC variable allows new variables to be returned to the RITM while running the workflow. An example use case: during Fulfillment stage, an asset tag is generated after the asset is acquired. This asset tag is not part of the catalog variable but can be sent to the RITM via the Workflow SC variables.

#### Good to include

Here's list of things to include when building a workflow:

* When retrieving/accessing record(s) from within the workflow activities, follow guidelines below to prevent worker jobs from running a long time.
  * add checks to consider null result or non-existent record.
  * use "if (gr.next())" instead of "while (gr.next())" when expecting only 1 record.
  * when "while" loop is required, consider adding a maximum # of times to go through the loop.
* Save any variable values and other data into the workflow scratchpad as soon as the workflow starts.  There is some instance when the context has been idle for a while and variables are lost.
* When reusing a workflow context, watch out for the Maximum activity count.  Whenever possible, start a new instance of the workflow instead of reusing an existing context.
* When workflow design involved a large number of activities and transitions (over 30), consider breaking the activities into sub-flow. Consider moving repeated activities into a sub-flow as well.
* Additional manual approvals can be added only if the Manual Approval activity within the Approval Coordinator activity is still executing. For these approvals to be picked up by workflow activity,
  * the "glide.manual.approval.pickup_on_update" property should be set to "true", and
  * either an update to the parent record happens or a broadcast event to the workflow context happens. E.g.
  ```javascript
    new Workflow().broadcastEvent(wfContext.sys_id, 'update');
  ```
  * Refer to PRB1115684 for more information.
* Approval Group activity is used when different groups have to weigh in on the approval.  If approvals are required from everyone in a group, Approval User should be used instead.
* On a table that does not have an OOB stage field, a custom field of type "Workflow" can store the stage value from a workflow. Under the Workflow Version's property, the Stage tab is visible only if the table has a "Workflow" type field.
* The Generate activity is to pre-generate Tasks and Approvals before the respective activities execute.  Use the Generate activity only if task and approvals need to be modified prior to executing or to show expected approvals or steps to the user.  Otherwise, task and approvals are created when the Task and Approval activities execute.

## PDXC ServiceNow's Workflow Supplemental Guidelines

Workflow must not exceed 50 activities as per recommendation of ServiceNow. (Case CS4700880)
