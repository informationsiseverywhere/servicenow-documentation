JIRA INTEGRATION MAINTENANCE
============================

This document outlines requirements for support and maintenance of the
ServiceNow to Jira integration.

  * [Background](#background)
  * [Jira Administration](#jira-administration)
    + [The Integration Service Account](#the-integration-service-account)
    + [Ensuring Epic Links can be Created](#ensuring-epic-links-can-be-created)
    + [Administrators](#administrators)
    + [Adding Product Owners in Jira](#adding-product-owners-in-jira)
    + [Adding Versions in Jira](#adding-versions-in-jira)
  * [Trouble Shooting](#trouble-shooting)
    + [The Epic added in a ServiceNow Story is not showing up in the Jira Story](#the-epic-added-in-a-servicenow-story-is-not-showing-up-in-the-jira-story)
    + [The ServiceNow Story has not created a Jira Story](#the-servicenow-story-has-not-created-a-jira-story)
    + [There is no Assignee in the Jira Story](#there-is-no-assignee-in-the-jira-story)
    + [There is no Fix Version in the Jira Story](#there-is-no-fix-version-in-the-jira-story)

BACKGROUND
----------

- To see details of the integration design, including field mappings and transforms, see the [SN to Jira Integration page](./SN%20to%20Jira%20Integration.md).

- All ServiceNow stories that have a Jira Project configured for the integration
are automatically created and updated in Jira.

-   All stories are created in the associated Jira Project based on ConnectNow Correlation configuration, e.g. [Orchestration](https://jira.csc.com/issues/?jql=project%20%3D%20%22OEE%20Orchestration%22).

-   Stories are created in Jira using the **servicenow-api** Jira service account.

JIRA ADMINISTRATION
-------------------

### The Integration Service Account

The **servicenow-api** account must have the correct permissions in each applicable Jira Project, e.g. Orchestration.
project. To avoid issues, and allow for future enhancements, the following roles are granted to the servicenow-api account:

-   Administrator

-   Developer

-   Product Owner

### Ensuring Epic Links can be Created

The servicenow-api account must have **Product Owner** permission to the various
Epic programs (this is above and beyond the Product Owner permission within the OEE Orchestration project).
This allows the integration to associate the Epic (the Epic Link
field in Jira) with a Story record. Currently, the servicenow-api account is
associated with these Epic programs:

-   SPDXC

-   BAP

-   CEP

-   DIF

-   EFP

If other Epic Programs are created in Jira, and need to be utilised in
ServiceNow stories, then a request must be made for the Program Owner to grant
**Product Owner** permission to the **servicenow-api** account, for that specific Epic Program. Raise the request via a
[GitHub issue…](https://github.dxc.com/Platform-DXC/JIRA/issues/new)

*Without Product Owner permission for the servicenow-api account in the required Jira Program, then the Epic
Link field in Jira will remain unpopulated, even if the Epic exists in
ServiceNow.*

### Administrators

The Jira **OEE Orchestration** project has the following administrators:

| **Administrator** |
|-------------------|
| Paul Chaplin      |
| Paul T Kawka      |
| Robin McKee       |
| Shivani Joshi     |
| Clay Ramsey       |
| Gurwinder Singh   |

To add a new user to Jira, raise a [GitHub issue](https://github.dxc.com/Platform-DXC/JIRA/issues/new?assignees=&labels=%3Abust_in_silhouette%3A+new+user%2C+%3Arobot%3A&template=create-jira-user.md&title=Create+Jira+User%3A+NAME+HERE).

The key function of the Administrators in terms of the integration is to:

- Maintain Product Owners in Jira
- Maintain FixVersions in Jira

### Adding Product Owners in Jira

To avoid the overhead of maintaining all ServiceNow Agile users in Jira, the
integration assigns Stories in Jira to the Product Owner of the ServiceNow Agile
Product, not the actual assignee in ServiceNow.

Therefore, any new Product Owners in ServiceNow, must be added to the Jira **OEE
Orchestration** project, with **Product Owner** permissions.

Once logged into Jira, select **Settings** \> **Projects**, (top right).

![](../../images/JiraAdmin1.png)

**Project List** \> **OEE Orchestration**

**Project Settings** \> **Users and roles \> Add users to a role**

![](../../images/JiraAdmin2.png)

Enter the user’s name, and select **Product Owner** as the Role.

### Adding Versions in Jira

Fix Versions in Jira are maintained in **Project Settings**. Access as noted above. In Project Settings, select **Versions**.


---------------------------------


TROUBLE SHOOTING
----------------

### The Epic added in a ServiceNow Story is not showing up in the Jira Story

-   Does the [Epic record in
    ServiceNow](https://csc.service-now.com/nav_to.do?uri=%2Frm_epic_list.do%3Fsysparm_query%3Dactive%253Dtrue%255EEQ%26sysparm_view%3Dscrum%26sysparm_userpref_module%3D30154714ef941000a7450fa3f82256ef%26sysparm_clear_stack%3Dtrue)
    have a **Correlation ID** that matches the Jira Epic Issue Key? It is the
    **Correlation ID** that is used to retrieve the correct Epic Link in Jira.
    If there is no match, no Epic will be added to the story in Jira.

-   Does the servicenow-api account have Product Owner permissions associated
    with the Epic Program (e.g. CEP, BAP, etc.)? If not, then make a [request
    via GitHub](https://github.dxc.com/Platform-DXC/JIRA/issues/new) for the
    Program Manager to give the servicenow-api account **Product Owner**
    permission to the Program.

### The ServiceNow Story has not created a Jira Story

-   Is the
    [Product](https://csc.service-now.com/nav_to.do?uri=%2Fcmdb_application_product_model_list.do%3Fsysparm_view%3Dscrum%26sysparm_userpref_module%3D876add92ef931000a7450fa3f82256c9%26sysparm_clear_stack%3Dtrue)
    on the ServiceNow Story associated with the correct Jira Project via ConnectNow Correlation record pairs for Product Group and Project Key? If
    not, see the [Story Creation in Jira](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/Jira/SN%20to%20Jira%20Integration.md#story-creation-in-jira) instructions, and then touch the Story in ServiceNow, making a minor update
    to one of the [mapped](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/Jira/SN%20to%20Jira%20Integration.md#field-mappings) fields. This should create the Story in Jira.

-   Does **ConnectNow** hold a record of the transactional data? Check in the
    ConnectNow **External References** table, querying for the ServiceNow Story
    Number in the **Record ID** field. All Story integrations use the **External
    Party** identifier [ServiceNow Jira
    Integration](https://csc.service-now.com/nav_to.do?uri=%2Fu_external_references_list.do%3Fsysparm_query%3Du_external_party%253DServiceNow%2520JIRA%2520Integration%26sysparm_first_row%3D1%26sysparm_view%3D).

    If there is a record, then check the External Record ID value, which is the associated Jira record. If there is no record, then has there been a recent change to the ConnectNow functionality?
    
-  It may just be a question of waiting. Sometimes, if the ConnectNow tables are dealing with a high demand from other integration sources, it can take 10 minutes or more for the Story to be created in Jira.
    
### There is no Assignee in the Jira Story

The Assignee in the Jira story is derived from the Product Owner on the ServiceNow story. The Product Owner must exist in Jira as a valid user, with Product Owner permission in the Jira OEE Orchestration project. The [OEE Orchestration administrators](#administrators) can check this, and add a new user with the correct permission if necessary.

### There is no Fix Version in the Jira Story

- First check if there is a **Tag** on the ServiceNow Story in the format FY21Q2. The integration specifically looks for Tags with the FY prefix, a two digit year, followed by Q, and finally digits 1 to 4. If there is no Tag on the ServiceNow Story in that format, then there will be no Fix Version value in Jira.

- Does the Fix Version value exist within the Jira OEE Orchestration project? The [OEE Orchestration administrators](#administrators) can check this in Jira: JIRA ADMINISTRATION > Projects. Select OEE Orchestration from the Project List. Project Settings > Versions. New versions can be added here as necessary.



----------------------

See also:

[Details on field mappings and
transforms…](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/Jira/SN%20to%20Jira%20Integration.md#field-mappings)

------------------------------------

|[Top](#jira-integration-maintenance)|
|-------|
