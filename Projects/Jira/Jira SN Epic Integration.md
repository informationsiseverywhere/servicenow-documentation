Jira EPICS to ServiceNow Agile Integration
====================================

![](../../images/SNJira1.png)


- [Background](#background)
- [Assumptions](#assumptions)
- [Epic Creation from Jira](#epic-creation-from-jira)
  * [Field Mappings](#field-mappings)
  * [Transforms](#transforms)
  * [Field Mapping Considerations](#field-mapping-considerations)
  * [Value Mappings](#value-mappings)
- [Epic Updates from Jira](#epic-updates-from-jira)

------------------------------


Background
==========

**NOTE: The integration from Jira to ServiceNow to create Epics does not yet exist.**

[STRY0273300](https://csc.service-now.com/nav_to.do?uri=rm_story.do?sys_id=4db7d992db4808d00e15cdae3b9619df%26sysparm_view=scrum)
delivered a uni-directional integration from ServiceNow Agile stories to Jira
Stories. This page describes an integration involving Epics, which will be delivered as part of [STRY0334013](https://csc.service-now.com/nav_to.do?uri=rm_story.do?sys_id=0874d949db2010d0467fa2bc0b961953%26sysparm_view=scrum). Epics created in
Jira (which is the source of truth for Epics), will be created and updated in
ServiceNow.

The updating of ServiceNow stories from Jira is dealt with in a [separate
document](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/Jira/Jira%20to%20SN%20Integration.md).

Assumptions
===========

-   Jira will create and update Epics in ServiceNow

-   Epic creation will be dependent on the addition of an label in Jira named
    ‘Orchestration’

-   ServiceNow will not create or update Epic records in Jira

-   Product Owners will manually update the Product in the ServiceNow Epic
    record (there is no Product concept in Jira, but the field is mandatory in
    ServiceNow; therefore, Epic records will be created with a placeholder
    Product that will be updated at a later date by the Product Owner).

-   The DXC custom ServiceNow application, **ConnectNow** will be used as the
    integration method. (ConnectNow is a point to point (P2P) rapid integration
    framework that leverages ServiceNow's event-driven processing architecture,
    with configurable REST and SOAP endpoints in data configuration forms).

-   The Jira DevOps team will provide webhooks to the ServiceNow development
    team, which will be utilised by ConnectNow to update ServiceNow

-   A ServiceNow integration account will be used to make the relevant updates.

Epic Creation from Jira
=======================

Any Epic created in Jira with a label of Orchestration, will trigger the
creation of an Epic in ServiceNow (rm_epic table), with fields mapped as shown
[below](#field-mappings).

If an Orchestration label is added to an existing Jira Epic (that has not
already been created in ServiceNow), then that will trigger the creation of a
new Epic in ServiceNow. However, there should be a mechanism in place to ensure
that one Epic record in Jira cannot generate a duplicate Epic record in
ServiceNow.

It is expected that upon successful creation of the Epic in ServiceNow, a Work
Note comment will added in the ServiceNow Epic record with a URL link to the
Jira record (e.g. "Created from Jira Epic BAP-1012").

Additionally, it will be useful if a Jira Activity Comment can be created with a
URL link to the ServiceNow record (e.g. ServiceNow Epic
[EPIC0011502](https://csc.service-now.com/nav_to.do?uri=%2Frm_epic.do%3Fsys_id%3D23fcd42cdb285c50fae8a2bc0b9619bc%26)
created).

Field Mappings
--------------

| **Jira Field** | **ServiceNow Field (rm_epic)** | **Transform/Consideration** |
|----------------|--------------------------------|-----------------------------|
| Status         | State                          | Transform required          |
| Summary        | Short description              | No transform                |
| Description    | Description                    | No transform                |
| Issue Key      | Correlation ID                 | No transform                |
| FixVersion     | Tag                            | Special consideration       |

Transforms
----------

### STATE field

| **Jira Value** | **ServiceNow Value** |
|----------------|----------------------|
| Open           | Draft                |
| Analyzing      | Draft                |
| Backlog        | Ready                |
| In Progress    | Work in progress     |
| Validation     | Work in progress     |
| Done           | Complete             |
| Rejected       | Cancelled            |

Field Mapping Considerations
----------------------------

**FixVersion(Jira)/Tag(ServiceNow)**

The *Tag* field in ServiceNow is similar in concept to the *Label* field in
Jira. The Tag values exist in a separate ServiceNow table (label), and will be
used to store the Fix Version (e.g. FY21Q1).

If the value of the Fix Version in Jira does not exist as a value in the
ServiceNow label table, then it should be created by the integration, with the
following values:

| **Field**   | **Value**                              |
|-------------|----------------------------------------|
| Name        | FixVersion (from Jira)                 |
| Owner       | ServiceNow Integration service account |
| Type        | Standard                               |
| Viewable by | Everyone                               |
| Active      | True                                   |

Value Mappings
--------------

Values that do not exist as data in the Jira issue table, but that should be
passed during the creation process to the ServiceNow Epic record. These values
should not change in any subsequent updating, but may change manually in the
ServiceNow Epic record.

| **Value**           | **ServiceNow Epic Field** |
|---------------------|---------------------------|
| Product Placeholder | Product                   |

Epic Updates from Jira
======================

Any changes to field values in the [Field Mappings](#field-mappings) above below
should trigger an update to ServiceNow of those changed values.

------------------------------------------

| [Top](#jira-epics-to-servicenow-agile-integration) |
|----------------------------------------------|


