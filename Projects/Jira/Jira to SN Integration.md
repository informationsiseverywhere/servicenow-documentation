Jira to ServiceNow Agile Integration
====================================

![](../../images/SNJira1.png)

- [Background](#background)
- [Assumptions](#assumptions)
- [Story Updates from Jira](#story-updates-from-jira)
  * [Field Mappings](#field-mappings)
  * [Transforms](#transforms)
    + [PRIORITY field](#priority-field)
    + [STATE field](#state-field)
  * [Field Mapping Considerations](#field-mapping-considerations)
    + [EPIC Field in ServiceNow](#epic-field-in-servicenow)
    + [TAGS field in ServiceNow](#tags-field-in-servicenow)
    + [WORK NOTES in ServiceNow](#work-notes-in-servicenow)

--------------------------------------------------

Background
==========

**NOTE: The integration from Jira to ServiceNow does not yet exist.**

[STRY0273300](https://csc.service-now.com/nav_to.do?uri=rm_story.do?sys_id=4db7d992db4808d00e15cdae3b9619df%26sysparm_view=scrum)
delivered a uni-directional integration from ServiceNow Agile stories to Jira
Stories. This page describes the addition of a bi-directional integration, to be delivered as part of [STRY0334020](https://csc.service-now.com/nav_to.do?uri=%2Frm_story.do%3Fsys_id%3Dea38d181dba010d0467fa2bc0b961928%26).
Stories in Jira, originally created in ServiceNow, will send updates back to the
ServiceNow Agile stories.

Additionally, Epic records, created in Jira (which is the source of truth for
Epics), will be created and updated in ServiceNow Agile. Epic creation and
updating are dealt with in a [separate document](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/Jira/Jira%20SN%20Epic%20Integration.md).

Assumptions
===========

-   Jira will only pass updates to ServiceNow Stories that have been already
    been created as part of the ServiceNow to Jira integration (i.e. the
    Reporter in Jira is the ServiceNow Jira service account, servicenow-api)

-   Jira will not create new Stories in ServiceNow Agile.

-   The DXC custom ServiceNow application, **ConnectNow** will be used as the
    integration method. (ConnectNow is a point to point (P2P) rapid integration
    framework that leverages ServiceNow's event-driven processing architecture,
    with configurable REST and SOAP endpoints in data configuration forms).

-   The Jira DevOps team will provide webhooks to the ServiceNow development
    team, which will be utilised by ConnectNow to update ServiceNow

-   A ServiceNow integration account will be used to make the relevant updates.

Story Updates from Jira
=======================

Any changes to field values in the [Field
Mappings](#field-mappings) section below should trigger an update
to ServiceNow of those changed values.

The updates will happen only in the following conditions:

-   The Jira record was created by the ServiceNow integration (i.e. the Reporter
    is the servicenow-api account).

-   The Jira record is being updated by an account other than the servicenow-api
    account.

Field Mappings
--------------

| **Jira Field**           | **ServiceNow Field (rm_story)** | **Transform/Consideration** |
|--------------------------|---------------------------------|-----------------------------|
| Summary                  | Short Description               | No                          |
| Priority                 | Priority                        | Transform required          |
| Points                   | Points                          | No                          |
| Acceptance Criteria      | Acceptance Criteria             | No                          |
| Description              | Description                     | No                          |
| Status                   | State                           | Transform required          |
| Custom field (Epic Name) | Epic                            | Special consideration       |
| Attachments              | Attachments                     |                             |
| fixVersion               | Tags (FY21Q1 format)            | Special consideration       |
| Activity Comment         | Work Note                       | Special consideration       |

Transforms
----------

### PRIORITY field

| **Jira Value** | **ServiceNow Value** |
|----------------|----------------------|
| Critical       | 1 – Critical         |
| High           | 2 – High             |
| Medium         | 3 – Moderate         |
| Minor          | 4 – Low              |

### STATE field

| **Jira Value** | **ServiceNow Value** |
|----------------|----------------------|
| Open           | Draft                |
| Preparing      | Estimation           |
| Ready          | Ready                |
| In Progress    | Work in progress     |
| Done           | Complete             |
| Rejected       | Cancelled            |

Field Mapping Considerations
----------------------------

### EPIC Field in ServiceNow

The Epic reference field on rm_story is a look-up value from the rm_epic table,
Short Description. The rm_epic table has a field, **Correlation ID**, that holds
the value of the **Issue Key** in Jira (e.g. SPDXC-121).

Therefore, any Epic field updates in Jira, should pass the **Issue Key** value
to ServiceNow. The integration will perform a look-up in the rm_epic table of
the **Correlation ID** to ensure the correct Epic name is populated in the
ServiceNow Story record.

### TAGS field in ServiceNow

Updates from Jira to the **FixVersion** field, should update the **Tags** field
for the relevant ServiceNow Story record.

If the value of the **Fix Version** field in Jira does not exist as a value in
the ServiceNow **label** table (which holds **Tag** values), then it should be
created by the integration, with the following values:

| **Field**   | **Value**                              |
|-------------|----------------------------------------|
| Name        | FixVersion (from Jira)                 |
| Owner       | ServiceNow Integration service account |
| Type        | Standard                               |
| Viewable by | Everyone                               |
| Active      | True                                   |

If a **FixVersion** value is removed from a Story record in Jira, then the Tag
value should also be removed from the corresponding ServiceNow Story record (but
NOT from the label table).

### WORK NOTES in ServiceNow

It is assumed that the Story Work Notes in ServiceNow, when updated by Jira,
will be tagged with the name of the Integration User ID. Therefore, to add
clarity to who actually made the **Activity Comment** update in Jira, the Work
Note in ServiceNow should be prefixed with the shortname of the Jira user (e.g.
"pchaplin: " + the Work Note).

------------------------------------------------------


| [Top](#jira-to-servicenow-agile-integration) |
|----------------------------------------------|

