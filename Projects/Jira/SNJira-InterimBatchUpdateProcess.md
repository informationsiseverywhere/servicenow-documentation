# Interim Batch Update Process

Whilst the [ServiceNow to Jira integration](./SN%20Jira%20Integration.md) is being built, there is an interim manual batch update process to update existing records in Jira that have been imported from SN, and import new Stories created in SN to Jira.

The process involves exporting records from both Jira and SN, running several key field compares within Excel, and then running
multiple Bulk Updates within Jira.

[See attached Excel file for the process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Jira%20SN%20Updates%2027Jan2020.xlsx?raw=true)

[Use the attached configuration file](../../Files/SN2JiraImportConfigv1.txt?raw=true)
