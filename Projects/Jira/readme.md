# ServiceNow Agile and Jira Integration

A space for details about the integration between these two tools, and the on-going support and maintenance.

![](../../images/SNJira1.png)

-----------------------------------

For all Stories related to the ServiceNow Jira integration, see [here...](https://csc.service-now.com/nav_to.do?uri=%2Frm_story_list.do%3Fsysparm_query%3Dshort_descriptionLIKESN%2520to%2520Jira%2520Integration%255EORepic%253Df9ca2194dba3ccd0ca77a5db0b96190a%255EORepic%253D7ef7d05ddb3d4850ca77a5db0b961995%255EORshort_descriptionLIKEJira%2520to%2520SN%2520Integration%26sysparm_first_row%3D1%26sysparm_view%3Dscrum)

--------------------------------

For roadmap details, see [here...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/SN%20to%20Jira%20Integration%201.0.pptx)

![](../../images/JiraIntegration.png)

-------------------------------


