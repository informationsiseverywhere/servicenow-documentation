
# ServiceNow Agile and Jira Integration


![](../../images/SNJira1.png)


Details for an ‘e-bond’ proof of concept.
([STRY0273300](https://csc.service-now.com/nav_to.do?uri=rm_story.do?sys_id=4db7d992db4808d00e15cdae3b9619df%26sysparm_view=scrum))

- [Background](#background)
- [Assumptions](#assumptions)
- [Key ServiceNow tables](#key-servicenow-tables)
- [Triggers](#triggers)
  * [Story Creation in Jira](#story-creation-in-jira)
  * [Updates in Jira from SN](#updates-in-jira-from-sn)
- [Field Mappings](#field-mappings)
  * [Field Mapping Considerations](#field-mapping-considerations)
  * [Value Mappings](#value-mappings)
  * [Transforms](#transforms)
  * [Jira Status Change Rules](#jira-status-change-rules)
- [Error Handling](#error-handling)
- [Future Considerations](#future-considerations)
- [Reference Notes](#reference-notes)

--------------------------------------

Background
==========

Orchestration use ServiceNow Agile for user Stories and work tracking. SN Agile
is embedded within Orchestration’s entire Release Management process, and forms
the bed-rock of CI/CD automation of the delivery pipeline, and must be retained
as an intrinsic workflow tool.

However, Jira, is used throughout the OE&E organisation for tracking projects,
epics and user stories. Senior management expect to be able to see a common
backlog of stories, understand their status, % completion, alignment with agreed
projects, PIs, etc., in Jira.

Therefore, integration of ServiceNow Agile Stories, with Jira, will allow
visibility of ALL work carried out across OE&E without destabilising the clearly
defined DevOps practices of Orchestration.

Assumptions
===========

-   For the purposes of the initial MVP, *creation* of Stories will be one-way
    (ServiceNow to Jira). Story updates will be two-way (ServiceNow to Jira, and
    Jira to ServiceNow).

-   All Stories created in Jira will be aligned to **a predefined OEE**
    Project.

-   Not all Stories created in ServiceNow will initiate an outbound call to
    create a Jira Story (see [Triggers](#triggers) section).

-   Outbound calls to create and update from ServiceNow to Jira will be
    implemented via a Jira userid that has the appropriate Jira permissions in the
    selected Project to create Stories, and to map Epics to those
    Stories.

-   It is not possible to jump workflow Status values in Jira via the API (see
    the [Jira Status Change Rules](#jira-status-change-rules) section).

-   Jira is the source of truth for Epics. Epics will be created manually in
    Jira. Product Owners will manually create the Epic in SN, rm_epic table,
    entering the Epic Name field value from Jira into the Short Description, and
    the Issue Key field value from Jira into the Correlation ID field in SN.
    
-   Product Owners (PO) will be maintained in Jira (Users and Roles in the OEE Orchestration Project),
    and the username of the PO in Jira will match the user id of the SN Product Owner
    (less the domain identifier).
    
-   Fix Versions will be maintained in Jira (Versions, under Releases in the OEE Orchestration Project),
    in the format FY, 2-digit year, Q, 1 - 4 (e.g. FY20Q4) to match the format of the SN Tag.

-   The DXC custom SN application, **ConnectNow** will be used as the integration method.
    (ConnectNow is a point to point (P2P) rapid integration framework that leverages SN's
    event-driven processing architecture, with configurable REST and SOAP endpoints in data
    configuration forms).

Key ServiceNow tables
=====================

| **ServiceNow Form**         | **Table Name**                 |
|-----------------------------|--------------------------------|
| Agile Story                 | rm_story                       |
| Agile Epic                  | rm_epic                        |
| Agile Product               | cmdb_application_product_model |
| ConnectNow Correlations     | u_connectnow_correlation       |

Triggers
========

Story Creation in Jira
----------------------

Any Story created in ServiceNow, that has an associated Product
(cmdb_application_product_model table) with existing ConnectNow Correlation records (related to a Product Model) that have a Parameter value
of 'project_key' and 'product_group' will trigger the creation of a Story in Jira,
with fields mapped as shown [below](#field-mappings).

If the ConnectNow Correlation records do not exist for the product you wish to attach to the SN story, you must create these. Below are examples
and details on how to create such records for the Product “Application Model: Security Operations."

![](../../images/SNJira3.png)

![](../../images/SNJira4.png)

1.  You must always use the ConnectNow Definition of CNDEF0001503 for the Correlation records. This is the Definition for the Jira integration.

2.  You must have two records, one with the Parameter of ‘project_key’, and the other with the Parameter of ‘product_group’. These strings must be entered exactly as shown. A specific Correlation ID is paired with each Parameter.

3.  On the Correlation record with the ‘project_key’ Parameter, you must enter a valid Jira Project in the Correlation ID field. The example is showing 'ORCH'. On the record with the ‘product_group’ Parameter, you will enter the name of the product group responsible for the product.  The example is showing 'Orchestration'.  Note that the "Correlation ID" field values must not contain any spaces, as Jira labels will not accept spaces and an error will occur when the message is sent from SN.

4.  Use the Source field to relate the Correlation records to an existing Product Model in ServiceNow.

Below are example pairs of completed ConnectNow Correlation records for 4 different Products.

![](../../images/SNJira5.png)

If the Product is changed on an *existing* SN Story, such that the new Product is
one with existing Correlation records (and the previous Product was not), then that will
trigger the creation of a new Story in Jira.

However, there should be a mechanism in place to ensure that one Story record in
ServiceNow cannot generate more than one Story in Jira.

It is expected that upon successful creation of the Story in Jira, the Issue Key of
the record will be returned to SN, and a Work Note comment added with a URL link to the 
Jira record (e.g. "Jira Story [ORCH-1005](https://jira.csc.com/browse/ORCH-1005) created")
in the SN Agile Story.

Additionally, it will be useful if a Jira Activity Comment can be created with a URL link to the SN record
(e.g. Created from ServiceNow Agile Story [STRY0273300](https://csc.service-now.com/nav_to.do?uri=%2Frm_story.do%3Fsys_id%3D4db7d992db4808d00e15cdae3b9619df%26sysparm_stack%3D%26sysparm_view%3Dscrum%26sysparm_view_forced%3Dtrue)).

Updates in Jira from SN
-----------------------

Any changes to field values in the [Field Mappings](#field-mappings) section below should trigger
an update to Jira of those changed values.

Field Mappings
==============


| **SN Field (rm_story)** | **Jira Field**           | **Transform/Consideration** |
|-------------------------|--------------------------|---------------|
| Short Description       | Summary                  | No            |
| Priority                | Priority                 | Yes           |
| Points                  | Points                   | No            |
| Acceptance Criteria     | Acceptance Criteria      | No            |
| Description             | Description              | No            |
| State                   | Status                   | Yes           |
| Blocked                 | Status                   | Yes           |
| Epic                    | Custom field (Epic Name) | Yes           |
| Product Owner           | Assignee                 | Yes           |
| Story Number            | Labels                   | Yes           |
| Tags                    | Labels                   | Yes           |
| Tags (FY20Q4 format)    | fixVersion               | Yes           |
| Work Note               | Activity Comment         | Yes           |
| Release (parent)        | Sprint                   | Yes           |
| Blocked reason          | Activity Comment         | Yes           |
| _Type/Classification_   | _Labels_                 | _Not in MVP_  |

## Field Mapping Considerations

### EPIC Field in SN

The Epic Field on rm_story is a look-up value from the rm_epic table, Short
Description. The rm_epic table has a field, **Correlation ID**, that will hold
the value of the **Issue Key** in Jira (e.g. SPDXC-121). Epics are manually created
in Jira first, and then manually created in SN, by the PO. The integration should
use the value from the Correlation ID field in the Epic table, to map directly to the Jira
Custom field (Epic Name). Do not use the Short Description in rm_Epic, or Epic value in
rm_Story. Jira will automatically convert the Correlation ID/Issue Key to the Epic Name.

### TAGS field in SN

Tags in SN may hold multiple values. There are 3 types of Tags supported by this integration.

1.  Tags in the format of 'Roadmap' are sent to Jira as a "Labels" value of 'Roadmap'.

2.  Tags in the format of 'Capitalization' are sent to Jira as a "Labels" value of 'capitalizable'.

3.  Tags in the format FY, 2-digit year, Q, 1 to 4 (e.g. FY20Q3) are sent to Jira as a "Fix Version". There may be more than one FY tag value, 
associated with a single Story, e.g. FY20Q3 and FY20Q4. These values are mapped to the Jira
_fixVersion_ field, which can accept more than one value.

-  SN "Tag" values must be in the correct 'FYnnQn' format in order to be sent to Jira as a "Fix Version".

-  SN "Tag" values must be pre-defined in Jira in order to update the "Fix Version", otherwise they will receive error message *“Error! Unable to update Tag/fixVersion for JIRA record.”* in the SN Story activity log.

-  SN "Tag" values must be pre-defined in Jira in order to update the "Labels", otherwise they will receive error message *“Error! Unable to update Tag/Label for JIRA record.”* in the SN Story activity log.

-  SN “Tag” values in the proper format that are removed in SN are also removed from the Jira “Fix Version” and/or "Labels".

### PRODUCT OWNER field in SN

The Product Owner field on the Story table holds data in the format of First Name Last Name. 
The UI in Jira also displays data in the Assignee field in this format, however, it is believed
that the API will require the name in shortname format, e.g. pchaplin. This will therefore need
to be derived from the SN sys_user table (user id less the domain identifier).
There may be occassions when the SN shortname does not match with a username in Jira, or when
the user has not been added as a Product Owner to the OEE Orchestration Project in Jira.
See [Error Handling](#error-handling).

SN Agile Development > Products form users can change the "Product owner" and see the new value accurately reflected as a change
to the "Assignee" in all of the related Stories in Jira.  Only active SN Stories are updated, i.e. not 'Completed' or 'Cancelled', to
preserve accurate Story “Product owner” history.  Accomplished by passing the "Product owner" of applicable SN Stories to the Jira
Story "Assignee".


### ACTIVITY COMMENTS in Jira

It is assumed that the Activity Comments in Jira will be tagged with the name of the Integration User ID.
Therefore to add clarity to who actually made the Work Note update in
SN, the Activity comment in Jira should be prefixed with the shortname of the SN user (e.g. "pchaplin: "
\+ the Work Note).

### LABELS in Jira

The Label field in Jira can hold multiple values, similar to the Tags field in SN. Multiple values
are separated by a space - this will be required when mapping the Story Number and the text string which contains the product group (e.g. Orchestration) 
(see [Value Mappings](#value-mappings) below), and the values 'Roadmap' and 'capitalizable'.

### TYPE and CLASSIFICATION in SN (This requirement is under discussion, and will not be in MVP)

Where the *Type* field in rm_story = 'Development', **and** the *Classification* field = 'Feature', 
then a Label value should be passed to Jira of **SW_Development**. This value should appear in the Labels
field in Jira, along with the Orchestration label and SN Story Number.

e.g. SW_Development Orchestration STRY00100250.

If the Type or Classification values in SN are changed, and therefore the condition noted above no longer applies,
then the SW_Development label should be removed from Jira.

### RELEASE field in SN

As part of the existing process, the Release value on a SN Agile Story is added when the Story is added to a Release in the Release v2 application. The Release will have a parent Release record (e.g. PDXC ServiceNow Release 16.02 - Apr 27, 2020). At present, there is a manual process to batch update stores in Jira with the value of the associated release in SN.

The Correlation ID on the parent Release (rm_release) record has been exposed, and can be populated by the Release Manager ('release_v2_admin' role required). Members of the 'SEC: Agile Administrators' group will also have access to this field via a new role ‘rm_release_jira’. The value in the Correlation ID field will pass to the Sprint field in Jira for the relevant SN Agile Stories that have been ‘e-bonded’ to Jira.

This integration will run when:

-  A story is removed from a Release

-  A story is added to a Release

-  If the parent Release Correlation ID is renamed

If the value in the Correlation ID field of the parent Release name does not exist as a Sprint record in Jira, then it will be created in Jira, and associated with the OEE Orchestration Project.

Creation and updates happens near real-time, using REST APIs and ConnectNow.

Value Mappings
--------------

Values that will not exist as data in the SN rm_story table, but that should be
passed during the creation process to the Jira record. These values should not
change in any subsequent updating.

| **Value**         | **Jira Field** |
|-------------------|----------------|
| *Product Group*   | Labels         |
| Story             | Issue Type     |
| *Project Key*     | Project Name   |

## Transforms

### PRIORITY field

| **ServiceNow Value** | **Jira Value** |
|----------------------|----------------|
| 1 – Critical         | Critical       |
| 2 – High             | High           |
| 3 – Moderate         | Medium         |
| 4 – Low              | Minor          |

### STATE field

| **ServiceNow Value** | **Jira Value** |
|----------------------|----------------|
| Draft                | Open           |
| Estimation           | Preparing      |
| Ready                | Ready          |
| Awaiting Info        | In Progress    |
| Work in progress     | In Progress    |
| Blocked (flag)       | Blocked        |
| Development Complete | In Review      |
| Ready for testing    | In Review      |
| Testing              | In Review      |
| Testing Failed       | In Review      |
| On Hold              | In Progress    |
| Complete             | Done           |
| Cancelled            | Rejected       |

Jira Status Change Rules
------------------------

Note, that Jira Status values are linked to workflow, and only the following
transitions are allowed:

| **Change From SN Agile State** | **To Jira Status Value (one of)**         |
|--------------------------------|-------------------------------------------|
| Draft                          | Preparing, Ready, Rejected, Blocked       |
| Estimation                     | Ready, Blocked, Rejected                  |
| Ready                          | Preparing, In Progress, Blocked, Rejected |
| Awaiting Info                  | Ready, In Review, Done, Blocked, Rejected |
| Work in progress               | Ready, In Review, Done, Blocked, Rejected |
| Blocked (flag)                 | Ready, In Progress, Rejected              |
| Development Complete           | In Progress, Done, Blocked, Rejected      |
| Ready for testing              | In Progress, Done, Blocked, Rejected      |
| Testing                        | In Progress, Done, Blocked, Rejected      |
| Testing Failed                 | In Progress, Done, Blocked, Rejected      |
| On Hold                        | Ready, In Progress, Blocked, Rejected     |
| Complete                       | In Progress, In Review, Blocked, Rejected |
| Cancelled                      | Preparing, Blocked, Rejected              |

The integration accommodates State changes in SN, which may require moving through several Status change steps in Jira.  For example, a change from 'Draft' to 'Complete' in SN, requires a change in Jira from 'Open' to 'Ready' to 'In Progress' to 'Done'.  A Jira Status cannot return to ‘Open’ once it has been in any other Status such as ‘Blocked’, so once “unblocked” it instead returns to ‘Preparing’.

![](../../images/SNJira2.png)

# Error Handling

The Jira API will validate data before allowing an import. Specifically, the following Jira fields
need to be correctly matched:

-   Epic Name
-   Assignee
-   fixVersion

If there is no match, the preference is the record should still be created in Jira; consideration should be given as to how to 
automatically alert designated ServiceNow team members in such cases. However, if the Jira API rejects the creation of records with no matching Epic Name, Assignee or fixVersion, then the integration should write to the Work Notes in SN, indicating specifically which field was not matched, e.g.

"Story was NOT created in Jira, because a matching Epic could not be found."

How to resolve the following error when trying to create a Jira story from SN:  {"errorMessages":[],"errors":{"customfield_10303":"Field 'customfield_10303' cannot be set. It is not on the appropriate screen, or unknown."}}

-   The field, “customfield_10303” is the Acceptance Criteria on the Jira story.
-   When a project is setup in Jira, the Product Owner needs to add fields to the Issue scheme in order for them to exist on the Issue.
-   If that field is removed from the scheme for the given project (e.g. ORCH), when we try to push a value to it from SN, Jira will not know what it is and returns the above error.
-   This can be fixed by opening an issue with Jira support to have them add it back to the scheme, since the Studio does not have the permissions to see the scheme for the Jira projects.

# Future Considerations

Future considerations that are currently out of scope for MVP, but may be included in a future release:

-  Creation of Epics in SN, triggering creation of Epics in Jira
-  Mapping of attachments
-  Mapping/creation of users


# Reference Notes

- The Jira version is 7.5.4 (technically out of support)

- All calls will need to be https

**Info on webhooks:**

https://developer.atlassian.com/server/jira/platform/webhooks/

These can only be configured by JIRA administrators.

**JIRA Rest API:**

https://docs.atlassian.com/software/jira/docs/api/REST/7.5.0/

**Jira Status IDs**

https://jira.csc.com/rest/api/2/issue/ORCH-1545/transitions?expand=transitions.fields

------------------------------------

[Interim Batch Update Process](SNJira-InterimBatchUpdateProcess.md)

------------------------------------

|[Top](#servicenow-agile-and-jira-integration)|
|-------|
