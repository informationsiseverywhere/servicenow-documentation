Service Request Catalog – PDXC ServiceNow migration from Entitlement to User Criteria
-------------------------------------------------------------------------------------

**This communication describes the PDXC ServiceNow migration process that will
be used to move from Catalog Item Entitlement to User Criteria.**

Background
----------

Catalog items are made available only to users who have subscribed to the
service or product the catalog items deliver, and restricted to all other
users.  Without this, any user would be able to order any service or product
even if their organization does not have an agreement with DXC to provide it.

 

Traditionally ServiceNow has used ‘Entitlement rules’ to provide or restrict
availability to catalog items. Entitlement can be configured at various levels.

 

More recently ServiceNow has introduced ‘User Criteria’ as a way of providing or
restricting user access.  ‘User Criteria’ can be defined in many ways such as
for Companies, Locations, Departments, Groups, Users or Roles, or combinations
of each.  It has already been implemented in PDXC ServiceNow for Knowledge
articles.

 

PDXC ServiceNow is currently being enhanced to replace Entitlement rules with
User Criteria for catalog items.

Current approach
----------------

When catalog items are created in DEV or Commercial QA (Pre-prod) environments,
a default Entitlement rule is created and migrated to Production.  This default
Entitlement rule entitles the catalog item to a group called ‘ENT CSC Default’. 
As this group does not contain any users, access to the catalog item is
restricted to everyone.

 

During testing, Entitlement rules may be manually added to aid in testing the
catalog item, but these rules are not migrated to upper environments.

 

Once the catalog item has been migrated to production, Data Support teams or
Catalog Managers add the necessary Entitlement rules to make the catalog item
available to the relevant Users.

Proposed approach
-----------------

In the future, PDXC ServiceNow will use User Criteria, not Entitlement rules, to
make catalog items available to users or restrict access to catalog items. 
Existing Entitlement rules will be replaced by User Criteria for all existing
catalog items.  The option to manually add Entitlement rules to a catalog item
will no longer be available.  While Entitlement rules would still be able to be
data-loaded to a catalog item, these rules will be ignored when determining who
can access the catalog item.

 

When catalog items are created in development environments (including Commercial QA), a default
Entitlement rule will be automatically attached to the catalog item so that
entitlement can still be used in upper environments during the transition from
Entitlement to User Criteria.  For all new catalog items, a default User
Criteria ‘SRM-UC DXC Default’ should also be added manually to the catalog item
during development of the catalog item.  This default ‘SRM-UC DXC Default’ User
Criteria will be available in all PDXC ServiceNow environments and refers to the
existing  ‘ENT CSC Default’ group.  When the catalog item is migrated to upper
environments, the ‘mtom’ relationship between this User Criteria and the catalog
item should also be migrated.

 

During testing, additional User Criteria may be added to aid in testing the
catalog item, but the User Criteria and ‘mtom’ relationships will not be
migrated to upper environments.

 

Once the catalog item has been migrated to a pre- or production instance, Data
Support teams or Catalog Managers add the necessary User Criteria to make the
catalog item available to the correct entities.

 

Initially, User Criteria will be automatically created for all Entitlement rules
that exist in each PDXC ServiceNow instance.  This includes User Criteria for:

-   Companies

-   Departments

-   Groups

-   Locations

-   Users

User Criteria is configuration data and is created or maintained only by Data
Support teams.  When there is a requirement for new User Criteria in a PDXC
ServiceNow environment, a data request should be submitted to the relevant data
team.  When the User Criteria has been created in that environment, the data
team or Catalog Manager can relate the User Criteria to the catalog item.

Interim approach
----------------

During development of the User Criteria functionality, an interim approach is
needed for catalog item development.

1.  When User Criteria functionality starts being developed in DEV, it will no
    longer be possible to manually add Entitlement rules to catalog items in DEV
    and User Criteria will need to be used for testing in DEV.  When catalog
    items are created in DEV, a default Entitlement rule will be automatically
    attached to the catalog item so that entitlement can still be used in upper
    environments during the transition from Entitlement to User Criteria.  All
    existing catalog items in DEV will be migrated from Entitlement rules to
    User Criteria as part of the User Criteria development activity.  New
    catalog items should have the default User Criteria ‘SRM-UC DXC Default’
    manually added.  Entitlement rules can still be added in upper environments
    for testing.

2.  When User Criteria development is completed and migrated to DEVTEST, it will
    no longer be possible to manually add Entitlement rules to catalog items in
    DEV or DEVTEST environments and User Criteria will need to be used for
    testing.  All existing catalog items in DEVTEST will be migrated from
    Entitlement rules to User Criteria as part of the User Criteria development
    activity.  Entitlement rules can still be added in upper environments for
    testing.

3.  When User Criteria functionality is migrated to DEVQA, it will no longer be
    possible to manually add Entitlement rules to catalog items in DEV, DEVTEST
    or DEVQA environments and User Criteria will need to be used for testing. 
    All existing catalog items in DEVQA will be migrated from Entitlement rules
    to User Criteria as part of the User Criteria development activity. 
    Entitlement rules can still be added in upper environments for testing.

4.  When User Criteria functionality is migrated to QA and PROD instances, it
    will no longer be possible to manually add Entitlement rules to catalog
    items in all environments so User Criteria will need to be used from this
    point forward.  All existing catalog items in QA and PROD will be migrated
    from Entitlement rules to User Criteria as part of the User Criteria
    development activity.  Entitlement rules can no longer be added in any
    environments.

Next steps
----------

1.  Configuration changes for User Criteria has been completed in Sandbox2 and
    the Entitlement migration script to migrate existing catalog items to User
    Criteria has been run in Sandbox2.  All existing catalog items in Sandbox2
    have been migrated from Entitlement rules to User Criteria.  
    NOTE: Entitlement is no longer available in Sandbox2, User Criteria must be
    used.

2.  Begin User Criteria development in DEV.  This is due to start later this
    week.  A notification will be sent to this mailing list when development is
    about to start and Interim approach [1] is in effect.  
    NOTE: Entitlement will no longer be available in DEV, User Criteria must be
    used.

3.  Migrate User Criteria functionality to DEVTEST.  This will occur when
    development in DEV is complete.  All existing catalog items in DEVTEST will
    be migrated from Entitlement rules to User Criteria.  A notification will be
    sent to this mailing list when migration is about to start and Interim
    approach [2] is in effect  
    NOTE: Entitlement will no longer be available in DEVTEST, User Criteria must
    be used.

4.  Migrate User Criteria functionality to DEVQA.  This will occur when testing
    in DEVTEST is complete.  All existing catalog items in DEVQA will be
    migrated from Entitlement rules to User Criteria.  Release will be managed
    as part of the regular Release process.  A notification will be sent to this
    mailing list when migration is about to start and Interim approach [3] is in
    effect  
    NOTE: Entitlement will no longer be available in DEVQA, User Criteria must
    be used.

5.  Release User Criteria functionality to Pre- and Production instances.  All
    existing catalog items in Pre- and Production instances will be migrated
    from Entitlement rules to User Criteria.  Release will be managed as part of
    the regular Release process.  
    NOTE: Entitlement will no longer be available, and User Criteria must be
    used in all environments.
