**PDXC Service Request Enhancements**
=====================================

The PDXC ServiceNow platform includes multiple enhancements to the Service
Request/Catalog application. These include:

| RELEASE | STORY                                                                                                                             | CATEGORY        | DESCRIPTION                                          |
|---------|-----------------------------------------------------------------------------------------------------------------------------------|-----------------|------------------------------------------------------|
| 12.6    | [STRY0108355](https://csc.service-now.com/nav_to.do?uri=rm_story.do?sys_id=a42dec97db5c57c86668cdae3b961923%26sysparm_view=scrum) | Approvals, NSSR | Mid-workflow approvals. [More….](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Applications/SR/Mid-Workflow%20Approvals.md) |
|   12.10      |     [STRY0079165](https://csc.service-now.com/nav_to.do?uri=%2Frm_story.do%3Fsys_id%3D77b6411adb31cf0ca764d6d4ce961914%26sysparm_stack%3D%26sysparm_view%3Dscrum%26sysparm_view_forced%3Dtrue)                                                                                                                           |                 |                                        Table to hold look-up data for reference fields and list collectors. [More...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Applications/SR/Table%20for%20Custom%20Reference%20Data.md)              |
