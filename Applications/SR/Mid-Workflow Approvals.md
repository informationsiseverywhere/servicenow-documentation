Mid-Workflow Approvals
======================

This capability allows the SRM Approval Model to be called multiple times in the
same workflow.

SUMMARY
-------

### Addition of Workflow Stage in SRM Approval Model

-   A new field named ‘Workflow Stage’ has been added on the SRM Approval Model
    form.

    ![](../../images/MW1.jpg)

-   Catalog developers will have the option of specifying ‘Workflow Stage’ with
    the ‘SRM Approval Model’ records. If left blank, all such records will
    become part of the default stage.

-   The values available in the menu of ‘Workflow Stage’ field are the ‘Stages’
    configured in the workflow which is associated with the catalog item.

-   While calling the approval model in the workflow, catalog developers will be
    able to specify which approval phase they would like to invoke.

### Calling SRM Approval Model from Workflow with Stage

-   Catalog developers while creating workflows will have the option of
    specifying workflow stage while initiating the approvals. This will be done
    by passing the Stage name in the ‘Checking for Approval Model’ workflow
    activity as shown below:

     ![](../../images/MW2.png)

-   This can also be left blank if default stage has to be initiated where there
    is no value in ‘Workflow Stage’ field.

-   The SRM Approval Model records within a specific ‘Workflow Stage’ (or for
    default which is blank) will be executed based on sequence (ascending order)
    defined for these records.

DETAILS
-------

There are three (potential) steps required while calling SRM approvals. (The approvals can be called multiple
times in a single workflow):

**STEP 1: CREATE WORKFLOW STAGES (OPTIONAL)**

-   Open a workflow

-   Go to Workflow Properties and add a new stage.

 ![](../../images/MW3.jpg)

**STEP 2: CREATE SRM APPROVAL MODEL RECORDS AND SELECT ‘STAGE’ IN ‘WORKFLOW
STAGE’ FIELD FOR THESE RECORDS**

-   Multiple SRM approval model records can be created for a single Workflow
    Stage.

-   Each time approvals are initiated, they are generated for a specific stage
    (if the Workflow Stage is blank for SRM Approval Model records, consider it
    to be running under a single workflow Stage =’’).

-   The order field is used to determine the sequence of approvals generated
    under a given workflow stage.

 ![](../../images/MW4.jpg)

**STEP 3: CREATE WORKFLOWS AND WHILE CALLING SRM APPROVAL MODEL RECORDS, SPECIFY
THE WORKFLOW STAGE**

A.  Approval Action (core activities)

- Action should be set to ‘Mark Task Requested’

B.  Run Script (copy of existing but modify to include the workflow stage)

- Default (used today on almost all of the workflows):

```
var chk = new CSCSrmApprovalModel();
chk.CSCSrmApprovalreq(current.cat_item,current.request.requested_for.company);
```

- Specific Workflow Stage:

```
var chk = new CSCSrmApprovalModel();
chk.CSCSrmApprovalreq(current.cat_item,current.request.requested_for.company,"Quotation
Approval");
```

C.  Add in Wait for WF Event Approved

D.  Add in Wait for WF Event Rejected

- A single Rejected workflow activity can be reused by other workflow activities created for invoking SRM approval model (as shown in example 1 below).

- Else we can create one ‘Wait for WF Event’ (rejected) workflow activity for each SRM Approval Model call for a workflow stage.

E.  Add Approval Action – Approved?

- Action should be set to ‘ Mark task approved’

F.  Add Approval Action – Rejected?

- Action should be set to ‘ Mark task rejected’

**Steps *A to F* are required in a workflow each time the SRM approval model is invoked for a specific workflow stage.**

### Examples

Example 1: [Quantum - NSSR Multiple
Approvals](https://cscdev.service-now.com/workflow_ide.do?sysparm_wf_id=49b0df84db191f88ccd8a5db0b961957)

 ![](../../images/MW5.jpg)

Example 2: [NSSR Offering - Non-Standard Service
Request ](https://cscdev.service-now.com/workflow_ide.do?sysparm_wf_id=6e958095dbe55380ccd8a5db0b9619eb)

 ![](../../images/MW6.jpg)
 
 |[Top](#mid-workflow-approvals)|[Return to the Service Request Enhancement list](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Applications/SR/SR%20Enhancements.md)|
 |-------|-------|
