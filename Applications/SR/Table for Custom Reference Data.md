# Custom Table for Service Catalog Item Choices

A new table called “Catalog Item Variable Choices” is created in ServiceNow and
can be accessed from Navigation Menu by selecting “Variable Choices” under
Service Catalog.

![](../../images/vc1.png)

The “Catalog Item Variable Choices” table consists of six custom fields:

![](../../images/vc2.png)

**Functionality**:

When a new List Collector variable or Reference type variable in created on a
Service Catalog Item, “Catalog Item Variable Choices” table can be used as a
reference table in type specification tab for those variables.

While creating records in the “Catalog Item Variable Choices”, the Domain field
value will be auto filled based on the Company’s Domain.

*Below screen shot is an example of how to use “Catalog Item Variable Choices”
table as a reference table in List Collector variable*.

Example:

Step1: Open the Catalog Item, where a new variable will be added

Step2: Go to Variables tab in Item form and click new

Step3: New form for variable creation will open

Step4: Select variable type as List Collector

Step 5: In the Type Specification tab, select the list table as “Catalog Item
Variable Choices”

![](../../images/vc3.png)

Step6: Use Reference qual for filtering the data in Table.

Step7: Submit the form.

Step8: Open the Service Catalog Item where the new variable is created.

Step9: verify the list collector values populated based on the filtering
condition mentioned on the Reference qual.

---------------------------

|[Training Video](https://video.dxc.com/media/t/1_hrevi5in/86573511)|
|----|

---------------------------

 |[Top](#custom-table-for-service-catalog-item-choices)|[Return to the Service Request Enhancement list](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Applications/SR/SR%20Enhancements.md)|
 |-------|-------|
