Workflow Editor (WE) is the standard, OOTB worfklow component in ServiceNow, used to control workflow in many catalog items. 

Dynamic Workflow (DW) functions as a form-driven UI that enables those not as experienced with WE, to more easily create such workflow.

# Differences between DW and WE

## Workflow Versioning

WE is versioned and therefore a historical RITM will have the version of the workflow that was current at the time of the order.
Any changes to that workflow on the item will not affect any inflight RITMS.

However, with DW the workflow Steps are configured in real time – no versioning. So removing or adding tasks could affect any in
flight RITMS. 

e.g. So let’s say we have a DW Item with a 2 task sequential workflow, and inflight RITM has Task 1 closed / Task 2 Open.
If you then add a 3rd sequential task to the item – Task 3 will be raised on the existing inflight RITM after Task 2 has been closed.
This wouldn’t happen in WE.

Same applies if a task is removed, as future tasks on an inflight RITM will not show, with the exception of existing tasks being
removed (i.e. status of open or closed), then these remain on the RITM.


## Variables

By default from a user perspective, DW shows all variables on a task and WE does not

