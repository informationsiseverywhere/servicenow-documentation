# How We Work

[Our General Working Process](#our-general-working-process)

[Scrum Meetings](#scrum-meetings)

[Working With Stories](#working-with-stories)

 - [Key Fields](#key-fields)
 - [What the Story States mean](#what-the-story-states-mean)
 - [Story Point Estimation](#story-point-estimation)
 - [Standard Story Scrum Tasks](#standard-story-scrum-tasks)
 - [Using Visual Task Boards](#using-visual-task-boards)

[Working With Incidents](#working-with-incidents)

-------------------------

## Our General Working Process

Each member of the scrum development team will be involved in this process. See the [Roles](./Roles.md) section.

![](../images/WorkingProcess.png)

|[1. Working With Stories](#working-with-stories)|[2. Scrum Meetings](#scrum-meetings)|[3. Roles and Responsibilities](./Roles.md)|[4. Working with Instances](./#working-with-servicenow)|
|-------|-------|-------|-------|

---------------------------

## Scrum Meetings

As part of a scrum development team you will have regular calls, dependent on your team lead and scrum master approach:

- **Daily Scrum call**: Quick update status call to discuss what you've accomplished since the last call, what you intend to accomplish before the next call, and any impediments to progress

- **Daily Technical call**: Call to discuss technical approaches and methods for active stories. Typically for developers and Application Engineers

- **Weekly Team call**: General management updates

- **Retrospective call**: Post-release, discussion of lessons learned

---------------------------


## Working With Stories

Stories are typically created by a Product Owner/Lead or Domain Architect. Story creation guidelines [here...](StoryCreation.md), and mapped to Epics stored in the OEE tracking tool Jira. An [integration with Jira](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/Jira/SN%20to%20Jira%20Integration.md) exists.

| For in-depth details realting to Agile in ServiceNow, the data model, concepts and usage, see [this PowerPoint presentation...](https://dxcportal.sharepoint.com/:p:/s/SMDevStudio/EbSnWx8F_65NoLayx_k3Q8gBieuQ1XGSFdi7b8szRIEwMg?e=32cazr) |
|----------|

### Key Fields

![](../images/StoryFields.png)

| **Field**                 | Comment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Product**               | The application component. Used to derive: 1. **Read-write access permission** (in conjunction with the Scrum Release field). 2. **Operational pillar** (e.g. Orchestration) which is used in certain functions (e.g. integration with Jira)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| **Epic**                  | Epics are created in the rm_Epic table by Product Owners, and hold the matching Jira issue-key for integration purposes. (Jira is viewed as the source of truth for Epics).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **Scrum Release**         | The team responsible for developing the Story                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| **Description**           | A clear concise description of the business requirement. Include a justification descriptor, explaining the business, architectural and technical reasons for the story, as well as any critical timelines.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| **Acceptance criteria**   | Defines when the story is ‘done’. Typically: \`As a \< type of user \>, I want \< some goal \> so that \< some reason \>\`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| **Points**                | Points assigned, in a Fibonacci-like sequence, that represent the relative difficulty of the story (which gives a rough estimate of the time required to complete the story). See [Story Point Estimation](#story-point-estimation) guidelines.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| **State**                 | See [What the Story States Mean…](#what-the-story-states-mean)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **Work Notes**            | Work notes should be used to clearly identify the design decisions and what has been modified. This benefits developers working on development later in the same functional area, indicating how and why the code was modified. It also helps the Technical Review by allowing the Tech Lead and Global Tech Leads to quickly evaluate the changes. Work notes should also be used to clearly identify anything that would block a story from being completed in the time frame estimated. By doing this proactively, it allows the Scrum Master to work at removing the blockers and assisting the tester, developer or Tech lead to complete their work quicker and give a manager, Product Owner or Stakeholder visibility as to what is causing a delay. |
| **Migration Information** | Correct update set and other data information that might be required to complete a story.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |





### What the Story States Mean

| **State**             | **Who**                                                   | **Metric Class** | **Comment**                                                           |
|-----------------------|-----------------------------------------------------------|------------------|-----------------------------------------------------------------------|
| Draft                 | Product Owner, Domain Architect, Application Engineer. <sup>Δ</sup>     | Backlog          | Initial creation of story                                             |
| Estimation            | Product Owner, Domain Architect, Application Engineer     | Backlog          | Initial review (pre-grooming). May include story point estimation     |
| Ready                 | Product Owner, Domain Architect, Scrum Team               | Backlog          | Groomed by Scrum Team and ready to start development        |
| Awaiting Info         | Developer                                                 | In flight        | Developer cannot fully progress whilst awaiting further details       |
| Work in Progress      | Developer, Tech Lead, Scrum Team Tester                   | In flight        | Tester has created the test scripts, Developer working the story, Tech Lead is reviewing the code, Tester performing testing   |
| Development Complete  | Developer, Technical Lead, Scrum Team Tester, Product Owner | In flight      | Developer has completed the story’s objectives and unit tested, Tech Lead has reviewed the code and moved the story to TEST, Tester has completed testing, Product owner has accepted the story as complete        |
| *Ready for Testing\** | Test Lead                                                 | In flight        | The Test Lead assigns to a relevant tester                            |
| *Testing\**           | Tester                                                    | In flight        | Testing in progress or completed                                      |
| *Testing Failed\**    | Tester                                                    | In flight        | Testing failed (but does not necessarily pull the story from release) |
| On Hold               | Scrum Master                                              | In flight        | De-prioritised, blocker(s), doubt about inclusion in next release     |
| Complete ^            | Release Manager/Release Tester, Scrum Master              | Released         | Marked when fully tested, or post-release                             |
| Cancelled             | Product Owner, Domain Architect, Scrum Master             | \-               | No longer required                                                    |


References to Product Owner includes the Product Lead.

<sup>Δ</sup>Typically, developers would not create stories, but there may be occassions where they create a story from a defect, or create a story to cover a technical change to underlying code as requested by a tech lead for a future improvement

\*These three testing states are used by the Global Deployment testers. Unit testing by the developer occurs during the Work in Progress state, and testing by the development team testers occurs during the Development Complete state.

^ A Spike story must not be updated to the 'Complete' state until the follow-up Dev story (if deemed necessary) is created and the "Opened from Task" field on the Related Records tab of the Dev story contains the same RITM TASK number as the Spike story.  This best practice ensures that the RITM is not closed prematurely.

See the [Roles](./Roles.md) page for more information about who does what.

### Story Point Estimation

Story points rate the relative effort of work for a developer (IDD - Ideal Developer Days) in a Fibonacci-like format, to groom, code, and unit test. Each team should decide their own basis for estimation. This is an example of what works for some.

| **Shirt Size** | **Story Points** | **No. of Days (IDD)** | **Comment**                                                           |
|----------------|------------------|-----------------|-----------------------------------------------------------------------|
| XS             | 1                | \< 1            | Very easy to complete within a Sprint                                 |
| S              | 2                | 1 - 2           | Very easy to complete within a Sprint                                 |
| M              | 3                | 2 - 3           | Easy to complete within a Sprint                                      |
| M              | 5                | 4 - 5           | Can complete within a Sprint                                          |
| L              | 8                | 5 - 10          | Increased complexity, may be possible to complete within a Sprint  |
| XL             | 13               | 10 - 15         | Increased complexity, unlikely to be possible to complete within a Sprint |
| XXL            | 21               | \> 15           | Consider breaking up into smaller Stories                             |
| XXXL           | 40               | Unknown         | Complex story that is very difficult to break into smaller stories and that is likely to take several weeks to complete|

The _Shirt Size_ is a rough estimate of the order of magnitude of the work, often made by the Product Owner or Lead.

The _Story Points_ are typically estimated during a grooming session.

Note, the table comments above are based on a team working a 2-week sprint. However, typically in the DevStudio, we work using the Kanban approach. So although there are 2-weekly release cycles, a sprint concept is not necessarily relevant.

### Standard Story Scrum Tasks

I&A Domain Architects (DA), Application Engineers (AE) and Scrum Masters whose "Scrum Release" team has “opted-in” for use of standard Scrum Tasks on every Story, can see that standard set of Scrum Tasks automatically created as follows:

-  For new Stories created with a valid "Scrum Release" value as defined in system property ‘dxc.agile.scrum_task.optin’.

-  When the Story’s “Scrum Release” value changes from <null> to a valid value, to minimize the generation of duplicate sets if the Story is transitioned from one scrum team to another.

-  Standard Scrum Tasks are stored in system property ‘dxc.agile.scrum_task’.

-  To support the I&A Engineering and PX Product Management leadership objective for consistent collection of assigned resource planned and actual effort hour metrics in the Data Lake.

-  Eliminates the need to manually create Scrum Tasks, which has been done inconsistently or not at all.

System Properties used to manage the automation:

-  dxc.agile.scrum_task.optin = A list of the valid “Scrum Release” values for scrum teams that have “opted-in” for use of standard Scrum Tasks on every Story.

-  dxc.agile.scrum_task = The list of fields as Key/Value Pair in JSON format, used to generate the Scrum Tasks while creating a Story with a valid “Scrum Release” defined in the ‘dxc.agile.scrum_task.optin’ system property.

Here are the Standard Scrum Tasks you will see if your scrum team “opts-in”:

| **Short description**      | **Type**                | **Purpose**                                                                                            |
|----------------------------|-------------------------|--------------------------------------------------------------------------------------------------------|
| Create Test Script         | Testing                 | For a tester to create a test script.                                                                  |
| Review Test Script         | Testing                 | For another tester to review the test script created above.                                            |
| Spike Analysis             | Analysis                | For a developer to perform research on story requirements and/or technologies before starting development, usually for a Spike.                       |
| High-Level Development Plan| Analysis                | For a developer to document a plan for design to get TL pre-approval before starting development, usually for stories of 5 or more Points. |
| Coding/Unit Testing        | Coding          | For a developer to code and unit test their solution, initially in SB2 then later in Dev.                      |
| Translations               | Coding         | For a developer to determine language translation requirements/gaps when applicable, then request from the Translation team and apply to respective tables. |
| Technical Review           | Coding           | For a TL to review developer solutions, provide feedback and promote from Dev to DevTest.                     |
| Execute Test Script        | Testing         | For a tester to execute the test script created above.                                                         |
| CCN-ATG-PM Documentation   | Documentation   | For the AE to create the Capability Change Note, Update the Account Transitions Guidelines, Permissions Model and GitHub when applicable.|
| Review Spike findings-Create Dev stories| Documentation| For the AE to review Spike findings, facilitate decisions from PO, DA and other key stakeholders, then create applicable Dev stories.|
| GTL Review Rework          | Coding          | For the TL and/or developer to respond to GTL Review issues when applicable.                                   |

Here is the expected Life Cycle of a Scrum Task:

1.	Assign applicable scrum tasks to yourself when they reach associated Kanban VTB lanes, changing “Assigned to” = <your name>.

2.	Retain previous the “Primary Assignee” as an “Additional Assignee” on the VTB.

3.	Optionally add a brief “Description” to help the team know the scope of work you are planning to do for the task.

4.	Update the “Planned effort” based on the story “Points” estimate, where 1 Point = 1 developer day.  Other supporting roles will expend less effort.

5.	Update the “Actual effort” and “Remaining effort” as you make progress.

6.	Update the “State” from ‘Draft’ -> ‘Ready’ -> ‘Work in progress’ -> ‘Complete’, or ‘Cancelled’ when determined to be not applicable.  It’s OK to leave in ‘Draft’ when not applicable.

7.	Add “Work notes” if anything substantial occurs to cause you to deviate from your initial scope of work for the task.

8.	Continue to add “Work notes” and all attachments back on the Story record to provide general progress status to the team.

9.	Ensure that your “Actual effort” is accurate, and your “Remaining effort” is ‘0’ when you set the “State” = ‘Complete’.

10.	All scrum tasks should be ‘Complete’ or ‘Cancelled’ or ‘Draft’ (if not applicable) before changing the Story “State” = ‘Development Complete’.

Known Issues:

-  If the DA or AE or SM happened to manually add Scrum Tasks to the Story BEFORE the scrum team opted-in, AND the Story’s “Scrum Release” value is then subsequently set for the first time, they will see a duplicate set of Scrum Tasks, be forced to manually remove a set, and learn not to do that ever again.

-  It is also possible to get a duplicate set if the “Scrum Release” value is changed from a valid value to < null >, then changed to a valid value.

-----------------------------

## Using Visual Task Boards 

Each scrum team will have their own visual task board (VTB), typically managed by the Scrum Master. An example is shown here. Your team’s board may have different lanes, flags and checklists.

![](../images/VTBExample.png)

Separate VTBs may be used for specific projects or groups of Stories or Tasks.

[Read more about working with VTBs ...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Process/Working%20with%20VTBs.md)

### Review and Release VTB

A VTB is used by the Global Tech Leads to assit them with the review and processing of stories into the DevQA and subsequent Production environments. 

When Scrum Development Teams have changed the State of stories to _Development Complete_, they will appear on the Review and Release board, and the Scrum Master for each team will move the stories to the _Ready for Review_ lane on the board.

[Details here ...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Process/Review%20Process.md)

-----------------------------

# Working With Incidents

The [Incident queue](https://csc.service-now.com/nav_to.do?uri=%2Fincident_list.do%3Fsysparm_query%3Dactive%253Dtrue%255Eassignment_group%253Df3709f266f186100c5afed5dbb3ee455%26sysparm_first_row%3D1%26sysparm_view%3D) (**PDXC-Orch-ITSM**) is monitored by the [Application Engineers](./AE%20Duties.md) who triage Incidents relevant to their applications. Typically, a Story will be raised for any Incident requiring an application fix, the Story being prioritised as normal by the Product Owner/Lead.

[Details here ...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Process/Incidents.md)

-----------------------------

|[Top](#how-we-work)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
