# Access to the ServiceNow Development Stack

Access to the ServiceNow development servers ([Dev](cscdev.service-now.com), [DevTest](csctest.service-now.com), [DevQA](cscdevqa.service-now.com), and [Sandbox2](cscsandbox2.service-now.com)) is strictly controlled.

Developer access (Sys Admin. SEC: Administrators) can be requested via a ServiceNow Catalog item: [PDXC ServiceNow Dev Access](https://csc.service-now.com/sp?id=sc_cat_item&sys_id=50a9889ddbd6b384ccd8a5db0b961922).

At present, all requests for sys admin access must be associated with a Scrum Release team.

## Workflow

![](../images/sn-devaccess.png)

### Approval

After submission, worfklow will direct the request for business approval by a member of the [Development Studio - Admin](https://csc.service-now.com/nav_to.do?uri=%2Fsys_user_group.do%3Fsys_id%3D670ad2dcdb7cdb40acd5cebe3b96198b%26sysparm_record_target%3Dsys_user_group%26sysparm_record_row%3D1%26sysparm_record_rows%3D1%26sysparm_record_list%3DnameSTARTSWITHDevelopment%2BStudio%2B-%2BAdmin%255EORDERBYname) group.

### Fulfilment

Once approved, a task will be generated to the Tech Lead of the Scrum Release team that has been selected in the request. Scrum teams will only be listed that have a Tech Lead associated with them in the [scrum_pp_team table](https://csc.service-now.com/nav_to.do?uri=%2Fscrum_pp_team_list.do).

_Team Leads_ should be alert to update the manager field in the [scrum_pp_team table](https://csc.service-now.com/nav_to.do?uri=%2Fscrum_pp_team_list.do) when their Tech Leads change. Of course, Team Leads may designate someone other than the Tech Lead to provide sys admin access.

-----------------------------------------

## Future

Future versions of this catlaog item will:

- allow for request to multiple sandboxes, and have an administrator who can fulfil the request if there is no appropriate scrum release team to associate the user to (e.g. for sandbox access only).

- allow for different types of access (e.g. tester, various Agile roles)

- allow for automated provisioning

----------------------------------------

|[Top](#access-to-the-servicenow-development-stack)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
