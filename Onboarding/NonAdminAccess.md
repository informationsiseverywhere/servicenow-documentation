# Non-Admin Access to the ServiceNow Development Stack

If you have been granted non-admin access to one of the ServiceNow servers in
the development stack, please be aware of the implications of your actions
within the system.

**Multiple teams of developers work within the ServiceNow Development stack, and
the code within is effectively source code for the production systems.**

If your access is non-admin, your activity will be restricted. Nevertheless,
certain non-admin roles have greater privileges than others. Therefore:

- You are not permitted to do, or attempt to do, any development within the
system

- You should not attempt to run any batch jobs or scripts, or any other process
that could impact system performance  

- You should not delete or amend test data records created by others (it may be
being used by others)

- You should not attempt to change or modify core data (such as Company names)

-------------------------


|[Top](#non-admin-access-to-the-servicenow-development-stack)|[Looking for Admin Access? ...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Access-SN%20Dev.md)|
|--------|-------------------|
