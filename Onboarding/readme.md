# Onboarding

Welcome to the **PDXC SeviceNow Dev Studio Team**! This material is designed to give an overview of where you fit in the team, and the basic working processes you'll need to follow, and tools you'll need to use, to get your job done. **It's mandatory that you read this through _before_ you start working in our PDXC SN environments.**

[Introduction](#introduction)

[Developer Framework](#developer-framework)

[Our Working Process](#our-working-process)

[Working with ServiceNow](#working-with-servicenow)

[Our Collaboration Tools](#our-collaboration-tools)

[Other Key References](#other-key-references)

-------------------------

## Introduction

•	[Who are we?](Studio-SN-DXC.md#who-are-we)

•	[Where do we fit in the organisation?](Studio-SN-DXC.md#where-do-we-fit-in-the-organisation)

•	[What are the main roles in the team?](Roles.md)

•	[Which teams look after which applications?](Teams.md)

------------------

## Developer Framework

You can't develop within our ServiceNow environments until you have reviewed all of our developer documentation (the Framework).
The Framework is split across 8 separate sections. Make sure you read each, you may be tested later!

**Read this:** [**The Developer Framework** ...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md#documentation-for-pdxc-servicenow-developers)

------------------

## Our Working Process

We work within an **Agile Kanban** environment. That means nearly everything we do is Story-driven. You will work within a scrum development team, have daily scrum calls, and utilise Visual Task Boards.

**Read this:** [**Our Working Process** ...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Working%20Process.md)

------------------

## Working with ServiceNow

### Instances

You'll initially only require admin access to the [Sandbox](https://cscsandbox2.service-now.com) and [Development](https://cscdev.service-now.com) instances. The [Tech Lead](TechLead.md) in your team should provide you the correct access, after an official request has been raised. Details [here...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Access-SN%20Dev.md)

The dev instance feeds all the other environments within the Commerical stack, as well as the secure instances in the US, APAC, and EMEA. See all of our instances [here...](https://dxcportal.sharepoint.com/sites/SMDevStudio/SitePages/ServiceNow%20Instances.aspx)

### Raising tickets with ServiceNow

If you're a developer, ask your Team Lead to create a [HI Service Portal](https://hi.service-now.com/hisp?id=hisp_home) account for you. This will enable you to raise tickets or incidents directly with ServiceNow.

------------------

## Our Collaboration Tools

### GitHub
GitHub is PDXCs primary system for documentation artifacts. We use it to store key documentation (including what you are reading now), useful ServiceNow tools, scripts, process overviews, and more.

- [DevStudio's main GitHub repository](https://github.dxc.com/ServiceNowDXC)

Teach yourself GitHub videos [here ...](https://web.microsoftstream.com/channel/b23ee545-7205-4c02-ac5d-7d31c54df1fa). There's also a wiki with some quick summaries on how to edit in the GitHub markdown language [here ...](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/wiki/(A)-Basic-GitHub-Editing)

### MS Teams

- [DevStudio Channel](https://teams.microsoft.com/l/team/19%3a6ddbe74c554141179a02bfb10abe4838%40thread.skype/conversations?groupId=f9ecb739-6766-44f1-ba5e-6539cb4b27f6&tenantId=93f33571-550f-43cf-b09f-cd331338d086). Ask your team lead to add you to the channel.

### Stream

- [Our Video Channel](https://web.microsoftstream.com/group/f9ecb739-6766-44f1-ba5e-6539cb4b27f6) (Under development)

### Workplace

- [General discussion area](https://dxc.facebook.com/groups/1773599182937033/)

- [Upgrade and Release announcements](https://dxc.facebook.com/groups/2216522765298535/)

------------------

## Other Key References

| Reference                                                                                                                                                                                                                                                                                                                                 | Comment                                                                                                                             |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| [ServiceNow Account Transition Guidelines](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/blob/master/README.md)                                                                                                                                                                                                      | Describes how accounts and deployment teams can set up and configure data driven functionality across all the PDXC SN applications. |
| [Global ServiceNow Permissions Model](https://dxcportal.sharepoint.com/sites/SMDevStudio/Shared%20Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FPublished%20Documents%2FPermissions%20Model&FolderCTID=0x012000D112D155D5D1C245869705B81202D754&View=%7BA0A2DB3C-B384-462A-BBAD-5ECE67B4DEA5%7D) | Our permissions model. Any group/role additions must be updated here.                                                |
| [Release Schedule](https://dxcportal.sharepoint.com/sites/SMDevStudio/sitepages/agile%20testing%20release.aspx)                                                                                                                                                                                                                           | Lists out the two-weekly release cycle, including content cut-off dates (when development must be complete to make a release)       |
| [Dev Studio SharePoint Site](https://dxcportal.sharepoint.com/sites/SMDevStudio/default.aspx)|Being re-structured, but several useful document repositories here|
| [Dev Studio GitHub Pages](https://github.dxc.com/pages/ServiceNowDXC/PDXC-SNStudio/)|Still under construction, this site will become the portal for all of our information repositories|
| [Training](./Training.md)|Make the most of our training opportunties, in ServiceNow, and releated tools and processes|

---------------------------------

|[Top](#onboarding)|
|--------|
