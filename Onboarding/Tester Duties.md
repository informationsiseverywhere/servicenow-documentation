# Duties of the Tester

•	Produce Test Documentation (Test Plan, Test Cases, Test Summary Report)

•	Familiar with Test Driven Development approach

•	Perform System Testing, run Automated Regression Testing and Post Implementation Testing

•	Test Summary Report walkthrough of each release

•	Use ServiceNow Agile Development for Agile Management

•	Follow an Agile test approach and attend daily ‘Stand Up’ meetings

•	Create and execute automated regression test scripts using Selenium

•	Familiar with Microsoft Outlook/Word/Excel 2010 etc.

•	ServiceNow London and ITSM, ITAM, ITOM, SRM and Integration experience

•	Familiar with ServiceNow workflow

•	Strong communication skills in English

•	Familiar with SOAP UI, WebServices, REST API to simulate transaction with external application

•	Strong analysis skills to identify side effects of the development


## Typical Experience

•	Bachelor's degree in a technical field or equivalent work experience

•	Minimum of 2 years enterprise/large scale, multi-platform applications

•	Experience in Agile Application Development & Scrum methodologies

•	Passionate about team and team members’ success

•	Excellent communication, problem solving and analytical skills

•	Strong sense of ownership, responsibility, and initiative

•	Flexibility to balance multiple assignments in a fast-paced environment

----------------------------------------

|[Top](#duties-of-the-tester)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
