DevStudio, ServiceNow and DXC
=============================

Who Are We?
-----------

We are a global team of ServiceNow experts who develop and maintain multiple
leveraged and stand-alone instances of ServiceNow.

The **Platform DXC ServiceNow (PDXC SN)** image is our branded version of
ServiceNow, extended and customized to support the business requirements as set by the Product Owners and Architecture.

We have three key regional PDXC SN development groups based in North America, EMEA, and APAC, each running several
scrum development teams.

| [More information about the product we support …](https://github.dxc.com/pages/ServiceNowDXC/PDXC-SNStudio/overview/) |
|-----------------------------------------------------------------------------------------------------------------------|
| [**More information about our roles and teams …**](./Roles.md)                                                            |

-------------------------------------

Where Do We Fit in the Organisation?
------------------------------------

The DevStudio PDXC ServiceNow development teams are part of the **OE&E - Automation, Architecture and Engineering**
organisation.

The ServiceNow capabilities that we develop and manage, form part of an overall
product strategy delivered to customers by DXC called [Platform DXC
(PDXC)](https://github.dxc.com/pages/Platform-dxc/docs/overview/). PDXC is
itself part of a larger digital product branded by DXC as [DXC
Bionix<sup>TM</sup>](https://whatwesell.dxc.com/dxc/compound-offerings/dxcbionix/)

PDXC includes many tools, which are categorised as either core, or one of three
‘pillars’, known as *Intelligence, Orchestration, and Automation*.

ServiceNow sits under the **Orchestration** pillar.

![](../images/OrchestrationDiagram.png)

Suggested Reading
-----------------

| [What is Platform DXC?](https://github.dxc.com/pages/Platform-dxc/docs/overview/) |
|-----------------------------------------------------------------------------------|
| [**What is Bionix?**](https://whatwesell.dxc.com/dxc/compound-offerings/dxcbionix/)       |

---------------------------------------

|[Top](#devstudio-servicenow-and-dxc)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
