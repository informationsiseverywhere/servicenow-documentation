Self-Training Course
====================

  * [Introduction](#introduction)
  * [Section 1 - Knowledge Transfer References and Recordings](#section-1---knowledge-transfer-references-and-recordings)
  * [Section 2 - Technical Leaders and Developers](#section-2---technical-leaders-and-developers)
  * [Section 3 - Scrum Team Certification](#section-3---scrum-team-certification)
  * [Section 4 - Scrum Team Setup](#section-4---scrum-team-setup)
  * [Section 5 - Scrum Team Mentoring](#section-5---scrum-team-mentoring)

Introduction
------------

The following Platform X (PX) ServiceNow (SN) Development Studio - Agile Development process and tool usage best practices and standards can assist Application Engineer,
Technical Leader, Developer and Tester scrum team roles perform more consistently, efficiently and effectively with Domain Architect, Product Owner and Global Technical
Leader roles.

Section 1 - Knowledge Transfer References and Recordings
--------------------------------------------------------

Target audience includes all roles mentioned above, with focus on the Application Engineer role.

1.  Open [Application Engineer Knowledge Transfer References vN.N](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Application%20Engineer%20Knowledge%20Transfer%20References%20v1.0.docx) and click "View raw" to download, which functions as the syllabus for this section of the course.

2.  Open [Service Management Studio - ITSM - New ServiceNow Application Architect Onboarding Presentation vN.N](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/Service%20Management%20Studio%20-%20ITSM%20-%20New%20ServiceNow%20Application%20Architect%20....pptx) and click "View raw" to download, which provides supporting material.

3.  Select from 2 available sets of audio/video recordings.

    a)  Each set covers the material in the Knowledge Transfer References and Onboarding Presentation, using live examples in the PDXC ServiceNow Commercial Production
    instance Agile Development module and related development and testing instances.
    
    b)  While viewing one set may provide sufficient insight, viewing the other set may be beneficial to get a broader set of Q&A with different participants.
    
4.  View set 1 of DevStudio KT Recordings - Daleks to WMSM – 10.0 hours.

    a)  [DevStudio KT Recordings - Daleks to WMSM - Session1](https://web.microsoftstream.com/video/62c48526-03d7-4bd6-8bbb-fd0376dd2bf4)
    
    b)  [DevStudio KT Recordings - Daleks to WMSM - Session2](https://web.microsoftstream.com/video/9d7673ba-df99-4f43-b738-22064ec3a976)
    
    c)  [DevStudio KT Recordings - Daleks to WMSM - Session3](https://web.microsoftstream.com/video/92e2b948-1098-497e-a375-765da33e584e)
    
    d)  [DevStudio KT Recordings - Daleks to WMSM - Session4](https://web.microsoftstream.com/video/a2b3318d-70d2-43c9-bbdf-1e9629aa26d7)
    
    e)  [DevStudio KT Recordings - Daleks to WMSM - Session5](https://web.microsoftstream.com/video/223fbdfc-78e7-41a0-801b-8b8b981381ce)
    
    f)  [DevStudio KT Recordings - Daleks to WMSM - Session6](https://web.microsoftstream.com/video/a6deeb1e-f0bc-432f-8e8e-5cbb816cf16e)
    
    g)  [DevStudio KT Recordings - Daleks to WMSM - Session7](https://web.microsoftstream.com/video/f8046a60-e1aa-4123-9a93-a1b59f43a02f)

5.  View set 2 of DevStudio KT Recordings - Daleks to Alderaan - Session1 – 8.5 hours.

    a)  [DevStudio KT Recordings - Daleks to Alderaan - Session1](https://web.microsoftstream.com/video/554a798e-630d-4fce-a060-5b486c92387f)
    
    b)  [DevStudio KT Recordings - Daleks to Alderaan - Session2](https://web.microsoftstream.com/video/d539685b-a9c7-4da0-bd88-78def7e7e18e)
    
    c)  [DevStudio KT Recordings - Daleks to Alderaan - Session3](https://web.microsoftstream.com/video/46824fcf-88d7-4b4b-a785-c4e812adefaa)
    
    d)  [DevStudio KT Recordings - Daleks to Alderaan - Session4](https://web.microsoftstream.com/video/2976d72c-c9b8-4328-8e1a-de14c4da6899)
    
    e)  [DevStudio KT Recordings - Daleks to Alderaan - Session5](https://web.microsoftstream.com/video/026a4b6d-09fa-4d4f-bfae-d2b5719586c5)
    
    f)  [DevStudio KT Recordings - Daleks to Alderaan - Session6](https://web.microsoftstream.com/video/062a8f12-56fe-4d98-961f-3fcfc9064f2f)
    
6.  Review our [Working Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Working%20Process.md) which provides the general principles of working in our Agile environment.

Section 2 - Technical Leaders and Developers
--------------------------------------------

Target audience is reduced to Technical Leaders and Developers.

1.  Review [PX ServiceNow Code Development Guidelines](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Code%20Guidelines%20ServiceNow.md) – 3.0 hours.

2.  Review [PX ServiceNow Catalog Developers Documentation](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Catalog%20Developers/README.md) #1 to 4 (if applicable) – 1.5 hours.

3.  Review [PX ServiceNow Test Automation](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TestAutomation.md) (if applicable) - 3.0 hours.

Section 3 - Scrum Team Certification
------------------------------------

Target audience is new Studio or Inner-Sourced scrum teams, whose members must certify course completion to obtain System Administrator access.

1.  Open the [PDXC SN DevStudio KT Self-Training Certification Tracking Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Files/PDXC%20SN%20DevStudio%20KT%20Self-Training%20Certification%20Tracking%20Template.xlsx) and click "View raw" to download.

2.  Have new Application Engineer or Technical Leader save a copy of the template to compile tracking information into
“PDXC SN DevStudio KT Self-Training Certification Tracking - Scrum Release team name”.

3.  Provide the information in the template to the Scrum Release team’s new Application Engineer and Technical Leader, who will track completion in the certification
tracking file.

4.  Schedule a follow-up meeting for the new Scrum Release team as needed with an experienced Application Engineer and/or Technical Leader to answer outstanding questions,
tracking completion in the certification tracking file.

5.  Have new Technical Leader setup a mentoring session with an experienced Technical Leader, tracking completion in the certification tracking file.

6.  Have new Application Engineer or Technical Leader submit the completed Scrum Release team’s certification tracking file to request user account access to applicable
PX ServiceNow instances, by submitting to PDXC ServiceNow Development Studio Orchestration Manger > Clay Ramsey clay.ramsey@dxc.com 

Section 4 - Scrum Team Setup
----------------------------

Target audience is new Studio or Inner-Sourced scrum teams, to create an Agile Development module meta data structure that is consistent with existing Studio scrum teams.

1.  Go to [Operations Engineering & Excellence – ITSM Studio > Documents > Agile](https://dxcportal.sharepoint.com/sites/SMDevStudio/Shared%20Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FAgile&FolderCTID=0x012000D112D155D5D1C245869705B81202D754&View=%7BA0A2DB3C%2DB384%2D462A%2DBBAD%2D5ECE67B4DEA5%7D).

2.  Open “ServiceNow Agile Onboarding vN.N”.

3.  Follow the instructions to populate and submit the “ServiceNow Agile Onboarding Template”.

4.  Setup Scrum Release team, Sprints, Assignment group with an experienced Application Engineer.

5.  Setup Scrum Release team access to applicable Products, Themes, Epics with Product Owners.

6.  Create Kanban Visual Task Board (VTB) with the Scrum Release team members.

7.  Add new Scrum Release team to Global Technical Leader (GTL) “Stories – Ready for Review and Release” VTB.

Section 5 - Scrum Team Mentoring
--------------------------------

Target audience is new Studio or Inner-Sourced scrum teams, to operate in the Agile Development module consistent with existing Studio scrum teams.

1.  New Application Engineer and new Technical Leader provides leadership and guidance to the new Scrum Release team Developers and Testers.

2.  New Scrum Release team Developers and Testers begin to utilize their Kanban VTB to develop and test stories for release.

---------------------------

|[Top](#introduction)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
