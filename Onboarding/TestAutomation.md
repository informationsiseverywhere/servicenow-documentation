Test Automation
====================

  * [Introduction](#introduction)
  * [Section 1 - Training](#section-1---training)
  * [Section 2 - DevTest Environment](#section-2---devtest-environment)
  * [Section 3 - ATF Testing Process](#section-3---atf-testing-process)
  * [Section 4 - Test Status Reporting](#section-4---test-status-reporting)

Introduction
------------

The Platform X (PX) ServiceNow (SN) Development Studio promotes test-driven development practices.  Test Automation is a key enabler to achieve the I&E Engineering leadership
goal to minimize release cycle time.  While there is still a significant amount of test automation collateral in Selenium, a major objective is to move that collateral
to the SN Automated Test Framework (ATF) module.  The first priority is to create new ATF collateral where none exists in Selenium, then replicate what is in Selenium as much
as possible in ATF, then move to steady state to support all I&A scrum team story development that impacts this ATF test collateral.

The following information covers the current ATF development process, including applicable training, the team's working process in the DevTest environment, and related
progress tracking standards.  The target audience is any Test Automation developer who is working in PX SN ATF.

Section 1 - Training
--------------------

The [ServiceNow Developer website](https://developer.servicenow.com/dev.do) provides a personal instance for all user who can sign-in to the developer site.

1.  Developers can access their personal instance either by logging into developer site or by navigating to URL of the personal instance, e.g.

https://devxxxxx.servicenow.com/login.do?user_name=admin&sys_action=sysverb_login&user_password=xxxxxxxxxxxx

2.  The ServiceNow Developer site provide basic training module in Automated Test Framework (ATF).  To access the training module, navigate to Learn > Courses >
Automated Test Framework > Using Automated Test Framework.

![](../images/TestAutomation001.png)

3.  The ATF training module is comprised of the following key elements:

    a)  Creating ATF Tests, Test templates and Test Suites
    
    b)  Creating parameter for parameterized testing
    
    c)  Test Scheduling
    
    d)  Test Debugging

![](../images/TestAutomation002.png)

4.  The ‘Server Scoped’ and ‘Server Global’ sections under the [Reference](https://developer.servicenow.com/dev.do#!/reference/api/quebec/server) module provides a list
of all server side API’s which could be referred and used in the Server Side Scripting in ATF.

![](../images/TestAutomation003.png)

5.  Below are some of the key SN documents related to ATF:

    a)  [Automated Test Framework (ATF)](https://docs.servicenow.com/bundle/paris-application-development/page/administer/auto-test-framework/concept/automated-test-framework.html)
    
    b)  [ATF Properties](https://docs.servicenow.com/bundle/paris-application-development/page/administer/auto-test-framework/reference/atf-admin-properties.html)
    
    c)  [Custom UI test steps](https://docs.servicenow.com/bundle/paris-application-development/page/administer/auto-test-framework/concept/custom-ui-test-steps.html)
    
    d)  [Test a Service Catalog request](https://docs.servicenow.com/bundle/paris-application-development/page/administer/auto-test-framework/task/atf-serv-cat-use.html)

    e)  [Automated Test Framework Best Practices](https://www.servicenow.com/content/dam/servicenow-assets/public/en-us/doc-type/success/quick-answer/automated-test-framework-best-practices.pdf)

6.  The [Product Documentation](https://docs.servicenow.com) page has many documents related to ATF.

7.  Any queries and questions can be posted in the [ServiceNow Community](https://community.servicenow.com/community) page.

8.  For issues regarding the performance or technical assistance, a case could be raised as an HI Ticket in the [ServiceNow - Now Support](https://support.servicenow.com/now)
page with the required access to login.

    a)  The access to Now Support is provided by the Manager or Technical lead on request.
    
    b)  With the access by logging in to now support site HI ticket can be created by clicking ‘Create a Case’ button, where one can describe the issue or technical doubts.
    
    c)  Create a Case > Performance Issue or ServiceNow related question > Enter (Subject, Description, Steps to Reproduce > Enter contact information >
    Enter affected instance (cscdev non-prod) > Add screenshots (if necessary).
    
9.  Automated Test Framework (ATF) Certification:

    a)  ServiceNow provide a fundamental micro-certification on Automated Test Framework (ATF) including the pre-requisite course on the Now Learning Platform.
    
    b)  Here is the link to the course and the certification:  [Automated Test Framework (ATF) Fundamental Micro-certification Assessment](https://nowlearning.service-now.com/lxp?id=overview&sys_id=e22631dddbdef300760a71043996191f&type=path)
    
Section 2 - DevTest Environment
-------------------------------

[DevTest](https://csctest.service-now.com) is a Service Automation - DEVELOPMENT Test Environment where in ATF test cases are created and tested.

1.  To create Automated Test Framework (ATF) tests in DevTest , in the application navigator – 

    a)  Automated Test Framework (ATF) > Tests
    
    b)  Automated Test Framework (ATF) module also provides the navigation to test suits , test results , manual page inspector, client test runner , ATF Properties etc. 

2.  Creating ATF tests involves two main attributes to be defined – which includes Name and Description.

    a)  Test Naming convention – Name STARTSWITH ATF, e.g. ATF #1 Test Case Name  or  ATF_#1 Test Case Name.
    
    b)  Description should include the data sets involved in that test case like Users, Assignment groups and Configuration items.
    
3.  Key points to be noted before creating the test case are ‘Users’ and their ‘Groups’.

    a)  The “sys_user” table contains all the users and “sys_user_group” table contains all the user groups available in the environment.
    
    b)  Baseline documentation have been created containing the data sets to be used for the different module of ATF testing (ITSM, ITBM, Asset and Configuration Management, Service Portal, Service Request).
    
    c)  [Operations Engineering & Excellence - ITSM Studio - ATF - All Documents](https://dxcportal.sharepoint.com/sites/SMDevStudio/Shared%20Documents/Forms/AllItems.aspx?viewid=a0a2db3c%2Db384%2D462a%2Dbbad%2D5ece67b4dea5&id=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FATF) links to the Sharepoint library which contains the baseline test data documents for each product area.

4.  ATF Tests Migration:

    a)  During the regression testing for releases and upgrades, ATF test cases are exported from DevTest to other PX SN instance for testing.
    
    b)  The [Replicate ATF test cases from DevTest to any other PX SN instance](https://dxcportal.sharepoint.com/:w:/r/sites/SMDevStudio/Shared%20Documents/ATF/Replicate%20ATF%20test%20cases%20from%20DevTest%20to%20any%20other%20PX%20SN%20instance.docx?d=wb64dfbd3d7d142f5945505e3eca85a9c&csf=1&web=1&e=rYSXUv) document contains the detailed process of exporting the tests from DevTest and importing them to another instance.

Section 3 - ATF Testing Process
-------------------------------

1. 	Review the GDN Testing team's manual regression test scripts used for major upgrade and release testing.

    a)  [CSCV GESM - IVT Current Test Script Templates for Update - All Documents](https://dxcportal.sharepoint.com/sites/CSCVGESM/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FCSCVGESM%2FShared%20Documents%2FGESM%2FTesting%2FService%20Now%2FGDN%2FSNOW%20Test%20Scripts%2FIVT%20Test%20Scripts%20Template%2FIVT%20Current%20Test%20Script%20Templates%20for%20Update&p=true&originalPath=aHR0cHM6Ly9keGNwb3J0YWwuc2hhcmVwb2ludC5jb20vOmY6L3MvQ1NDVkdFU00vRW9WV3JqQ1FLcXBCaXdEU2prbEVXb2tCMGRWdjVobVBhaHdvM2l1MDhWclVtUT9ydGltZT1ZUVM0Wm10ZTJVZw) share point library contains all the current GDN manual regression testing list, which is THE source for ATF testing scripts. 

2. 	Create the scrum task under the respective story for the test case under testing.

    a)  In Dalek’s Board , ‘Automated TS’ lane contains stories for creating ATF test cases under different modules.
    
    b)  Below are the fields that need to be defined to create a scrum task:

       -  Type = Coding
        
       -  State
        
       -  Assigned to = The Tester’s name
        
       -  Planned Effort
        
       -  Actual Effort
        
       -  Short Description = The name of the test script

3.  Prepare or identify all the necessary data sets (Users, Groups, Configuration Items etc.) required for the test script.

    a)  Separate stories are created to supplement baseline test data for each product area, since the xml files must be promoted to all
    PX SN instances to permit running ATF tests in those instances.
    
    b)  Some test cases may need data configurations to be done to meet the test script pre-conditions before test execution. For all such
    test cases “Data Configuration Notes” are attached to the ATF tests.

![](../images/TestAutomation005.png)

4.	 Design the test scripts and Execute test runs.  Test scripts are created according to the standards covered in ‘Section 2’.

5.	 Report the result in the tracking sheet.  Instructions for test result reporting is included in ‘Section 4’.

Section 4 - Test Status Reporting
---------------------------------

1.  [Automation Test Script - Master List- Refractoring](https://dxcportal.sharepoint.com/:x:/s/CSCVGESM/ETlmHye_SDZBmZXwXHycWzkB1SJRsjMhVtJZzmhoXSTxYQ?e=4%3Avo0OZ1&at=9&CID=091B7D16-8056-407B-8F5D-D18869B41799&wdLOR=c493FEB74-6CCF-4BE8-9225-59C6C1CB43A1) is a master tracking sheet to track the status of the test case development.

    a)  The tracking sheet contains data pertaining to test script name, description, implemented hours, Tester name, status, limitation(if any) which needs to be updated once the test case creation is completed.

    b)  Provides the I&A leadership team insight into the overall Test Automation coverage and associated gaps.

    c)  The ‘Manual Test Cases’ page of Automation Test Script - Master List contains the list of manual test cases which are grouped under various modules and product areas.
    
    d)  Each Test script has status tracking for both Selenium and ATF combined.
    
    e)  All the ATF data has a column header with ‘ATF’ included in its name.

![](../images/TestAutomation004.png)

2.  Data to be defined during the status tracking are as below –

    a)  ATF Planned Implementation : Quarter of the current fiscal year when the test case is created.

    b)  ATF Implemented Hours : Number of hours consumed while creating test for the test script.(Should be same as the ‘Actual effort’ of the scrum task)

    c)  ATF Status: According to result of the ATF test , the status can be marked as Completed, Completed(Limitation), NOT START, Skipped .

    d)  ATF Tester: Contains the name of the tester who have created the tests in ATF.

    e)  ATF Limitation : If creating the test case is complete but there are some limitations in some sections of the test scripts , any such limitations are described in this column. A ‘Completed(Limitation)’ status should have a limitation marked for the respective test scripts.

    f)  ATF Limitation Percentage % : Is an estimated percentage of limitation steps to the total number of steps in the test script.

3.  Skipped state of ATF Status is considered in two conditions –

    a)  Those test cases are tested and are covered in Selenium.

    b)  The requirement of test script is not suitable for ATF and are out of ATF scope at present.  This condition also involves a reason on why test scripts are skipped from ATF testing in the Limitation column.
    
4.  For those test scripts which are not yet in the ATF Tracking sheet -

    a)  In order to add a test script to the list, filter the data to that module under which the test scripts need to be added.
    
    b)  For Example, if ‘#1 Test case name’ is an Asset Management test script which needs to be added, apply a filter for module name column to be ‘Asset Management’ and add the test script according to the test script number such that the order of test case is not affected.

---------------------------

|[Top](#introduction)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
