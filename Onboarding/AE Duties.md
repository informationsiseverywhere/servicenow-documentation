# Duties of the Application Engineer

  * [Overview](#overview)
  * [Technical Understanding](#technical-understanding)
  * [Defect Monitoring and Triage](#defect-monitoring-and-triage)
  * [Incident Monitoring and Triage](#incident-monitoring-and-triage)
  * [Documentation](#documentation)
  * [Strategy and Continuous Improvement](#strategy-and-continuous-improvement)

Overview
--------

Specialises in a ServiceNow application (or applications), having in-depth
understanding and insight of both the OOTB features, and any customisations.
Works with the Domain Architect, Product Owner and Offerings teams to help
achieve a clear strategy for the application.

Advises and works with the application development team, to ensure that Stories
they work can be achieved technically, and meet the general design principles of
the application.

Considered a subject-matter expert, the Application Engineer is the general
‘go-to’ person for technical questions in relation to the application that they
have specialist responsibility for.

Technical Understanding
-----------------------

Investigation and on-going research needed to keep up-to-date with newly
released features and functionality of the application. Keeps abreast of
upcoming ServiceNow upgrades, researching the potential impact and benefit for
PDXC.

Understands the PDXC permissions model as it relates to the application.

Defect Monitoring and Triage
----------------------------

Monitors on a daily basis the [defect queue](https://csc.service-now.com/nav_to.do?uri=%2Frm_defect_list.do%3Fsysparm_clear_stack%3Dtrue%26sysparm_userpref_module%3D6d691477efb42000a7450fa3f8225612%26sysparm_view%3DScrum%26sysparm_query%3Dactive%3Dtrue%5EEQ) for any defects that have been raised
against the application. Triages the defects, and updates the defect record – if
they are valid, then assigns the defect to the Team Lead. If the defects are not
valid, then re-assigns the defect back to the person who opened the defect, with
relevant comments.

Incident Monitoring and Triage
-------------------------------

Monitors on a daily basis the [Incident queue](https://csc.service-now.com/nav_to.do?uri=%2Fincident_list.do%3Fsysparm_query%3Dactive%253Dtrue%255Eassignment_group%253Df3709f266f186100c5afed5dbb3ee455%26sysparm_first_row%3D1%26sysparm_view%3D) for any Incidents that have been raised against the application, following the process documented [here ...](../Process/Incidents.md)


Documentation
-------------

Responsible for creating the Capability Change Note (CCN) for new
customisations. This document gives a brief overview of the functionality
implemented in a story, notes dependencies/relationships to other stories, and
any high-level deployment considerations. CCNs are attached to the relevant
story, and also stored in a DevStudio AE SharePoint folder [CCN archive](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?viewpath=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FCapability%20Change%20Notes&sortField=Modified&isAscending=false).

1.	 Start with the [CCN template](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx), which is an MS Word .dot file type.  Note that this folder contains "ServiceNow Capability Change Note - STRYXXX" v1.0, v2.1 and v2.2.  Use v2.1 unless you are doing policy workflow which uses v2.2.

2.	 Change the date at top-right to preserve it, update the STRY# in the title and the footer, replace ‘Studio Application Engineer’ with your name in the footer, then save the file after replacing the STRY# in the file name.  This creates a .docx file type from the .dot template.

3.	 Follow the instructions in each section of the template, replacing those instructions with content.  Primary content is from the STRY Acceptance Criteria, reworded from “will/should” to ‘is/are”.  Include screenshots from Dev or DevTest where applicable.

4.	 Reference the library of existing CCN examples in the AE sharepoint [CCN archive](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?viewpath=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FCapability%20Change%20Notes&sortField=Modified&isAscending=false).

5.	 Optionally ask an experienced AE to review the first couple of your CCNs, then apply feedback before publishing.  See [TeamsFull.md](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md) for the current AEs.

6.	 Attach your CCN .docx file to the STRY in SN Comm Prod once development and internal testing are complete and you are ready for Demo.

7.	 Upload your CCN .docx file to the AE sharepoint [CCN archive](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?viewpath=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FCapability%20Change%20Notes&sortField=Modified&isAscending=false).

8.	 Use applicable deployment content from your CCN to update the ATG.


Responsible for updating the [Account Transition Guidelines (ATG)](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/blob/master/README.md) which contain
detailed information on functionality, configuration, and deployment
considerations.  See the [ATG Wiki](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/wiki) for maintenance details.

It is expected that the AE would also make meaningful contributions to the
Development Studio’s [GitHub repository](https://github.dxc.com/ServiceNowDXC) and [SharePoint site](https://dxcportal.sharepoint.com/sites/SMDevStudio/default.aspx), and assist with
reporting.

Strategy and Continuous Improvement
-----------------------------------

The main strategists for business applications in PDXC ServiceNow are the Chief Architect, Product Owner and Domain
Architect. However, it is expected that the AE will also have involvement in
strategy discussions, which may include dialog with ServiceNow themselves.

Looks for ways to improve the process and functionality of the tool,
understanding what should be left to ServiceNow to develop, and what potentially
should be handled by the Development Studio to add to the PDXC value
proposition.

----------------------------------------

|[Top](#duties-of-the-application-engineer)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
