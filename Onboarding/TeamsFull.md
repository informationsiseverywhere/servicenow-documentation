# Platform X Teams - Who's Who

## ServiceNow Applications and Related Development Teams

-------------------------------

### Platform Owners

| Role                   | Who                                                                                                                                                                                                                           |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Product Manager**        | [Iain Caldwell](mailto:icaldwell@dxc.com)                                                                                                                                                                            |
| **Chief Architect**        | [Nick Bentley](mailto:nbentley@dxc.com)                                                                                                                                                                                       |
| **Global Technical Leads** | [Contact via MS Teams Channel](https://teams.microsoft.com/l/channel/19%3ac1a29c138d8943b789617b098a0aa9ba%40thread.skype/General?groupId=f9550240-335a-47d5-88fc-8003e5e4a471&tenantId=93f33571-550f-43cf-b09f-cd331338d086) |

See also the [Platform and Roles](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Platform%20and%20Roles.md)

--------------------------------------------------

| **App/Area**             | **Scrum Team** | **Team Lead**     | **[Application Engineer](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/AE%20Duties.md)** | **Scrum Master**  | **Domain Architect** | **Product Owner** | **[Tech Lead](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TechLead.md)** | **Business Sponsorship (L1/L2/L3)** |
|---|---|---|---|---|---|---|---|---|
| **Agile**            | Daleks            | Jim Zacharias<br>Victor Martinetti      | Victor Martinetti             | Victor Martinetti      | Nick Bentley         | Paul Kawka    | Dan Tevis        |L1: [Iain Campbell](mailto:icaldwell@dxc.com)<br/>L2: [Terese Pate](terese.pate@dxc.com)<br/>L3: [James Taylor](james.taylor@dxc.com)|
| **Asset**             | Alderaan           | Jim Zacharias<br>Victor Martinetti   | Sue McInnis     | Alain Beaufayt       | Sadagopan Gopinath          | Paula Mattingly    |  Eric Knoerr  ||
| **ATF**            | Kaveri            |    Victor Martinetti      | Ankita Jain            | Ankita Jain      | Nick Bentley         | Clay Ramsey    | Ankita Jain        ||
| **Case**              | Daleks           | Jim Zacharias<br>Victor Martinetti       | Victor Martinetti         | Victor Martinetti       | Nick Bentley            | Paula Mattingly    | Dan Tevis ||
| **Change**            | Victoria           | Didier Poisseroux | Dale Crawford            | Didier Poisseroux | Nick Bentley | Paul Kawka       | Bibeesh Thaliyingal ||
| **Chat**                         | Daleks             | Jim Zacharias<br>Victor Martinetti       | Victor Martinetti        | Victor Martinetti       | Nick Bentley        | Paul Kawka   | Dan Tevis  ||
| **Cloud - AWS**<br />*offering* | AWS | |Vidita Sharma|Srinath RAJU GV|Anatoly Kofman|Eric Moore| ||
| **Cloud - Azure** <br />*Offering*| | |Anatoly Kofman| | | |Anatoly Kofman ||
| **Cloud - CPS**<br />(Cloud Platform Services)<br />*Offering* | | | | | | | Jan Vana<br />Chandra Kamalakantha ||
| **Config**     | Alderaan           | Jim Zacharias<br>Victor Martinetti   | Sue McInnis     | Alain Beaufayt       | Sadagopan Gopinath   | Paula Mattingly  | Eric Knoerr  ||
| **CNOW**<br /> ConnectNow Framework | | | | | | | ||
| **CNOW** <br /> **Client ebonds** | | | | | | | Rich Miller <br/> Deva Undamatla ||
| **Discovery** ServiceNow    | Alderaan           | Jim Zacharias<br>Victor Martinetti   | Sue McInnis     | Alain Beaufayt       | Sadagopan Gopinath   | Paula Mattingly  | Eric Knoerr  ||
| **Discovery** UD/UCMDB    | Umbara           | Navendu Gupta   | Sue McInnis     | Alain Beaufayt       | Sadagopan Gopinath   | Paula Mattingly  | Fabrizio Mancuso  ||
| **Event**             | Alderaan              | Jim Zacharias<br>Victor Martinetti | Sue McInnis       | Alain Beaufayt       | Christopher Cannon        | Paula Mattingly  | Sudeep Sandhu  ||
| **Field**<br>**Services**               | WMSM           | Amit Dwiveldi | Sundeep Kumar            | Nhung Do | Nick Bentley | Shivani Joshi       | Veeresh Alur ||
| **Incident**          | Victoria           | Didier Poisseroux | Dale Crawford            | Didier Poisseroux | Nick Bentley | Paul Kawka       | Bibeesh Thaliyingal ||
| **Know-**<br>**ledge**         | Victoria           | Didier Poisseroux | Dale Crawford            | Didier Poisseroux | Nick Bentley | Paul Kawka       | Bibeesh Thaliyingal ||
| **Offering**<br>**to Deal**             | Daleks           | Jim Zacharias<br>Victor Martinetti       | Victor Martinetti         | Victor Martinetti       | Nick Bentley	   | Paula Mattingly       | Dan Tevis  ||
| **Policy**<br>**Workflow**              | Victoria             | Didier Poisseroux | Dale Crawford            | Didier Poisseroux | Christopher Cannon  | Paula Mattingly     | Bibeesh Thaliyingal ||
| **Portal**                       | Daleks             | Jim Zacharias<br>Victor Martinetti       | Victor Martinetti        | Victor Martinetti       |  Nick Bentley        | Paul Kawka   | Dan Tevis  ||
| **Problem**                      | Victoria           | Didier Poisseroux | Dale Crawford            | Didier Poisseroux | Nick Bentley | Paul Kawka       | Bibeesh Thaliyingal ||
| **Project**<br>**Portfolio** | Daleks           | Jim Zacharias<br>Victor Martinetti       | Victor Martinetti         | Victor Martinetti       | Nick Bentley       | Paula Mattingly    | Dan Tevis  ||
| **Release**           | Daleks            | Jim Zacharias<br>Victor Martinetti      | Victor Martinetti             | Victor Martinetti      | Nick Bentley         | Paul Kawka    | Dan Tevis   ||
| **Security Operations**<br /> *Offering* | | | | Georgi Simeonov | Amit Kumar Verma | Amanda Keene | Amit Kumar Verma ||
| **Service**<br>**Catalog**<br>**Clients**              | T&T<br>Engineering          | Paul Chaplin     | Paul Chaplin       | N/A      | Nick Bentley         | N/A     | Arun Thanikachalam   ||
| **Service**<br>**Catalog**<br>**Offerings**              | T&T<br>Engineering          | Paul Chaplin     | Paul Chaplin       | N/A       | Nick Bentley         | N/A      | Arun Thanikachalam    ||
| **Service**<br>**Request**   | Victoria            | Didier Poisseroux     | Dale Crawford       | Didier Poisseroux      | Nick Bentley         | Paul Kawka     | Bibeesh Thaliyingal  ||
| **Survey**                       | Daleks             | Jim Zacharias<br>Victor Martinetti       | Victor Martinetti        | Victor Martinetti       |  Nick Bentley        | Paul Kawka   | Dan Tevis ||
| **Test Automation**                       | Kaveri             | Victor Martinetti       | Ankita Jain       | Ankita Jain       |  Nick Bentley        | Clay Ramsey   | Ankita Jain  ||
| **Usage and Invoice**                       | Alderaan             | Jim Zacharias<br>Victor Martinetti       | Sue McInnis        | Alain Beaufayt       |  Sadagopan Gopinath        | Paula Mattingly   | Eric Knoerr ||
| **W&M - DXC Virtual Agent**<br/>*offering*| WMSM | Amit Dwivedi | TBD | Nhung Do | TBD | Shivani Joshi | Veeresh Alur ||
| **W&M - Actionable Moments**<br/>*offering*| WMSM | Amit Dwivedi | TBD | Nhung Do | TBD | Shivani Joshi | Veeresh Alur ||
| **W&M - Language Translation Microservice**<br/>*offering* | WMSM | Amit Dwivedi | TBD | Nhung Do | TBD | Shivani Joshi | Veeresh Alur ||
| **W&M - Flexera**<br/>*offering* | Jedha | John Losensky | John Losensky | John Losensky | Fitzgerald Stewart | Debra Rolston | Lynn Wang |  |
| **W&M - SN Asset**<br/>*offering* | Mustafar | John Losensky | John Losensky | John Losensky | Fitzgerald Stewart | Debra Rolston | Veeresh Alur |  |
|||||||||

Technical Leads (application code owners) can be engaged via the [PDXC SN Tech Leads Teams channel](https://teams.microsoft.com/l/channel/19%3ac1a29c138d8943b789617b098a0aa9ba%40thread.skype/General?groupId=f9550240-335a-47d5-88fc-8003e5e4a471&tenantId=93f33571-550f-43cf-b09f-cd331338d086).

## Notes for Updaters

- Try and use the same name that's entered in ServiceNow
- If you are changing a Tech Lead, don't forget to update the _Manager_ name in the [scrum_pp_team table](https://csc.service-now.com/nav_to.do?uri=%2Fscrum_pp_team_list.do%3Fsysparm_query%3Dactive%253Dtrue%26sysparm_first_row%3D1%26sysparm_view%3D). This entry is used to assign workflow tasks when [requests are made for sys admin access.](https://csc.service-now.com/sp?id=sc_cat_item&sys_id=50a9889ddbd6b384ccd8a5db0b961922) See the [process...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Access-SN%20Dev.md)

-----------------------------

[See the summary view](./Teams.md)

----------------------------

[View all PDXC development teams in the Orchestration pillar](https://github.dxc.com/Platform-DXC/org/blob/master/map-scrum-architects.md#2--orchestration-pillar---scrum-teams---)

----------------------------

|[Top](#devstudio-teams---whos-who)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
