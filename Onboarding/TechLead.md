# Duties of the Technical Lead

## Overview

The Tech Lead operates within one or more development teams, acting as the 'eyes and ears' of the Global Tech Lead (GTL) team. The Tech Lead therefore has responsbibility to help *protect the scalability, integrity and architecture of the PDXC SN solution*, by reviewing design options during groominmg sessions, and reviewing code both during and post-development.

The Tech Lead must act as a *champion* for the [admin rules](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md#pdxc-sn-development-admin-rules) governing the development of the PDXC SN solution, as well as the overall [Development Framework.](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md#documentation)

## Key Responsibilities

- Working with the [Application Engineer](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/AE%20Duties.md), acts a subject matter expert for areas of technical design within one or more applications/modules.

- Reviewing code development (update sets) by team members, _before_ migration to DevTest, ensuring that the [PDXC SN Development Framework](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md) is followed.

- Calling out conflicts in development (or proposed development) with the [Big Rules.](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Big%20Rules.md#development-big-rules)

- Engaging with the GTLs, whenever there are doubts about architectural or design decisions, via the Teams channel, [PDXC SN Technical Leads.](https://teams.microsoft.com/l/channel/19%3ac1a29c138d8943b789617b098a0aa9ba%40thread.skype/General?groupId=f9550240-335a-47d5-88fc-8003e5e4a471&tenantId=93f33571-550f-43cf-b09f-cd331338d086)

- Documenting within Stories any key design considerations, including confirmation of discussions, and approvals, from the GTL team.

- Granting admim access to instances in the development stack, only _after_ business approval of a [RITM request.](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Access-SN%20Dev.md)

- Mentoring and guiding more junior developemnt team members.


General ServiceNow Technical Lead Requirements
---------------------------------------

-   3 years Technical Leadership role for any software development, either in
    application or infrastructure

-   4 years ServiceNow Development experience

-   Experience with promoting update sets to other environments and performing
    analysis as to conflicts prior to promotion

-   Current ServiceNow SysAdmin certification (must be valid for latest GA
    ServiceNow release)

-   ServiceNow Domain Separation Training

-   Proficient in JavaScript (JS) and familiar with JS Best Practices

-   ServiceNow Advanced Scripting or equivalent training

-   “Advanced System Administration” or equivalent training


----------------------------------------

|[Top](#duties-of-the-technical-lead)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
