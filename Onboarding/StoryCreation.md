# User Story Creation Guidelines

The quality of user stories is a critical factor in the success of Agile initiatives from estimation to implementation. The following guidelines should be used.

User Stories describe a small piece of required application functionality, they can be considered as small, vertical slices of system functionality, sized so they can be completed and value delivered to the business in a single iteration or sprint (if possible) by an Agile/Scrum team.

## Format

User stories typically follow this format

`As a < type of user >, I want < some goal > so that < some reason >`

A good user story provides a short, simple description of a required feature told from the perspective of the person/actor who wants or needs the new capability.

Typically, the person/actor will be a user or customer.

## INVEST Principle

A good user story should be:

- **"I" ndependent** (of all other stories in so far as possible. Dependencies while sometime needed make planning, prioritization and estimation more difficult within a sprint)
- **"N" egotiable** (should not be a specific contract for features, have to leave space for discussion around story during review/planning sessions)
- **"V" aluable** (to provide some value to the users)
- **"E" stimable** (to a good approximation using either t-shirt sizing or story points estimation)
- **"S" mall** (so as to be deliverable within a single iteration or sprint if possible)
- **"T" estable** (in principle, even if the test needs to be created during the iteration or sprint. If acceptance test cannot be defined then team will be not able to determine when story is complete)

[INVEST Principle](https://en.wikipedia.org/wiki/INVEST_(mnemonic))

*Part credit to: PDXC Scaled Agile Guidelines*

-----------------------------------

|[Top](#user-story-creation-guidelines)|[Our Working Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Working%20Process.md#working-with-stories)|
|--------|-------------------|

