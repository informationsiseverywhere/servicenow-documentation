
# NEW TO GITHUB?
GitHub is a code hosting platform for version control and collaboration. 
The PDXC ServiceNow development team use GitHub to store 'documentation as code'.

THE LANGUAGE
------------

You're reading a GitHub page right now. Pages are written using a *Markdown* language.

Adding certain characters before words and sentences, when rendered on the web
page, creates formatting such as headings, bold text, italics, etc.

For **basic GitHub page editing and formatting** [see this Help
section.](https://help.github.com/articles/basic-writing-and-formatting-syntax/)

START PLAYING
-------------

To get yourself familiar with creating and editing pages, you may want to create
your own GitHub repository so that you can play around with the various editing
options. (To do that, go to the [DXC GitHub Home
page](https://github.dxc.com/) \> **Start a New Project**).

GET A WORD PLUG-IN TO HELP
--------------------------

You can also install a plug-in in MS Word called **Writage**, that will convert
Word pages into Markdown format, that you can then copy and paste into a GitHub
page. [See details
here.](https://github.dxc.com/ServiceNowDXC/Account-Transition-Guidelines/wiki/(H)-Using-the-Word-Plugin-(Writage))


## FURTHER INFORMATION

[What's GitHub?](https://guides.github.com/activities/hello-world/#what)





