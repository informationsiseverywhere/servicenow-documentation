# Duties of the Developer

Job Description
---------------

-   Researches, designs, develops, configures, integrates, tests and maintains
    existing and new business applications and/or information systems solutions
    including databases through integration of technical and business
    requirements. Applications and infrastructure solutions include both 3rd
    party software and internally developed applications and infrastructure.
    Responsibilities include, but are not limited to, analysis of business
    requirements, coding of modifications or new program, creation of
    documentation, testing and maintenance of applications, infrastructure, and
    information systems including database management systems. Works within the
    Information Technology function, obtaining resources and working in support
    of objectives and strategies. Provides required documentation and
    participates in architecture reviews to ensure that the solutions comply
    with standards and use approved technologies. Typical customers are company
    end users and external client end-users in a variety of industries such as
    Health Care, Transportation, Government, and Insurance.

Responsibilities
----------------

-   Participates as a member of an agile scrum team. Performs basic analysis of
    functional or business requirements. Completes code stubs prepared by more
    senior developers. Participates in code review. Prepares and executes Unit
    tests. Applies advanced technical knowledge to maintain a technology area
    (e.g. ServiceNow Studio development tools and techniques).

-   May perform solution design. Applies the company and 3rd party technologies
    to infrastructure and software solutions of moderate complexity. Implements
    end- user or enterprise infrastructure or services prepared by more senior
    technologist.

Education and Experience Required
---------------------------------

-   A technical Bachelor's degree and five years of ServiceNow development
    experience as an employee of a commercial firm or governmental entity, or a
    Master's degree and two years of ServiceNow development experience as an
    employee of a commercial firm or governmental entity.

Knowledge and Skills
--------------------

-   ServiceNow Studio development experience is required. The experience must be
    in ServiceNow development as an employee of a commercial firm or
    governmental entity. ServiceNow implementation experience or client end-user
    support experience is recognized as valuable but does not count toward the
    engineering experience requirement.

-   A valid ServiceNow Certified System Administrator Certificate is required.

-   Evidence of expert-level skills in JavaScript is required.

-   Completion of ServiceNow Domain Separation training is highly desired.

-   Completion of ServiceNow Advanced Scripting training is highly desired.

-   Experience in ServiceNow ATF design, development, and execution is a plus.

-   Experience using Continuous Integration/Continuous Delivery tools such as
    GitHub, Jenkins, Ansible, etc. is a plus.

-   Basic understanding of modern software development tools and SCM (software
    configuration management).

-   Basic understanding of Software Test methodologies, test scripting and
    testing tools.

-   Understanding of Basic Database Administration.

-   Good verbal and written communication skills.


----------------------------------------

|[Top](#duties-of-the-developer)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
