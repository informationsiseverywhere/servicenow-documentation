# Training

## Self-Training Documentation 

View the on-demand [Self-Training Course](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Self-Training%20Course.md#self-training-course) created from the materials compiled during the mentoring of new scrum teams.  Sections are required based on your role, where certification is tracked at the scrum team level.  Content can be consumed at your own pace.

## ServiceNow Partner Portal

Get access to the latest tools and exclusive content as a DXC partner to ServiceNow [here](https://partnerportal.service-now.com/).

## GitHub

Multiple [DXC video shorts](https://web.microsoftstream.com/channel/b23ee545-7205-4c02-ac5d-7d31c54df1fa). For beginners to advanced GitHub users.

## DXC Training

### Bionix

[5 modules](https://github.dxc.com/pages/bionix/storefront/training/bionixassettraining) that explain the relevance and importance of the DXC Bionix sub-brand, and its relationship to Platform DXC


------------------------------

|[Top](#training)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
