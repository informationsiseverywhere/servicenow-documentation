ROLES
-----

![](../images/roles.png)

---------------------------

See also the [Platform and Roles](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Platform%20and%20Roles.md)

---------------------------


| Role                 | Description                                                                                                                                               |
|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Product Owner**        | Works with the accounts and architects to help set tool strategy and prioritise development work. May have oversight of multiple Products (applications). |
| **Product Lead**         | Assists the Product Owner, and will typically oversee one or two Products.                                                                                |
| **Team Lead**            | Manages the development (scrum) team                                                                                                                      |
| **Scrum Master**         | Coordinates and manages scrum meetings and tasks                                                                                                          |
| **Domain Architect**     | Translates business requirement into functional requirement. High-level conceptual design                                                                 |
| [**Application Engineer**](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/AE%20Duties.md) | Application subject matter expert. Technical design. Defect triage. Capability Change Notes                                                               |
| [**Developer**](./Developer.md)            | Coding and scripting                                                                                                                                      |
| [**Dev Team Tester**](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Tester%20Duties.md)      | Test scripts, and functional testing in the DevTest environment                                                                                           |
| **Deployment Tester**    | QA Testing with production type data. Regression testing included                                                                                         |
| [**Regional Tech Lead**](./TechLead.md)   | Technical review of developer’s work before migrating it to the DevTest environment                                                                       |
| **Global Tech Lead**     | ‘Gatekeeper’ and ‘protector’ of the PDXC SN code base. Final review of code before promotion to production                                                |
| **Release Manager**      | Owns the process for migration to the Pre-Production and Production environments   |
| **Chief Architect**|Oversees the entire PDXC SN Architecture|

---------------------------------------

|[Top](#roles)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
