# Catalog Developers

| See the [T&T ServiceNow Catalog Development site](https://github.dxc.com/pages/tnt/sn-cat-dev/) for Catalogs |
|--------|

Catalog developers are responsible for the development of service request catalog items in PDXC ServiceNow.

## References

ℹ [**Service Catalog Maintenance - Training for Catalog Managers**](https://dxcportal.sharepoint.com/sites/GlobalServiceRequestManagement/SRM_Wiki/Service-catalog-maintenance-training.aspx)

Pay special attention to these sections:

- [**DXC internal service catalog request workflow**](https://dxcportal.sharepoint.com/sites/GlobalServiceRequestManagement/SRM_Wiki/DXC_ServiceNow_Catalog.aspx). This describes the process and workflow that all requests for catalog development go through. 
- [**Data standards and guidelines**](https://dxcportal.sharepoint.com/sites/GlobalServiceRequestManagement/SRM_Wiki/Service-catalog-maintenance-training.aspx). Review the service catalog data template best practises and all _CDT_ training videos. 
- [**The Catalog maintenance Golden rules**](https://dxcportal.sharepoint.com/sites/GlobalServiceRequestManagement/SRM_Wiki/Service-catalog-maintenance-training.aspx)


| **\#** | **Page**                                                                                                                                                                    | **Description**                                                                                                                      |
|--------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| 1.     | [Catalog Developer – Process](https://github.dxc.com/pages/tnt/sn-cat-dev/process/process/) | The process and workflow to follow when actioning catalog requests                                                                   |
| 2.     | [Catalog Developer – Guidelines](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/)   | DXC standards and conventions to be followed when building and maintaining catalogs                                                  |
| 3.     | [Catalog Developer – Review Checklist](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/codereview/) | Items that a tech lead will review before migrating a newly developed catalog to an upper environment                                |
| 4.     | [Service Portal Compatibility Issues](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/sp_compatability/)     | List of methods and functions that should no longer be used when developing catalogs as they are not supported in the Service Portal |
| 5.     | [Catalog Manager's Reference SharePoint Site](https://dxcportal.sharepoint.com/sites/GlobalServiceRequestManagement/SiteAssets/SRM%20ServiceNow%20Document%20Library.aspx)     | Useful source of information about the catalog data template, data standards, catalog picture library, etc. |
____________________________________
