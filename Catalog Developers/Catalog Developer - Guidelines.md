# Catalog Developers - Guidelines

| See the [T&T ServiceNow Catalog Development site](https://github.dxc.com/pages/tnt/sn-cat-dev/) for Catalogs |
|--------|

All Catalog development starts in the Development environment for secure accounts, catalogs with integration, and catalogs with Policy Workflow. Customer accounts leveraging the Commerical production system are built in the Pre-Production environment.

| **\#** | **Page** | **Description** |
|--------|----------|-----------------|
| 1.     | [Catalog](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/catalogcreate/) | Guidelines regarding the creation of the Catalog. |
| 2.     | [Catalog Items](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/Create%20Catalog%20Items/catalogitems/) | Guidelines regarding the creation of Catalog Items. |
| 3.     | [Update Set](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/updateset/) | Guideline regarding Update Sets. |
| 4.     | [Catalog Scripting](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/scripting/) | Guideline regarding Catalog Scripting. |
| 5.     | [Translations](https://github.dxc.com/pages/tnt/sn-cat-dev/guidelines/translations/) | Guideline regarding Catalog Translations. |


