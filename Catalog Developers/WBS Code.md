Obtaining WBS Codes for Customer Catalogs
=========================================

### This information is for catalog managers submitting requests for customer catalogs.

Customer catalogs are now actioned by the **Transition and Transformation** organisation. Please see their process for WBS codes here:

https://github.dxc.com/pages/tnt/sn-cat-dev/process/wbscode/

