Contributing in ServiceNow
===========================

- [Documentation](#documentation)
- [Development](#development)
  * [Onboarding New Development Teams](#onboarding-new-development-teams)
  * [Individual Contributors](#individual-contributors)
  * [What is the OEE Bionix Request Process?](#what-is-the-oee-bionix-request-process)
- [Contacts and Code Owners](#contacts-and-code-owners)
- [Can I use GitHub for SN Code?](#can-i-use-github-as-a-repository-for-servicenow-code)
- [Working with ServiceNow APIs](#working-with-servicenow-apis)

Documentation
-------------

We welcome updates to our documentation, and would encourage all in the PDXC
ServiceNow development community to actively contribute to our repositories. You
can create pull requests in this GitHub repository for changes, but if you’re
new to GitHub, check out [this page
…](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/New%20to%20GitHub.md)

Development
-----------

PDXC ServiceNow is not the same as generic ServiceNow, nor is it the same as
other components of PDXC. PDXC SN has robust processes for the management of
change to the code base, via request items and stories. So, if you want to
contribute to the PDXC SN code base, then there are some guidelines that we’d
like you to follow – we call those guidelines the **[Developer
Framework.](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md)**
The framework includes SN best practices, but also additional requirements that
are unique to one of the largest SN MSP (Managed Service Provider) deployments
in the world. Have a look at what we’d need you to [commit
to…](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)

Development work is based around application-specific scrum teams, working with
regional **[Technical
Leads](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TechLead.md)**
(the application code owner). Regional Technical Leads work closely with Product
Owners, Domain Architects, and [Application
Engineers](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/AE%20Duties.md)
to ensure that contributions to the code base:

-   Protect the integrity, stability and security of PDXC SN

-   Are part of the strategic roadmap defined by the Chief Architect, Product
    Manager and Product Owners

-   Adhere to SN and PDXC best development practices (the Framework).

Regional Tech Leads work under the direction of the **Global Tech Leads** (the
platform code owners).

![](/images/SNPlatform1.png)


### Onboarding New Development Teams

| **STEP** | **ITEM**                                                                                                                                                                                    | **HOW**                                                                                                                                                                                                                       |
|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1.       | **Get approval to work on the platform.** Open dialog with Product Manager and the Chief Architect                                                                                          | *See* [Contact list….](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md)                                                                                                                |
| 2.       | **Get Help:** Identify the application area(s) you are contributing to, and talk to the Product Owner(s) and Domain Architect(s)                                                            | *See* [Contact list….](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md)                                                                                                                |
| 3.       | **Initiate Onboarding:** Get your team set up in the ServiceNow Agile application. This is the hub of the release delivery process, and how your code will get into Production.             | *Download and populate the* [ServiceNow Agile Onboarding template](https://dxcportal.sharepoint.com/:x:/s/SMDevStudio/EXlt7E68AItInrL0JiWyyp8Bs6aCItjmaJT_dBUQZhObhA?e=yEfHPm)*, and* [submit](mailto:clay.ramsey@dxc.com)*.* |
| 4.       | **Understand the Developer Framework:** Get your team up to speed on the specific DXC ServiceNow code development guidelines and rules for use in our environments.                         | *See* [DXC SN Developer documentation….](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/README.md)                                                                                                            |
| 5.       | **Understand the Process:** Know that all designs must go through three levels of BAT approval (**B**usiness, **A**rchitectural, and **T**echnical) to maintain the integrity of the platform, and ensure the product roadmap is not impacted. | *See* [Development Process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/DevProcess.md)                                                                                                                                                                                                     |


New development teams should follow our general working process [here...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Working%20Process.md).

That includes:

- tracking update sets in the _Migration information_ field in SN Agile (see [Migration Information Template](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Template.md#migration-information-template))
- collaboration with the application Technical Lead (see [full list of contacts](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md))
- using the [Review and Release VTB process](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Process/Review%20Process.md)

### Individual Contributors

If you want to make an individual contribution to the code base, and you’re not
part of an existing team, what can you do? If you want to develop something in our Sandbox environment, and present it for review, you can follow steps **1 to 7** below.

However, it's advisable to firstly reach out to the [Product Owners/Leads for the application area](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md) you want to contribute to. They can give insight into the product roadmap and what is already in development. They may ask you to jump straight to step **7** (Raise an enhancement for consideration - the [OEE Bionix Request
    process](https://csc.service-now.com/sp?id=sc_cat_item&sys_id=5e4f4fd0dbc4d700e370a2bc0b961978) process).


> 1.  You need to at least have gone through SN System Administration training
>
> 2.  Familiarise yourself with the Developer Framework. Start [here
    …](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Developers/Developer%20Admin%20Rules.md)
>
> 3.  Confirm which application area you would like to contribute to, and note
>    which Scrum development team normally works in that area, checking the list
    [here
    …](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Teams.md)
>
> 4.  Raise a request for access to our Sandbox by following the process [here
    …](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Access-SN%20Dev.md)
>    (Note, initially, as a sole contributor not attached to a team, you should
>    only include in your request access to the Sandbox environment). When you’re
>     asked in the request to select a Scrum Release team, select the one that you
>     noted in step 4.
> 
> 5.  The request for access will be fulfilled by a Technical Lead. Contact the
>     Technical Lead for the application area you are working in (check
    [here](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md)
>     for the contacts) so that they are aware why you want to work in that area.
>     There may be specific reasons why you should not carry out your development
>    in Sandbox at that time. 
>     
>     _Typically, requests for Sandbox access are fulfilled within 3 business days._
> 
> 6.  Work on your contribution in the Sandbox environment
> 
>     _Note that any development in the Sandbox environment cannot be migrated into development. Rather, any work carried out there should be considered as a Proof of Concept. The Sandbox environment is regularly refreshed. Therefore, work with the Technical Lead to ensure that any work you are carrying out will not be lost._
> 
> 7.  Make a request for that contribution to be considered and reviewed via the
    [OEE Bionix Request
    process](https://csc.service-now.com/sp?id=sc_cat_item&sys_id=5e4f4fd0dbc4d700e370a2bc0b961978). The request type to use is **Enhance/Change**.
>    


### What is the OEE Bionix Request Process?

It’s a [process](https://dxcportal.sharepoint.com/sites/oeeDeliveryWorkspace/SitePages/OEE%20Bionix%20Intake%20Request%20Workflow%20-%20Default.aspx) where Product Owners and Domain Architects will triage requests
to determine whether there is business justification for the enhancement
request. For example, is it inline with the current product roadmap (for both
PDXC and ServiceNow)? Does it overlap existing functionality? Are there
alternative OOTB approaches? Is the suggestion already in development by another
team? The OEE Bionix Request is made via a [SN Catalog item](https://csc.service-now.com/sp?id=sc_cat_item&sys_id=5e4f4fd0dbc4d700e370a2bc0b961978).

Contacts and Code Owners
------------------------

For a list of development teams that handle each application within PDXC SN, see
[here
…](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/Teams.md)

For the key contacts within those teams (Product Owners, Domain Architects, Tech
Leads, Scrum Masters, Application Engineers), see [here
…](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/TeamsFull.md)

|**Technical Leads** (application code owners) can be engaged via the [PDXC SN Tech Leads Teams channel](https://teams.microsoft.com/l/channel/19%3ac1a29c138d8943b789617b098a0aa9ba%40thread.skype/General?groupId=f9550240-335a-47d5-88fc-8003e5e4a471&tenantId=93f33571-550f-43cf-b09f-cd331338d086).|
|--------|


Can I use GitHub as a repository for ServiceNow code?
-----------------------------------------------------

We currently do not support PDXC SN code in GitHub. Some SN developers have
achieved a limited GitHub integration; ServiceNow themselves are starting to
deliver in some of their future SN platform releases, elements of GitHub
integration, and we are working with SN to look at how we can apply that within
the complexities of our MSP space. We would need to ensure that an instance can
be spun up based exclusively on the content of a GitHub repository, including
plugins, app store applications, and all the accumulated customization and
technical debt of a ServiceNow installation. However, at present, that level of
technical maturity for GitHub integration simply does not exist.

Working with ServiceNow APIs
----------------------------

Want to integrate with ServiceNow? Check the [Contributing page](https://github.dxc.com/Platform-DXC/integration-api/blob/master/CONTRIBUTING.md) in the PDXC Integration API GitHub repo.

If you want to contribute to the **ServiceNow ConnectNow** framework, check the [Contributing page](https://github.dxc.com/Platform-DXC/integration-servicenow/blob/master/ConnectNowContributing.md) in the Integration ConnectNow repo.

------------

|[Top](#collaborating-in-servicenow)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|

