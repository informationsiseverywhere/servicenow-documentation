ServiceNow Configuration for Intelligence Data Push 
====================================================

PDXC ServiceNow data from the configured tables under Intelligence configuration
are streamed in near Real-time.

When a record from a configured table or any of its child tables is created,
updated or deleted the data is pushed to the AWS Data Lake.

If the data stream fails, the newly created, updated or deleted records are
written to an error queue table; these records are push again to the AWS Data
Lake by a scheduled trigger configuration.

You can navigate to the ServiceNow Intelligence modules with appropriate access
in PDXC environment under **“System Definition”** application.

![](../images/BEEA39DF.PNG)

## Intelligence Tables

PDXC ServiceNow configured active tables to push data to the AWS Data Lake
are visible through the **‘Intelligence Tables’** module.

![](../images/ABBACA54.PNG)

**Note: -**
-   No one should manually configure, modify or delete configured tables as
    these are considered as PDXC ServiceNow data that are part of the PDXC
    ServiceNow regular release cycle. The addition or removal of configured
    tables in a PDXC ServiceNow instance would be accomplished through a Bionix
    Work Intake Request.

-   This module shows only the base table. It does NOT show each extended tables
    of a configured base table; however, the data will also be pushed from all
    extended tables, if the base table is configured.

## Intelligence Error Queue

All error transactions will be available in the Intelligence Error Queues table,
and you can navigate to the table via the **‘Intelligence Error Queue’** module. The
data under this table is auto processed by a scheduled trigger until the data is
pushed to the AWS Data Lake.

![](../images/IEQ.jpg)

## Intelligence Properties form

The AWS configuration details are defined in the Intelligence Properties form through the **'Properties'** module.

 **Note: -** 
Data visible within the Intelligence Properties form screenshot is only for reference; the Application Support teams for each instance  in collaboration with the Offering need to update all the necessary fields on the Intelligence Properties form for each PDXC ServiceNow instance with the appropriate values provided by the offering.

-   **Enable Data Push from ServiceNow (True/False)**: The data push can be
    enabled or disabled for all active Intelligence configured tables by
    selecting/deselecting this field on the Intelligence Properties form. This
    field must be set to true to push the data to the Intelligence data lake.

-   **Endpoint for AWS Firehose:** The Endpoint for the AWS Firehose is defined
    in this field on the Intelligence Properties form.

-   **Basic Authentication(base64) for AWS Firehose**: The Basic Authentication
    (base64 format) for the AWS Firehose is defined in this field on the
    Intelligence Properties form.

-   **Firehose Data Stream:** Leave this field as blank.

-   **Impersonation user (user-id):** The Impersonation user (user-id of a user record) is defined in this field on the Intelligence Properties form. This field must be filled with a valid user-id with user name such as: ‘**Big Data**’, so that the record/data to be pushed to the data lake will use the language, date-format and time-zone of this user as defined in the user profile form. If this field is not filled or has an invalid user-id, the record/data to be pushed will use the language, date-format and time-zone of the logged-in user as defined in the user profile form for the logged-in user, which can cause inconsistency of language, date-format and time-zone of data pushed.

-   **Enable error handling (True/False):** The data push to the AWS Data Lake is retried for error queue data records when this field on the Intelligence Properties form is enabled. This field must be set to true to retry pushing the transactions from the error queue to the Intelligence Data Lake.

-   **Endpoint for AWS Firehose (Error Records):** The new Endpoint for the AWS
    Firehose is defined in this field on the Intelligence Properties form to
    push error records; however, absence of the value in this field will
    consider endpoint from field ‘**Endpoint for AWS Firehose**’ to push error
    records.

-   **Basic Authentication(base64) for AWS Firehose (Error Records):** The
    different Basic Authentication (base64 format) for the AWS Firehose is defined 
    in this field on the Intelligence Properties form to push error records; however,
    absence of the value in this field will consider basic authentication credentials 
    from field ‘**BasicAuthentication(base64) for AWS Firehose**’ to push error records.

-   **Firehose Data Stream (Error Records):** Leave this option as blank.

-   **Enable debugging (True/False):** Debugging can also be enabled or disabled
    by selecting/deselecting this field on the Intelligence Properties form. By
    enabling the debug configuration, all data push transactions will be logged
    under system logs. This option should be enabled only when we need to debug
    a current data stream issue and disable as soon as the debugging is done.

![](../images/IProp_1.png)


