# Changing the Ownership of a Visual Task Board

The owner of a visual task board can only be changed by an administrator of the system (SEC:Administrator).

The owner can be edited by an administrator by opening the VTB Board form (type **vtb_board.list** into the Application Navigator).

---------------------

[Working with VTBs](Working%20with%20VTBs.md)
