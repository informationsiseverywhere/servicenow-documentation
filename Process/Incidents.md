# Working with Incidents

The Dev Studio work assigned Incidents at a Level 3 Support level.

| Who                         | Responsibilities                                                                                                                                                                                                           |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| L1 Support                  | The initial group who handle the incident, either by raising it on behalf of a caller, or picking up the Incident when first raised. Will try to ascertain a solution based on KB articles, else will assign to L2 Support |
| L2 Support                  | ServiceNow support groups. If L2 Support cannot resolve an Incident, assign to L3 Support                                                                                                                                  |
| L3 Support (PDXC-Orch-ITSM) | OEE Dev Studio. Initial triage by Application Engineer, who may then further engage developers for assistance                                                                                                              |

-   The Application Engineers (AE) monitor the **PDXC-Orch-ITSM** queue daily, and take
    ownership of those Incidents that relate to their specialist application
    (e.g. SRM, Chat, etc.)

-   The responsible AE triages the Incident, potentially engaging members of the development
    team involved if the Incident warrants that level of technical expertise.

-   If the AE determines that the Incident relates to another application within
    ServiceNow that is not their specialist area, then the Incident is assigned
    to the relevant AE.

-   If the AE determines that the Incident is user error, then it is assigned
    back to the previous assignee.

-   If the AE determines that the Incident is an application defect:

       -  The AE assigns the Incident to themselves.
       -  The AE creates a Story from the Incident and updates the Incident with the Story ID in the Correlation ID field.

-  If there is a workaround:
       
   -  The AE provides the workaround in an Incident Work note.
   -  The AE creates/publishes a Knowledge article (KA) in the 'Known Errors' knowledge base confirming the known issue.
   -  The KA is assigned to the 'PDXC-Orch-ITSM' group and references the StoryID in the Correlation ID field on the KA.
   -  The AE searches for the KA from the Incident and attaches the KA to the Incident using ‘Attach to Incident’ and adds an appropriate Work note.
   -  The Story is prioritized by the Product Owner in the responsible scrum team's backlog.
    
-   If there is no workaround:

    -  The PO prioritizes the Story as an urgent change, targeted for the next possible PX SN release.
    -  The Scrum Master of the responsible scrum team ensures that a developer is immediately assigned to work the urgent change Story.
    
-   Once the Story is targeted for a PX SN release, the AE assigns the Incident back to the L2 Support agent who escalated the Incident to L3 Support, with a reference to the Story target release number and date.

-   L2 Support advises the Customer that the Incident is being resolved as the issue is addressed in the target release of PX SN.

-   If applicable, the AE retires the known error KA when the Story that resolved the Incident is moved to the GTL VTB.
    
##  Severity Approach

| **Incident Severity** | **Approach**                                                                                                                                                                                                                                    | **Response/Resolution time**                                                                       |
|-----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| 1                     | 5 x 24 follow the sun. Hand-offs to regional leads. Refer to [call-out rota](https://dxcportal.sharepoint.com/:x:/r/sites/SMDevStudio/Shared%20Documents/SM%20Studio%20On-Call%20Rota.xlsx?d=w5b1a4cc91d644c8286d94afc8b771887&csf=1&e=cQnK1B). | 5 x 24 follow the sun to resolution.                                                               |
| 2                     | 5 x 24 follow the sun. Hand-offs to regional leads. Refer to [call-out rota](https://dxcportal.sharepoint.com/:x:/r/sites/SMDevStudio/Shared%20Documents/SM%20Studio%20On-Call%20Rota.xlsx?d=w5b1a4cc91d644c8286d94afc8b771887&csf=1&e=cQnK1B). | 5 x 24 follow the sun to resolution.                                                               |
| 3                     | Triaged by Application Engineer. If valid, story created and added to backlog for PO prioritisation.                                                                                                                                            | OLA – 2 days. No guaranteed resolution time (dependent on business value of resolving the defect). |
| 4                     | Triaged by Application Engineer, as low priority. If valid, story created and added to backlog for PO prioritisation.                                                                                                                           | OLA – 5 days. No guaranteed resolution time (dependent on business value of resolving the defect). |

Depending on the nature of the Incident, HI cases may be raised with ServiceNow
via their [Support Portal](https://hi.service-now.com/hisp) for assistance with
resolution.

The relevant Orchestration leads will be tagged in all ServiceNow HI cases, so
that they are automatically emailed with ticket updates from ServiceNow.


-------------------------

[See separate Catalog Incident Process](./Catalog%20Incidents.md)

----------------------------------------

|[Top](#working-with-incidents)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
