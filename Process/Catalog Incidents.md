# Catalog Item Incident Process 

This page has been moved to the provided link below, which is hosted by the SRM Capability Team. The Catalog Item Incident Process involves several stakeholders, meaning:
  1. Service Desk;
  2. Catalog Manager; and
  3. 'PDXC-Orch-Catalogues' (Scarecrow) Team.

The SRM Capability Team already maintaince other Catalog related processes, the Catalog Item Incident Process will be available at the same location. 

1.	Service Desk perform initial investigation and resolve the incident if possible.
    
    o	If an update to a catalog item/workflow is required, the incident is assigned to the **'Catalog Maintenance - [_company domain_]'** group*.

This process ownership will remain with OE&E.

[Catalog Item Incident Process](https://dxcportal.sharepoint.com/sites/GlobalServiceRequestManagement/SRM_Wiki/catalog_incident_management.aspx)

2.	The **'ServiceNow Application Support'** group perform an investigation and resolve the incident if possible.

    o	If an update to a catalog item/workflow is required, the incident is assigned to the **'Catalog Maintenance - [_company domain_]'** group*.
    
    o	If further investigation is needed the incident is assigned to the **'Catalog Maintenance - [_company domain_]'** group*.


3.	The **'Catalog Maintenance - [_company domain_]'** group perform an investigation and resolve the incident if possible.

    o	If an update to a catalog item/workflow is required, the catalog manager initiate the update via a catalog maintenance request.
    
   	For P2 incidents (or higher)
    
    - Criteria: no workaround exist

    - Add the incident number in the catalog maintenance request. This will be used to prioritize the maintenance request.

    For P3 incidents (or lower)

    - Criteria: workaround exist
            
    - Will be managed following the normal maintenance release cycle

    o	If only a data related update is needed, the catalog manger will raise ServiceNow catalog data maintenance request.

    o	If an integration related update is needed, the catalog manager will raise an OEE Bionix Intake request.

    o	If a policy workflow related update is needed, the catalog manager will raise an OEE Bionix Intake request.

    o	If further investigation is needed the incident, is assigned to the **'PDXC-Orch-Catalogues'** resolver group.

4.	The **'PDXC-Orch-Catalogues'** group perform an investigation and resolve the incident if possible.
    
    o	If an update to a catalog item/workflow is required, the incident is assign to the **'Catalog Maintenance - [_company domain_]'** group* including the analysis of the updates required.
 
 
*If the **'Catalog Maintenance - [_company domain_]'** group* doesn't exist, assign the incident to the **'Catalog Maintenance Geeks' group**.

   -------------------
   
   [See separate Incident process](./Incidents.md)
   
   --------------------
