Dev Studio - Tech Review Process for DevQA
==========================================

A [Visual Task Board](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Process/Working%20with%20VTBs.md) (VTB) is used to manage the technical review of Stories that are ready for migration to the DevQA environment, and inform Release Management which Stories are due to be included in the next release.

Members of the VTB can access it via: [Stories - Ready for Review and
Release](https://csc.service-now.com/nav_to.do?uri=%2F$vtb.do%3Fsysparm_board%3D8b55c617dbe697c8a3a8cebe3b9619a3). If you do not have access, you can request it [here](mailto:rmckee@dxc.com).

---------------------------------------

## Contents

[What the VTB displays](#what-the-vtb-displays)

[Using the Lanes](#using-the-lanes)

[Using the Labels](#using-the-labels)

[Working with the Process](#working-with-the-process)

--------------------------------------

What the VTB displays
---------------------

A Story must meet the following criteria to be displayed on the VTB:

-   **Scrum Release** – one of: Quantum, Dalek, APAC Region, Vishanti, Code Maesters, Naboo, Kashyyyk, Endor, Alderaan, Bumblebee, WMSM, 
Workplace & Mobility, WM Optimized Software Request, Mustafar, Azure Team, Development Studio - Integrations, MinionMayhem, CPS SS Team, FireFly, SecOps Team, Starlight.

-   **State** – one of: Development Complete, Ready for testing, Testing, Testing Failed

If the State of a Story changes to a different one from those listed, it will automatically be removed from the board.

![VTB](../images/ReviewReleaseVTB.png)

Using the Lanes
---------------

Team Leads, Scrum Masters, and the Global Tech Lead (GTL) drag Story cards
between lanes as noted below:

| Lane               | Who Moves the Story to this Lane | Purpose                                                                                                                                                                                                                              |
|--------------------|----------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Dev Complete**       | Automatic                        | A story will appear by default in this lane when its State and Scrum Release match one of the values listed [above](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Process/Review%20Process.md#what-the-vtb-displays)                                                                                                                                        |
| **Ready for Review**   | Team Lead and/or Scrum Master    | Development and Dev Team testing complete in Dev Test, these stories are ready for review by the Global Tech Leads                                                                                                                    |
| **Review in Progress** | Global Tech Leads                 | Review of a Story has been started by the GTL. As soon as the cut-off date has been reached, the GTL moves all Stories in the Ready for Review lane to the Review in Progress Lane                                      |
| **Review Issues**      | Global Tech Leads                 | Review of a Story has discovered issues that must be resolved by the Scrum Team. Details will be added by the GTL to the *Work Notes* of the Story. Story will NOT be migrated to DevQA until the issues are satisfactorily resolved |
| **Ready for DQA**      | Global Tech Leads                 | Review of the Story has been completed successfully (but there may be a conditional migration note – see *Using the Labels* below). The Story is in scope for the next release, and is just awaiting the next migration to DevQA date                                                                                                  |
| **DQA/In-scope**    | Global Tech Leads                 | Story has been migrated to the DevQA environment, is in-scope for the next scheduled release, and can be communicated (by the Release Manager) to the wider community as being ready for DevQA testing |
| **Instance Specific**| Global Tech Leads                | Stories that are not deployed to commercial production, but that are instead account/instance specific|


Using the Labels
----------------

![VTB Labels](../images/labels1.png)

Labels can be dragged onto the Story boards to give further details:

| Label                 | Who Uses the Labels           | Purpose                                                                                                                                                     |
|-----------------------|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| High Priority         | Team Lead and/or Scrum Master | Indicates there is a criticality for this story, with the assumption from Product Owners that the Story will be included in the next release                |
| Conditional Migration | Global Tech Leads              | The Story has been migrated to the DevQA environment (or will be shortly), but the GTL has some concerns which are noted in the Work Notes of the Story.    |
| Issues Fixed          | Team Lead and/or Scrum Master and/or GTLs | Used to indicate to the GTL that a Story in the Review Issues lane has had the issues fixed. Work notes in the Story should be updated to indicate the fix. If, after review, the GTL discovers the issues still exist, the GTL will remove this label.|

Working with the Process
------------------------

During each release cycle, the VTB should be monitored on a daily basis:

**Team Leads/Scrum Masters**

-   Ensure all stories that need to be in DevQA are in the *Ready for Review*
    lane by the content cut-off date noted in the [Forward Schedule for
    Release](https://dxcportal.sharepoint.com/sites/SMDevStudio/SitePages/Agile%20Testing%20Release.aspx).

-   Check Stories that are in the *Review Issues* lane, and remediate as soon as
    possible. Once remediated, update Work Notes, and add *Issues Fixed* label
    to the Story card. A follow-up email to the relevant GTL may be required to notify them that a fix is in place.

-   Check Stories that are labelled as *Conditional Migration*, and remediate as
    soon as possible. . Once remediated, update Work Notes, and add *Issues
    Fixed* label to the Story card.

-   Confirm that expected Stories are in the *Ready for DQA* or *Migrated to
    DQA* lane.

**Global Tech Lead**

-   Move Stories to the *Review in Progress* lane. Generally this would be done as soon as the content cut-off date has been reached, 
    and the review process has started

-   Stories that cannot be migrated because of any issues should be moved to the
    *Review Issues* lane, and the Work Notes updated

-   Use the *Conditional Migration* label to highlight issues that have not
    prevented the Story from being migrated, but still need addressing

-   Monitor for Stories that have the *Issues Fixed* label attached, and have been returned to the *Review in Progress* lane. Re-review, and migrate if properly remediated
    
 **Release Manager**
 
-   Monitors Stories that are in the *Ready for DQA* or *Migrated to DQA* lanes, and uses that list to communicate the Stories scheduled for the next release

------------

|[Top](#dev-studio---tech-review-process-for-devqa)|[Onboarding Home](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Onboarding/readme.md#onboarding-materials---overview)|
|--------|-------------------|
