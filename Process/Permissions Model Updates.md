Permissions Model Updates
=========================

  * [Developers](#developers)
    + [New or Updated Security Group](#new-or-updated-security-group)
    + [Adding Roles to Group](#adding-roles-to-group)
    + [Removing Roles from Group](#removing-roles-from-group)
    + [Adding Visibility Domains to Group](#adding-visibility-domains-to-group)
    + [Removing Visibility Domains from Group](#removing-visibility-domains-from-group)
  * [Application Engineers](#application-engineers)
    + [Update the ‘Distribution and Change Log’ tab](#update-the-distribution-and-change-log-tab)
    + [Update the ‘Security Groups’ tab](#update-the-security-groups-tab)
    + [Update the ‘Group Roles – Security Groups’ tab](#update-the-group-roles--security-groups-tab)
    + [Request Peer Review/Approval](#request-peer-reviewapproval)
    + [Publish the New Version](#publish-the-new-version)

Developers
----------

1.  Log into the DEV instances.

2.  Ensure you are in the TOP domain.

3.  The previous method known as "Execution Steps for Permission Model Update 11-May-2017.doc" that utilized XLS sheets
and data load transform maps should no longer be used in relation to Permissions Model changes.

4.  These changes should be treated as any other story regarding the roles of the TL and GTL reviews.

5.  The Application Engineer (AE) will update the Permissions Model spreadsheet as applicable per their section
[below](#application-engineers).

### New or Updated Security Group

1.  Navigate to the **User Administration -> Groups** page (sys_user_group.list).

2.  Click New (or find your group and select it).

3.  Fill in the required fields and save your changes.

    a.  Security group names must start with “SEC: “ and if contain multiple words, all words are capitalized and space separated
    (e.g. SEC: ITIL Support).
    
    b.  Must be created in the TOP domain and ‘Managed domain’ checked.
    
    c.  Security groups should only have one type (‘Security’).
    
    d.  Description should be succinct and concise about the type of people who will be granted this group and what function(s)
    in the platform they will be performing.
    
    e.  Security groups must contain at least one role, that already exists.
    
    f.  Security groups may contain a visibility domain, depending upon the business requirement.  Domain record must already
    exist before this.

4.  Add roles / visibility domains as needed.  See the Removing setups if you are removing either object.

5.  If adding/changing multiple groups, then repeat steps 1-4 for each group.

6.  Once complete, navigate back to the Groups List page, if not already there.

7.  Change the filter criteria such that only the group you are promoting in the story are listed.

8.  Under the Additional actions icon (hamburger), select **Export -> XML**.

9.  If prompted, click the Download button.

10.  Navigate to the folder where the download was saved and rename the file to be the following, where STRYNNNNNN
matches your current story number, e.g. STRYNNNNNN_sys_user_group.xml.

11.  Attach the XML file to the story and add the file name to the migration information.

12.  If Adding Roles, see [Adding Roles to Group](#adding-roles-to-group) below.

13.  If Removing Roles, see [Removing Roles from Group](#removing-roles-from-group) below.

14.  If Adding Visibility Domains, see [Adding Visibility Domains to Group](#adding-visibility-domains-to-group) below.

15.  If Removing Visibility Domains, see [Removing Visibility Domains from Group](#removing-visibility-domains-from-group) below.

### Adding Roles to Group

1.  Navigate to the **User Administration -> Group Roles** page (sys_group_has_role.list).

2.  Change the filter criteria such that only the group roles you are promoting in the story are listed.

3.  Under the Additional actions icon (hamburger), select **Export -> XML**.

4.  If prompted, click the Download button.

5.  Navigate to the folder where the download was saved and rename the file to be the following, where STRYNNNNNN
matches your current story number, e.g. STRYNNNNNN_sys_group_has_role.xml.

6.  Attach the XML file to the story and add the file name to the migration information.

### Removing Roles from Group

1.  You will need the Group Sys Id and the Role Sys Id for each role being removed.

2.  Modify the attached script to remove the group role record and any related user role records.  Append STRYNNNNN_ to the
front of the file when saving it.

    [pm_remove_group_role.txt](https://github.dxc.com/ServiceNowDXC/Documentation/tree/master/Files#pm_remove_group_role.txt)
    
3.  Attach the txt file to the story and add the file name to the migration information.

### Adding Visibility Domains to Group

1.  Navigate to the **Group Visibility Domain** page (sys_user_group_visibility.list).

2.  Change the filter criteria such that only the visibility domains you are promoting in the story are listed.

3.  Under the Additional actions icon (hamburger), select **Export -> XML**.

4.  If prompted, click the Download button.

5.  Navigate to the folder where the download was saved and rename the file to be the following, where STRYNNNNNN
matches your current story number, e.g. STRYNNNNNN_sys_user_group_visibility.xml.

6.  Attach the XML file to the story and add the file name to the migration information.

### Removing Visibility Domains from Group

1.  You will need the Group Sys Id and the Domain Sys Id for each domain being removed.

2.  Modify the attached script to remove the group visibility domain record and any related user visibility domain records.
Append STRYNNNNN_ to the front of the file when saving it.

    [pm_remove_group_visibility_domain.txt](https://github.dxc.com/ServiceNowDXC/Documentation/tree/master/Files#pm_remove_group_visibility_domain.txt)
 
3.  Attach the txt file to the story and add the file name to the migration information.

Application Engineers
---------------------

1.  Review the XML file attached to the story by the Developer along with the story Description to determine the scope of the changes.

2.  Ensure you have Member (Edit) access to the following via one of the Site Administrators, who are other Studio AEs:

    a.  Application Engineer (AE) sharepoint    https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/SitePages/Home.aspx
    
    b.  Studio sharepoint    https://dxcportal.sharepoint.com/sites/SMDevStudio/default.aspx
    
3.  Open the current “working” version of the Permissions Model spreadsheet at AE Sharepoint > SM Studio - Application Engineers >
Application Engineer documents > Working documents > GIS-ARCH-PM-WorkFlow-Global-ServiceNow Permissions Model X.YZ.xlsx, located
[here.](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?viewid=0bae7f5e%2Da0af%2D46e7%2Dbe1d%2Dfcfa107de123&id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FWorking%20documents)

4.  If you are new to the Permissions Model, review the ‘Model Overview’ tab contents and contact one of the Studio AEs if you have
any questions.

### Update the ‘Distribution and Change Log’ tab

1.  Scroll to the bottom of the “Change Log” section.

2.  Note that the next (future) Permissions Model “Release” number should already be present in column A.  If it is not present,
then add it (X.YZ).

3.  Enter your story’s STRY# in column B.

4.  Enter a Summary of your story changes in column C.

5.  Enter the names of each spreadsheet Tab that you changed in column D.

6.  Enter the current Date in column E.

7.  Enter your Name in column F.

8.  Repeat for each STRY# you have, noting that other Studio AEs may have already entered their updates for this version, or will
be doing so later, but before the actual release.

### Update the ‘Security Groups’ tab

1.  Add or change rows for each applicable role impacted by the change, where there is a separate row for each role.

2.  If you are adding a new row for a new “ServiceNow Role”:

    a.  Insert a new row for the applicable “Functional Area”, in “ServiceNow Role” alphabetical order from top-to-bottom within
    each “Functional Area.”
    
    b.  Obtain the “Description of role”, “Contains” and “Included in” values from the PDXC SN Development instance >
    User Administration > Roles > select your role Name > Description, and then Related Lists > Contains Roles tab.
    
    c.  The “Contains” field lists the child roles for the current row role.  The “Included in” field lists the parent roles for the
    current row role.
    
3.  If you are adding a new “SEC: “ security group:

    a.  Insert a new column for the applicable “Group Function”, in “Security Group Name” alphabetical order from left-to-right
    within each “Group Function.”
    
4.  For all updates:

    a.  Place an ‘X’ in the cell for your “ServiceNow Role” row and “Security Group Name” column, to indicate that the group contains
that role.

    b.  Place a ‘c’ in the cell for your “ServiceNow Role” row and “Security Group Name” column to indicate that the group indirectly
receives that role via another parent role.

    c.  Remove any ‘X’ and ‘c’ values that are no longer applicable, remembering that they may added back later, so do NOT delete any
existing rows or columns.

5.  Highlight all your changes with a yellow cell background.

### Update the ‘Group Roles – Security Groups’ tab

1.  This tab only needs to be updated if you are adding a new role or security group.

2.  If you are adding a role to an existing security group:

    a.  Insert a new row for the applicable “Group.”

    b.  Copy (repeat) the “Group” value in column B.

    c.  Add the new “Role” value in column C in “Role” alphabetical order from top-to-bottom within each “Group.”

3.  If you are adding a new or existing role(s) to a new security group:

    a.  Insert a new row for the new “Group”, in “Group” alphabetical order from top-to-bottom.

    b.  Add the new “Group” value in column B.

    c.  Add the new or existing “Role” value in column C, in “Role” alphabetical order from top-to-bottom within that new “Group.”

    d.  Repeat for each applicable role.

4.  Remove any “Role” rows that are no longer applicable for any “Group”, remembering that they may added back later, so do NOT delete
any security groups unless no longer applicable.

5.  Highlight all your changes with a yellow cell background.

### Request Peer Review/Approval

1.  Compose an email to your AE peers to request review and approval of your proposed changes.

2.  Use a subject like “Permissions Model X.YZ for STRYnnnnnnn – <story short description>”.

3.  If there are multiple stories involved, list them in the body of the email, including their Short descriptions.

4.  Include your target PDXC SN release number and target publish date, which is typically a few days after the content cutoff date
based on the Studio Release Calendar located
[here.](https://dxcportal.sharepoint.com/sites/SMDevStudio/SitePages/Agile%20Testing%20Release.aspx)

5.  Send to your AE peers a few days prior to your target publish date.

6.  Apply suggested changes if applicable.

### Publish the New Version

1.  This section is typically performed by the lead Studio AE once all suggested AE changes have been applied and approved for
a given release cycle, within a few days of the content cutoff date.

2.  Review the “Distribution and Change Log” to ensure accurate and complete.  If not, contact the applicable AE for details.

3.  Copy the new version:

    a.  From the “working” folder at AE Sharepoint > SM Studio - Application Engineers > Application Engineer documents >
Working documents > GIS-ARCH-PM-WorkFlow-Global-ServiceNow Permissions Model X.YZ.xlsx, located
[here.](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?viewid=0bae7f5e%2Da0af%2D46e7%2Dbe1d%2Dfcfa107de123&id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FWorking%20documents)

    b.  To the “published” folder at AE Sharepoint > SM Studio - Application Engineers > Application Engineer documents >
Published documents > Permissions Model – published documents > GIS-ARCH-PM-WorkFlow-Global-ServiceNow Permissions Model X.YZ.xlsx,
located [here.](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FPublished%20documents%2FPermissions%20Model%20%2D%20published%20documents&viewid=0bae7f5e%2Da0af%2D46e7%2Dbe1d%2Dfcfa107de123)

    c.  Archive the prior “published” version X.Y[Z-1] by moving it to the “Archived Documents” folder.

4.  Publish the new version to the Studio Sharepoint > Operations Engineering & Excellence - ITSM Studio > Documents >
Published Documents > Permissions Model > GIS-ARCH-PM-WorkFlow-Global-ServiceNow Permissions Model X.YZ.xlsx,
located [here.](https://dxcportal.sharepoint.com/sites/SMDevStudio/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FSMDevStudio%2FShared%20Documents%2FPublished%20Documents%2FPermissions%20Model&viewid=a0a2db3c%2Db384%2D462a%2Dbbad%2D5ece67b4dea5)

    a.  Archive the prior “published” version X.Y[Z-1] by moving it to the “Archived PM Docs” folder.
    
5.  Prepare the new “working” version:

    a.  Make a copy of the new version X.Y.Z, saving as X.Y[Z+1].

    b.  Add a new “Release” number row for X.Y[Z+1] to the bottom of the “Change Log” section in the “Distribution and Change Log” tab.

    c.  Remove all yellow cell background highlights from the “Security Groups” and “Group Roles – Security Groups” tabs.

    d.  Post the new “working” version X.Y[Z+1] to the “working” folder at AE Sharepoint > SM Studio - Application Engineers >
Application Engineer documents > Working documents > GIS-ARCH-PM-WorkFlow-Global-ServiceNow Permissions Model X.Y[Z+1].xlsx,
located [here.](https://dxcportal.sharepoint.com/sites/devStudioDesignLeads/Design%20Lead%20documents/Forms/AllItems.aspx?viewid=0bae7f5e%2Da0af%2D46e7%2Dbe1d%2Dfcfa107de123&id=%2Fsites%2FdevStudioDesignLeads%2FDesign%20Lead%20documents%2FWorking%20documents)

    e.  Archive the prior version X.Y[Z-1] by moving it to the “Archived documents” folder.
    
6.  Send an email notification to all AEs who contributed to the new version, letting them know that version X.YZ has been published.


*Changes to this process must be made via a PDXC SN story and approved by the Studio GTLs, TLs and AEs.*

---------------------------

|[Top](#permissions-model-updates)|[Return to main Process page](https://github.dxc.com/ServiceNowDXC/Documentation/tree/master/Process)|
|-------|-------|	
