Agile and ServiceNow
====================

The ServiceNow Agile 1.0 plug-in is used to deliver a mature and capable set of
applications (Stories, Defects, Task Boards) within ServiceNow for use by all of the OE&E Orchestration pillar.
(Agile 2.0 is currently [under review](https://csc.service-now.com/nav_to.do?uri=%2Frm_story.do%3Fsys_id%3Dfa71af7edb05c81c0e15cdae3b9619f2%26sysparm_record_list%3Dactive%253dtrue%255enumberSTARTSWITHSTRY0288993%255eORDERBYDESCsys_updated_on%26sysparm_record_row%3D1%26sysparm_record_rows%3D1%26sysparm_record_target%3Drm_story%26sysparm_view%3Dscrum%26sysparm_view_forced%3Dtrue)).

Understanding the Concepts
==========================

[This PowerPoint
deck](https://dxcportal.sharepoint.com/:p:/s/SMDevStudio/EbSnWx8F_65NoLayx_k3Q8gBieuQ1XGSFdi7b8szRIEwMg?e=32cazr)
outlines the data model, concepts and usage.

# PDXC Customisations

## Integration with Jira

All stories created with a _Product_ that is linked to the Orchestration pillar will also be automatically created in Jira. Details of the integration, field and status mappings, etc., are [here...](https://github.dxc.com/ServiceNowDXC/Documentation/blob/master/Projects/SN%20Jira%20Integration.md)


## List of SN Agile Customisations


| **Application** | **Summary**           | **Comment**                                                                                              | **Story**                                                                                                                                                                                  |
|-----------------|-----------------------|----------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Agile Story     | Access Control        | Ensures write access only for members belonging to the Scrum Release team(s) associated with the Product | [STRY0068937](https://csc.service-now.com/nav_to.do?uri=%2Frm_story.do%3Fsys_id%3Df624544b37b44700c3a5d1b543990ee0%26sysparm_stack%3D%26sysparm_view%3Dscrum%26sysparm_view_forced%3Dtrue) |
| Agile Story     | Integration with Jira | One-way integration from ServiceNow to Jira                                                              | [STRY0273300](https://csc.service-now.com/rm_story.do?sys_id=4db7d992db4808d00e15cdae3b9619df&sysparm_view=&sysparm_view_forced=true&sysparm_time=1581952864791)                           |
                                                            

Getting Access
==============

Overview of Agile
