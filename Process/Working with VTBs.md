# Working with Visual Task Boards for Stories

1.  You can only edit the filter of a VTB that you have created (see below)

2.  You can only amend the lanes of a VTB that you have created, and that is a
    **Flexible Board** (not a **Guided Board**)

3.  You can only drag stories across lanes of a Flexible Board, if you are
    member of the board, and you have edit access to the Stories concerned

You can create a VTB by doing the following:

-   Filter a [Story
    list](https://csc.service-now.com/nav_to.do?uri=%2Frm_story_list.do%3Fsysparm_query%3D%26sysparm_first_row%3D1%26sysparm_view%3Dscrum)
    using the criteria you need (you can refine and change the filter as noted
    below)

-   Right-click the *Number* field in the header and select *Show Visual Task Board*

-   This will create a Flexible Board where you can add lanes and drag between
    lanes as you wish (a rigid board, known as a Guided Board, would have been
    created had you right clicked on the *State field* in your original story
    list)

-   Add/organise lanes as you wish

-------------------------------------

[Want to change the owner of a VTB?](./VTBOwnerChange.md)
 
